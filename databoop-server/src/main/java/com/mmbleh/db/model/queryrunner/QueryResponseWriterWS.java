package com.mmbleh.db.model.queryrunner;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;
import java.util.HashMap;

public class QueryResponseWriterWS extends QueryResponseWriter {

    private Logger logger = LoggerFactory.getLogger(QueryResponseWriterWS.class);

    private WebSocketSession session;
    private ObjectMapper objectMapper = new ObjectMapper();

    public QueryResponseWriterWS(WebSocketSession session) {
        this.session = session;
    }

    @Override
    public synchronized void writeResult(DBQueryResult result) {
        try {
            session.sendMessage(new TextMessage(objectMapper.writeValueAsString(wrapData("data", result))));
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
    }

    @Override
    public synchronized void writeStatus(int done, int total) {
        HashMap<String, Object> status = new HashMap<>();
        status.put("done", done);
        status.put("total", total);
        try {
            session.sendMessage(new TextMessage(objectMapper.writeValueAsString(wrapData("status", status))));
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
    }

    @Override
    public synchronized void complete() {
        try {
            session.close();
        } catch (IOException e) {
            logger.error("failed to close websocket", e);
        }
    }

    private HashMap<String, Object> wrapData(String type, Object data) {
        HashMap<String, Object> wrapped = new HashMap<>();
        wrapped.put("type", type);
        wrapped.put("data", data);
        return wrapped;
    }
}
