package com.mmbleh.db.model;

import java.util.Date;
import java.util.List;
import java.util.UUID;

public class SourceSchema {

    private UUID id;
    private UUID sourceId;
    private List<DBTable> schema;
    private Date lastUpdated;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getSourceId() {
        return sourceId;
    }

    public void setSourceId(UUID sourceId) {
        this.sourceId = sourceId;
    }

    public List<DBTable> getSchema() {
        return schema;
    }

    public void setSchema(List<DBTable> schema) {
        this.schema = schema;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }
}
