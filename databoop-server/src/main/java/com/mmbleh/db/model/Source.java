package com.mmbleh.db.model;

import java.util.UUID;

public class Source {

    private UUID id;
    private String name;
    private boolean readOnly = true;
    private int poolSize = 1;
    private int queryTimeout = 30;
    private boolean multiTenant = false;
    private int concurrentQueries = 1;
    private MultiTenantSource multiTenantSource;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isReadOnly() {
        return readOnly;
    }

    public void setReadOnly(boolean readOnly) {
        this.readOnly = readOnly;
    }

    public int getPoolSize() {
        return poolSize;
    }

    public void setPoolSize(int poolSize) {
        this.poolSize = poolSize;
    }

    public int getQueryTimeout() {
        return queryTimeout;
    }

    public void setQueryTimeout(int queryTimeout) {
        this.queryTimeout = queryTimeout;
    }

    public boolean isMultiTenant() {
        return multiTenant;
    }

    public void setMultiTenant(boolean multiTenant) {
        this.multiTenant = multiTenant;
    }

    public int getConcurrentQueries() {
        return concurrentQueries;
    }

    public void setConcurrentQueries(int concurrentQueries) {
        this.concurrentQueries = concurrentQueries;
    }

    public MultiTenantSource getMultiTenantSource() {
        return multiTenantSource;
    }

    public void setMultiTenantSource(MultiTenantSource multiTenantSource) {
        this.multiTenantSource = multiTenantSource;
    }
}
