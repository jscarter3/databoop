package com.mmbleh.db.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;
import java.util.List;
import java.util.UUID;

public class Query {

    private UUID id;

    private String name;

    private UUID sourceId;

    private List<UUID> connectionIds;

    private String query;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private boolean testQuery = false;

    private Date modified;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UUID getSourceId() {
        return sourceId;
    }

    public void setSourceId(UUID sourceId) {
        this.sourceId = sourceId;
    }

    public List<UUID> getConnectionIds() {
        return connectionIds;
    }

    public void setConnectionIds(List<UUID> connectionIds) {
        this.connectionIds = connectionIds;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public boolean isTestQuery() {
        return testQuery;
    }

    public void setTestQuery(boolean testQuery) {
        this.testQuery = testQuery;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }
}
