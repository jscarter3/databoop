package com.mmbleh.db.model.queryrunner;

public class QueryRequestData {

    private int numberOfConnections;

    private int concurrentQueries;

    private String query;

    private String currentUser;

    private QueryResponseWriter responseWriter;

    public int getNumberOfConnections() {
        return numberOfConnections;
    }

    public void setNumberOfConnections(int numberOfConnections) {
        this.numberOfConnections = numberOfConnections;
    }

    public int getConcurrentQueries() {
        return concurrentQueries;
    }

    public void setConcurrentQueries(int concurrentQueries) {
        this.concurrentQueries = concurrentQueries;
    }

    public int getQueryPoolSize() {
        return concurrentQueries > 0 ? Math.min(numberOfConnections, concurrentQueries) : numberOfConnections;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(String currentUser) {
        this.currentUser = currentUser;
    }

    public QueryResponseWriter getResponseWriter() {
        return responseWriter;
    }

    public void setResponseWriter(QueryResponseWriter responseWriter) {
        this.responseWriter = responseWriter;
    }
}
