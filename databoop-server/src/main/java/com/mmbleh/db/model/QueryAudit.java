package com.mmbleh.db.model;

import java.util.Date;
import java.util.UUID;

public class QueryAudit {

    private UUID id;
    private UUID queryGroup;
    private String query;
    private Long queryDuration;
    private String sourceName;
    private String dbName;
    private String dbUrl;
    private String username;
    private Date created;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getQueryGroup() {
        return queryGroup;
    }

    public void setQueryGroup(UUID queryGroup) {
        this.queryGroup = queryGroup;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public Long getQueryDuration() {
        return queryDuration;
    }

    public void setQueryDuration(Long queryDuration) {
        this.queryDuration = queryDuration;
    }

    public String getSourceName() {
        return sourceName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public String getDbUrl() {
        return dbUrl;
    }

    public void setDbUrl(String dbUrl) {
        this.dbUrl = dbUrl;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }
}
