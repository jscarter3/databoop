package com.mmbleh.db.model;

public enum Role {
    DEVELOPER, REPORTER
}
