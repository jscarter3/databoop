package com.mmbleh.db.model;

import java.util.Date;
import java.util.UUID;

public class QueryAuditSummary {

    private UUID queryGroup;
    private String sourceName;
    private String query;
    private Long count;
    private String username;
    private Date created;

    public QueryAuditSummary() {}

    public QueryAuditSummary(UUID queryGroup,
                             String sourceName,
                             String query,
                             Long count,
                             String username,
                             Date created) {
        this.queryGroup = queryGroup;
        this.sourceName = sourceName;
        this.query = query;
        this.count = count;
        this.username = username;
        this.created = created;
    }

    public UUID getQueryGroup() {
        return queryGroup;
    }

    public void setQueryGroup(UUID queryGroup) {
        this.queryGroup = queryGroup;
    }

    public String getSourceName() {
        return sourceName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }
}
