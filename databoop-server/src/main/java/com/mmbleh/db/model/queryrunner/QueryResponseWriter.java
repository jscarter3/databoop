package com.mmbleh.db.model.queryrunner;

public abstract class QueryResponseWriter {

    public abstract void writeResult(DBQueryResult result);

    public abstract void writeStatus(int done, int total);

    public abstract void complete();
}
