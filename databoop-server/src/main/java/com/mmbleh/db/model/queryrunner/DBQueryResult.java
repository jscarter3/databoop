package com.mmbleh.db.model.queryrunner;

import com.mmbleh.db.model.DBConnection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DBQueryResult {

    // TODO: this is ugly... fix it
    private HashMap<String, Object> dbConnection = new HashMap<>();

    private List<ResultHeaderElement> header = new ArrayList<>();

    private List<ResultRow> data = new ArrayList<>();

    private String error;

    public HashMap<String, Object> getDbConnection() {
        return dbConnection;
    }

    public void setDbConnection(HashMap<String, Object> dbConnection) {
        this.dbConnection = dbConnection;
    }

    public void setDbConnection(DBConnection dbConnection) {
        this.dbConnection.put("id", dbConnection.getId());
        this.dbConnection.put("name", dbConnection.getName());
    }

    public List<ResultHeaderElement> getHeader() {
        return header;
    }

    public int getHeaderIndexByName(String name) {
        for (int i = 0; i < header.size(); i++) {
            if (header.get(i).getName().equalsIgnoreCase(name)) {
                return i;
            }
        }
        return -1;
    }

    public void setHeader(List<ResultHeaderElement> header) {
        this.header = header;
    }

    public List<ResultRow> getData() {
        return data;
    }

    public void setData(List<ResultRow> data) {
        this.data = data;
    }

    public void addDataRow(ResultRow row) {
        data.add(row);
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
