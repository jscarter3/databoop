package com.mmbleh.db.model;

import java.util.List;

public class DBTable {

    private String name;

    private List<DBTableColumn> columns;

    public DBTable() {}

    public DBTable(String name) {
        this.name = name;
    }

    public DBTable(String name, List<DBTableColumn > columns) {
        this.name = name;
        this.columns = columns;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<DBTableColumn> getColumns() {
        return columns;
    }

    public void setColumns(List<DBTableColumn> columns) {
        this.columns = columns;
    }
}
