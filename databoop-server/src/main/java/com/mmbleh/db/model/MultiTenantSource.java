package com.mmbleh.db.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

public class MultiTenantSource {

    private UUID id;
    @JsonIgnore
    private UUID sourceId;
    private String dbUrl;
    private String dbUsername;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String dbPassword;
    private String tenantUsername;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String tenantPassword;
    private String query;

    private CredentialSource credentialSource = CredentialSource.STATIC;

    public enum CredentialSource {STATIC, QUERY}

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getSourceId() {
        return sourceId;
    }

    public void setSourceId(UUID sourceId) {
        this.sourceId = sourceId;
    }

    public String getDbUrl() {
        return dbUrl;
    }

    public void setDbUrl(String dbUrl) {
        this.dbUrl = dbUrl;
    }

    public String getDbUsername() {
        return dbUsername;
    }

    public void setDbUsername(String dbUsername) {
        this.dbUsername = dbUsername;
    }

    public String getDbPassword() {
        return dbPassword;
    }

    public void setDbPassword(String dbPassword) {
        this.dbPassword = dbPassword;
    }

    public String getTenantUsername() {
        return tenantUsername;
    }

    public void setTenantUsername(String tenantUsername) {
        this.tenantUsername = tenantUsername;
    }

    public String getTenantPassword() {
        return tenantPassword;
    }

    public void setTenantPassword(String tenantPassword) {
        this.tenantPassword = tenantPassword;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public CredentialSource getCredentialSource() {
        return credentialSource;
    }

    public void setCredentialSource(CredentialSource credentialSource) {
        this.credentialSource = credentialSource;
    }
}
