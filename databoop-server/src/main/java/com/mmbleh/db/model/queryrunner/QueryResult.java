package com.mmbleh.db.model.queryrunner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class QueryResult {

    private List<ResultHeaderElement> header = new ArrayList<>();

    private List<ResultRow> data = new ArrayList<>();

    private HashMap<String, String> errors = new HashMap<>();

    public List<ResultHeaderElement> getHeader() {
        return header;
    }

    public int getHeaderIndexByName(String name) {
        for (int i = 0; i < header.size(); i++) {
            if (header.get(i).getName().equalsIgnoreCase(name)) {
                return i;
            }
        }
        return -1;
    }

    public void setHeader(List<ResultHeaderElement> header) {
        this.header = header;
    }

    public List<ResultRow> getData() {
        return data;
    }

    public void setData(List<ResultRow> data) {
        this.data = data;
    }

    public void addDataRow(ResultRow row) {
        data.add(row);
    }

    public HashMap<String, String> getErrors() {
        return errors;
    }

    public void setErrors(HashMap<String, String> errors) {
        this.errors = errors;
    }

    public void addError(String tenant, String error) {
        errors.put(tenant, error);
    }
}
