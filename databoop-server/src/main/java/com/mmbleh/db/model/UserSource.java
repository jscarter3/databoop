package com.mmbleh.db.model;

import java.io.Serializable;
import java.util.UUID;

public class UserSource implements Serializable {

    private UUID id;
    private UUID userId;
    private UUID sourceId;
    private Role role;

    public UserSource() {}

    public UserSource(UUID userId, UUID sourceId, Role role) {
        this.userId = userId;
        this.sourceId = sourceId;
        this.role = role;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public UUID getSourceId() {
        return sourceId;
    }

    public void setSourceId(UUID sourceId) {
        this.sourceId = sourceId;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserSource that = (UserSource) o;

        if (!userId.equals(that.userId)) return false;
        return sourceId.equals(that.sourceId);
    }

    @Override
    public int hashCode() {
        int result = userId.hashCode();
        result = 31 * result + sourceId.hashCode();
        return result;
    }
}
