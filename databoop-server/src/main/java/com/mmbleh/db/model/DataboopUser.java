package com.mmbleh.db.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.*;

public class DataboopUser implements UserDetails {

    private UUID id;
    private String username;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;
    private String name;
    private String email;
    private boolean admin = false;
    private boolean active = true;
    private Date created;

    private List<UserSource> userSources = new ArrayList<>();

    private List<GrantedAuthority> authorities = new ArrayList<>();

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    @Override
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public List<UserSource> getUserSources() {
        return userSources;
    }

    public void setUserSources(List<UserSource> userSources) {
        this.userSources = userSources;
    }

    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isEnabled() {
        return active;
    }

    public Optional<Role> getRoleForSource(UUID sourceId) {
        return userSources.stream()
                .filter(us -> us.getSourceId().equals(sourceId))
                .map(UserSource::getRole)
                .findFirst();
    }

    public boolean hasAccess(UUID sourceId) {
        return getRoleForSource(sourceId).isPresent();
    }

    public boolean hasAccess(UUID sourceId, String role) {
        Role r = Role.valueOf(role);
        return hasAccess(sourceId, r);
    }

    public boolean hasAccess(UUID sourceId, Role role) {
        Optional<Role> roleForSource = getRoleForSource(sourceId);
        if (roleForSource.isPresent()) {
            switch(roleForSource.get()) {
                case DEVELOPER:
                    if (role.equals(Role.DEVELOPER)) return true;
                case REPORTER:
                    if (role.equals(Role.REPORTER)) return true;
                default:
                    return false;
            }
        } else {
            return false;
        }
    }
}
