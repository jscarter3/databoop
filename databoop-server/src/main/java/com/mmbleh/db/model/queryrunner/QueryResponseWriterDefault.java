package com.mmbleh.db.model.queryrunner;

import java.util.ArrayList;
import java.util.List;

public class QueryResponseWriterDefault extends QueryResponseWriter {

    private List<DBQueryResult> results = new ArrayList<>();

    @Override
    public synchronized void writeResult(DBQueryResult result) {
        DBQueryResult res = results.stream()
                .filter(r -> r.getDbConnection().equals(result.getDbConnection()))
                .findFirst()
                .orElseGet(() -> {
                    DBQueryResult r = new DBQueryResult();
                    r.setDbConnection(result.getDbConnection());
                    r.setHeader(result.getHeader());
                    results.add(r);
                    return r;
                });
        res.getData().addAll(result.getData());
        res.setError(result.getError());
    }

    @Override
    public synchronized void writeStatus(int done, int total) {
        // do nothing
    }

    @Override
    public synchronized void complete() {
        // do nothing
    }

    public List<DBQueryResult> getResults() {
        return results;
    }
}
