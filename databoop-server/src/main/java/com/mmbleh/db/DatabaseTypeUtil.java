package com.mmbleh.db;

import com.mmbleh.db.exception.DataboopException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.jdbc.DatabaseDriver;

import java.sql.JDBCType;
import java.util.Arrays;
import java.util.List;

public final class DatabaseTypeUtil {

    private static Logger logger = LoggerFactory.getLogger(DatabaseTypeUtil.class);

    public static boolean isReadOnly(String url, String query) {
        return isReadOnly(DatabaseDriver.fromJdbcUrl(url), query);
    }

    public static boolean isReadOnly(DatabaseDriver type, String query) {
        List<String> readOnlyKeywords;
        switch (type) {
            case H2:
            case MYSQL:
            case POSTGRESQL:
                readOnlyKeywords = Arrays.asList("select", "explain");
                break;
            default:
                throw new DataboopException("Unsupported database type");
        }

        for (String keyword : readOnlyKeywords) {
            if (query.toLowerCase().trim().startsWith(keyword)) {
                return true;
            }
        }
        return false;
    }

    public static String getValidationQuery(String url) {
        return getValidationQuery(DatabaseDriver.fromJdbcUrl(url));
    }

    public static String getValidationQuery(DatabaseDriver type) {
        switch (type) {
            case H2:
            case MYSQL:
            case POSTGRESQL:
                return "select 1";
            default:
                throw new DataboopException("Unsupported database type");
        }
    }

    public static String getSqlTypeName(int type) {
        switch (JDBCType.valueOf(type)) {
            case BIT:
            case BOOLEAN:
                return "BOOLEAN";
            case TINYINT:
            case SMALLINT:
            case INTEGER:
            case BIGINT:
                return "LONG";
            case FLOAT:
            case REAL:
            case DOUBLE:
                return "DOUBLE";
            case NUMERIC:
            case DECIMAL:
                return "BIGDECIMAL";
            case CHAR:
            case VARCHAR:
            case LONGVARCHAR:
                return "STRING";
            case DATE:
                return "DATE";
            case TIME:
                return "TIME";
            case TIME_WITH_TIMEZONE:
            case TIMESTAMP:
            case TIMESTAMP_WITH_TIMEZONE:
                return "TIMESTAMP";
            case BINARY:
            case VARBINARY:
            case LONGVARBINARY:
                return "BYTEARRAY";
            case JAVA_OBJECT:
                return "JAVA_OBJECT";
            case NULL:
            case OTHER:
            case DISTINCT:
            case STRUCT:
            case ARRAY:
            case BLOB:
            case CLOB:
            case REF:
            case REF_CURSOR:
            case DATALINK:
            case ROWID:
            case NCHAR:
            case NVARCHAR:
            case LONGNVARCHAR:
            case NCLOB:
            case SQLXML:
                return JDBCType.valueOf(type).getName();
            default:
                logger.error("This shouldn't happen... we have an unmapped data type: " + type);
                return "boop";
        }
    }
}
