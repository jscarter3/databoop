package com.mmbleh.db.access;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class AccessLogWriter {

    private Logger auditLogger = LoggerFactory.getLogger("audit");
    private Logger logger = LoggerFactory.getLogger(AccessLogWriter.class);
    private ObjectMapper objectMapper;

    public AccessLogWriter(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @EventListener
    @Async
    public void onEvent(AccessLogObject accessLogObject) {
        try {
            auditLogger.info(objectMapper.writeValueAsString(accessLogObject));
        } catch (JsonProcessingException e) {
            logger.error("failed to write access log", e);
        }
    }
}
