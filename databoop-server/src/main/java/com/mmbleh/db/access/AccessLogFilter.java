package com.mmbleh.db.access;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Service
public class AccessLogFilter extends OncePerRequestFilter {

    private ApplicationEventPublisher applicationEventPublisher;

    public AccessLogFilter(ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

        long startTime = System.currentTimeMillis();

        try {
            filterChain.doFilter(request, response);
        }
        finally {
            if (!isAsyncStarted(request)) {
                long duration = System.currentTimeMillis() - startTime;
                Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
                String user = authentication == null ? "null" : authentication.getName();
                AccessLogObject alo = new AccessLogObject();
                alo.setRemoteHost(request.getRemoteHost());
                alo.setUser(user);
                alo.setMethod(request.getMethod());
                alo.setUri(request.getRequestURI());
                alo.setQueryString(request.getQueryString());
                alo.setProtocol(request.getProtocol());
                alo.setUserAgent(request.getHeader(HttpHeaders.USER_AGENT));
                alo.setStatus(response.getStatus());
                alo.setDuration(duration);

                applicationEventPublisher.publishEvent(alo);
            }
        }

    }

}
