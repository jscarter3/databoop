package com.mmbleh.db.service;

import com.mmbleh.db.model.UserSource;
import com.mmbleh.db.repository.UserSourceRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class UserSourceService {

    private UserSourceRepository userSourceRepository;

    public UserSourceService(UserSourceRepository userSourceRepository) {
        this.userSourceRepository = userSourceRepository;
    }

    public List<UserSource> listForUser(UUID userId) {
        return userSourceRepository.listForUser(userId);
    }

    public List<UserSource> listForSource(UUID sourceId) {
        return userSourceRepository.listForSource(sourceId);
    }

    public void saveForUser(UUID userId, List<UserSource> userSources) {
        userSources.forEach(userSource -> userSource.setUserId(userId));
        List<UserSource> existing = listForUser(userId);
        update(userSources, existing);
    }

    public void saveForSource(UUID sourceId, List<UserSource> userSources) {
        userSources.forEach(userSource -> userSource.setSourceId(sourceId));
        List<UserSource> existing = listForSource(sourceId);
        update(userSources, existing);
    }

    private void update(List<UserSource> userSources, List<UserSource> existing) {

        List<UserSource> toDelete = existing.stream()
                .filter(existingUserSource -> !userSources.contains(existingUserSource))
                .collect(Collectors.toList());
        userSourceRepository.delete(toDelete);

        userSources.forEach(userSource -> {
            if (existing.contains(userSource)) {
                UserSource existingUserSource = existing.get(existing.indexOf(userSource));
                userSource.setId(existingUserSource.getId());
            }
        });
        userSourceRepository.createOrUpdate(userSources);
    }
}
