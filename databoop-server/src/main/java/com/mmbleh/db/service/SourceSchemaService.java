package com.mmbleh.db.service;

import com.mmbleh.db.exception.DataboopException;
import com.mmbleh.db.model.*;
import com.mmbleh.db.repository.SourceSchemaRepository;
import com.zaxxer.hikari.pool.HikariPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

@Service
public class SourceSchemaService {

    private Logger logger = LoggerFactory.getLogger(SourceSchemaService.class);

    private DataSourceService dataSourceService;
    private DBConnectionService dbConnectionService;
    private SourceSchemaRepository sourceSchemaRepository;

    public SourceSchemaService(DataSourceService dataSourceService,
                               DBConnectionService dbConnectionService,
                               SourceSchemaRepository sourceSchemaRepository) {
        this.dataSourceService = dataSourceService;
        this.dbConnectionService = dbConnectionService;
        this.sourceSchemaRepository = sourceSchemaRepository;
    }

    public List<DBTable> getSchema(Source source, boolean force) {
        SourceSchema schema = null;
        try {
            schema = sourceSchemaRepository.retrieveForSource(source.getId());
        } catch (EmptyResultDataAccessException e) {
            // swallow
        }
        if (schema == null || force) {
            if (schema == null) {
                schema = new SourceSchema();
                schema.setSourceId(source.getId());
            }
            schema.setSchema(buildSchema(source));
            schema.setLastUpdated(new Date());
            sourceSchemaRepository.createOrUpdate(schema);
        }
        return schema.getSchema();
    }

    private List<DBTable> buildSchema(Source source) {
        List<DBConnection> connections = dbConnectionService.listForSource(source.getId());

        if (connections.size() == 0) {
            throw new DataboopException("No database connections are defined for this source");
        }

        Collections.shuffle(connections);
        List<DBTable> tables = null;
        for (DBConnection dbConnection : connections) {

            DataSource ds = null;
            Connection connection = null;
            try {
                ds = dataSourceService.getDataSource(dbConnection, source);

                connection = DataSourceUtils.getConnection(ds);
                DatabaseMetaData metaData = connection.getMetaData();

                tables = getDBTables(metaData);

                for (DBTable table : tables) {
                    populateDBColumns(metaData, table);
                }
                break;

            } catch (HikariPool.PoolInitializationException err) { // This one sometimes happens, not worth a full stack trace
                logger.error(dbConnection.getName() + ": " + err.getMessage());
            } catch (Exception err) {
                logger.error(err.getMessage(), err);
            } finally {
                DataSourceUtils.releaseConnection(connection, ds);
            }
        }
        if (tables == null) {
            throw new DataboopException("Unable to load schema");
        }

        return tables;
    }

    private List<DBTable> getDBTables(DatabaseMetaData metaData) throws SQLException {

        ResultSet rs = metaData.getTables(null, null, null, new String[]{"TABLE"});

        List<DBTable> tables = new ArrayList<>();
        while (rs.next()) {
            tables.add(new DBTable(rs.getString("TABLE_NAME")));
        }
        return tables;
    }

    private void populateDBColumns(DatabaseMetaData metaData, DBTable table) throws SQLException {
        ResultSet rs = metaData.getColumns(null, null, table.getName(), null);
        List<DBTableColumn> columns = new ArrayList<>();
        while (rs.next()) {
            columns.add(new DBTableColumn(
                    rs.getString("COLUMN_NAME"),
                    rs.getString("TYPE_NAME"),
                    rs.getString("COLUMN_SIZE")));
        }
        table.setColumns(columns);
    }
}
