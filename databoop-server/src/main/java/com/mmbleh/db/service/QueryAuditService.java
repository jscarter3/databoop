package com.mmbleh.db.service;

import com.mmbleh.db.model.QueryAudit;
import com.mmbleh.db.model.QueryAuditSummary;
import com.mmbleh.db.repository.QueryAuditRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class QueryAuditService {

    private QueryAuditRepository queryAuditRepository;

    public QueryAuditService(QueryAuditRepository queryAuditRepository) {
        this.queryAuditRepository = queryAuditRepository;
    }

    public List<QueryAuditSummary> getSummary() {
        return queryAuditRepository.getSummary();
    }

    public List<QueryAudit> getForGroup(UUID queryGroup) {
        return queryAuditRepository.listForGroup(queryGroup);
    }

    public void save(QueryAudit query) {
        queryAuditRepository.save(query);
    }
}
