package com.mmbleh.db.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.security.crypto.encrypt.TextEncryptor;
import org.springframework.stereotype.Service;

import java.math.BigInteger;

@Service
public class CryptoService {

    private TextEncryptor encryptor;

    public CryptoService(@Value("${crypto.password}") String password, @Value("${crypto.salt}") String salt) {
        encryptor = Encryptors.text(password, String.format("%x", new BigInteger(1, salt.getBytes())));
    }

    public String encrypt(String input) {
        return encryptor.encrypt(input);
    }

    public String decrypt(String input) {
        return encryptor.decrypt(input);
    }
}
