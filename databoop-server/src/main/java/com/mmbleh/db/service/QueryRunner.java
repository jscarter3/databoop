package com.mmbleh.db.service;

import com.mmbleh.db.DatabaseTypeUtil;
import com.mmbleh.db.exception.WebSocketDisconnectedException;
import com.mmbleh.db.model.*;
import com.mmbleh.db.model.queryrunner.DBQueryResult;
import com.mmbleh.db.model.queryrunner.QueryRequestData;
import com.mmbleh.db.model.queryrunner.ResultHeaderElement;
import com.mmbleh.db.model.queryrunner.ResultRow;
import com.zaxxer.hikari.pool.HikariPool;
import org.apache.commons.lang3.StringUtils;
import org.postgresql.util.PGobject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.jdbc.support.JdbcUtils;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.security.SecureRandom;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class QueryRunner {

    private Logger logger = LoggerFactory.getLogger(QueryRunner.class);
    private SecureRandom random = new SecureRandom();
    private DataSourceService dataSourceService;
    private QueryAuditService queryAuditService;

    public QueryRunner(DataSourceService dataSourceService,
                       QueryAuditService queryAuditService) {
        this.dataSourceService = dataSourceService;
        this.queryAuditService = queryAuditService;
    }

    public void query(List<DBConnection> dbConnections, Source source, QueryRequestData queryData) {

        UUID queryGroup = UUID.randomUUID();

        AtomicInteger completedConnections = new AtomicInteger();

        ForkJoinPool pool = new ForkJoinPool(queryData.getQueryPoolSize());

        queryData.getResponseWriter().writeStatus(0, queryData.getNumberOfConnections());

        CompletableFuture[] cfs = dbConnections.stream()
                .map(dbConnection -> CompletableFuture.runAsync(() -> runQuery(dbConnection, source, queryGroup, queryData, completedConnections), pool))
                .toArray(CompletableFuture[]::new);

        CompletableFuture.allOf(cfs).join();
        queryData.getResponseWriter().complete();
        pool.shutdown();
    }

    private void runQuery(DBConnection dbConnection, Source source, UUID queryGroup, QueryRequestData queryData, AtomicInteger completedConnections) {

        DBQueryResult result = new DBQueryResult();
        result.setDbConnection(dbConnection);

        DataSource ds = null;
        Connection connection = null;
        Statement statement = null;
        Long queryDuration = null;
        try {
            if (queryData.getQueryPoolSize() > 1) {
                Thread.sleep(random.nextInt(queryData.getQueryPoolSize() * 15)); // linearly increase delay with number of tenants so we spread out the load on the db server
            }

            ds = dataSourceService.getDataSource(dbConnection, source);

            connection = DataSourceUtils.getConnection(ds);
            statement = connection.createStatement();

            if (source != null && source.getQueryTimeout() > 0) {
                statement.setQueryTimeout(source.getQueryTimeout());
            }

            long queryStart = System.currentTimeMillis();

            if (DatabaseTypeUtil.isReadOnly(dbConnection.getDbUrl(), queryData.getQuery())) {
                Pattern limitFinder = Pattern.compile(".*\\slimit\\s+.*");
                Matcher m = limitFinder.matcher(queryData.getQuery().toLowerCase());
                if (!m.find()) {
                    statement.setMaxRows(1000);
                }

                ResultSet rs = statement.executeQuery(queryData.getQuery());
                ResultSetMetaData metaData = rs.getMetaData();

                List<ResultHeaderElement> header = new ArrayList<>();
                if (source.isMultiTenant()) {
                    header.add(0, new ResultHeaderElement("Connection", "STRING"));
                }
                for (int i = 1; i <= metaData.getColumnCount(); i++) {
                    header.add(new ResultHeaderElement(metaData.getColumnLabel(i), DatabaseTypeUtil.getSqlTypeName(metaData.getColumnType(i))));
                }

                result.setHeader(header);

                List<ResultRow> resultRows = new ArrayList<>();

                while (rs.next()) {
                    ResultRow dataRow = new ResultRow();

                    if (source.isMultiTenant()) dataRow.add(dbConnection.getName());

                    for (int i = 1; i <= metaData.getColumnCount(); i++) {
                        Object val = rs.getObject(i);
                        if (val instanceof BigDecimal) {
                            val = ((BigDecimal) val).toPlainString();
                        } else if (val instanceof PGobject) {
                            val = ((PGobject) val).getValue();
                        } else if (val instanceof Clob) {
                            val = rs.getString(i);
                        }
                        dataRow.add(val);
                    }
                    resultRows.add(dataRow);
                    if (resultRows.size() >= 100) {
                        result.setData(resultRows);
                        queryData.getResponseWriter().writeResult(result);
                        resultRows.clear();
                    }
                }
                if (resultRows.size() > 0) {
                    result.setData(resultRows);
                    queryData.getResponseWriter().writeResult(result);
                    resultRows.clear();
                }

                rs.close();

            }
            else if (!source.isReadOnly()) {
                int updateCount = statement.executeUpdate(queryData.getQuery());

                List<ResultHeaderElement> header = new ArrayList<>();
                if (source.isMultiTenant()) {
                    header.add(0, new ResultHeaderElement("Connection", "STRING"));
                }
                header.add(new ResultHeaderElement("Updated Rows", "LONG"));

                result.setHeader(header);

                List<ResultRow> resultRows = new ArrayList<>();
                ResultRow dataRow = new ResultRow();

                if (source.isMultiTenant()) dataRow.add(dbConnection.getName());
                dataRow.add(updateCount);

                resultRows.add(dataRow);

                if (resultRows.size() > 0) {
                    result.setData(resultRows);
                    queryData.getResponseWriter().writeResult(result);
                    resultRows.clear();
                }
            } else {
                throw new Exception("Only SELECT queries are allowed");
            }

            queryDuration = System.currentTimeMillis() - queryStart;

        } catch (HikariPool.PoolInitializationException err) { // This one sometimes happens, not worth a full stack trace
            logger.error(dbConnection.getName() + ": " + err.getMessage());
            result.setError(err.getMessage());
        } catch (SQLSyntaxErrorException err) { // Syntax error
            logger.warn(dbConnection.getName() + ": " + err.getMessage());
            result.setError(err.getMessage());
        } catch (WebSocketDisconnectedException err) {
            logger.debug(err.getLocalizedMessage());
        } catch (Exception err) {
            logger.error(err.getMessage(), err);
            result.setError(err.getMessage());
        } finally {
            JdbcUtils.closeStatement(statement);
            DataSourceUtils.releaseConnection(connection, ds);
        }

        if (StringUtils.isNotEmpty(result.getError())) {
            queryData.getResponseWriter().writeResult(result);
        }
        queryData.getResponseWriter().writeStatus(completedConnections.incrementAndGet(), queryData.getNumberOfConnections());

        QueryAudit queryAudit = new QueryAudit();
        queryAudit.setQueryGroup(queryGroup);
        queryAudit.setQuery(queryData.getQuery());
        queryAudit.setQueryDuration(queryDuration);
        queryAudit.setSourceName(source.getName());
        queryAudit.setDbName(dbConnection.getName());
        queryAudit.setDbUrl(dbConnection.getDbUrl());
        queryAudit.setUsername(queryData.getCurrentUser());
        queryAudit.setCreated(new Date());
        queryAuditService.save(queryAudit);
    }
}
