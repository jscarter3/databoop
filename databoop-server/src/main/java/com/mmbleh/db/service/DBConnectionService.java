package com.mmbleh.db.service;

import com.mmbleh.db.exception.DataboopException;
import com.mmbleh.db.model.*;
import com.mmbleh.db.model.queryrunner.DBQueryResult;
import com.mmbleh.db.model.queryrunner.QueryRequestData;
import com.mmbleh.db.model.queryrunner.QueryResponseWriterDefault;
import com.mmbleh.db.model.queryrunner.ResultRow;
import com.mmbleh.db.repository.DBConnectionRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class DBConnectionService {

    private DBConnectionRepository dbConnectionRepository;
    private QueryRunner queryRunner;
    private DataSourceService dataSourceService;

    public DBConnectionService(DBConnectionRepository dbConnectionRepository,
                               QueryRunner queryRunner,
                               DataSourceService dataSourceService) {
        this.dbConnectionRepository = dbConnectionRepository;
        this.queryRunner = queryRunner;
        this.dataSourceService = dataSourceService;
    }

    public List<DBConnection> listForSource(UUID sourceId) {
        return dbConnectionRepository.listForSource(sourceId);
    }

    public void refreshMultiTenantConnections(Source source) {
        dataSourceService.evictForSource(source.getId());

        MultiTenantSource mtSource = source.getMultiTenantSource();

        if (StringUtils.isAnyEmpty(mtSource.getDbUrl(), mtSource.getQuery())) {
            throw new DataboopException("DB Url and Query are required");
        }

        DBConnection dbConnection = new DBConnection();
        dbConnection.setDbUrl(mtSource.getDbUrl());
        dbConnection.setDbUsername(mtSource.getDbUsername());
        dbConnection.setDbPassword(mtSource.getDbPassword());

        QueryRequestData queryData = new QueryRequestData();
        queryData.setCurrentUser(SecurityContextHolder.getContext().getAuthentication().getName());
        queryData.setNumberOfConnections(1);
        queryData.setConcurrentQueries(1);
        queryData.setQuery(mtSource.getQuery());
        QueryResponseWriterDefault responseWriter = new QueryResponseWriterDefault();
        queryData.setResponseWriter(responseWriter);

        queryRunner.query(Collections.singletonList(dbConnection), source, queryData);

        DBQueryResult queryResult = responseWriter.getResults().get(0);

        if (StringUtils.isNotEmpty(queryResult.getError())) {
            throw new DataboopException(queryResult.getError());
        }

        List<DBConnection> newConnections = new ArrayList<>();
        for (ResultRow row : queryResult.getData()) {
            DBConnection mtConnection = new DBConnection();
            mtConnection.setSourceId(source.getId());
            mtConnection.setName((String) row.get(queryResult.getHeaderIndexByName("name")));
            mtConnection.setDbUrl((String) row.get(queryResult.getHeaderIndexByName("jdbc_url")));
            if (mtSource.getCredentialSource().equals(MultiTenantSource.CredentialSource.QUERY)) {
                mtConnection.setDbUsername((String) row.get(queryResult.getHeaderIndexByName("jdbc_username")));
                mtConnection.setDbPassword((String) row.get(queryResult.getHeaderIndexByName("jdbc_password")));
            } else {
                mtConnection.setDbUsername(mtSource.getTenantUsername());
                mtConnection.setDbPassword(mtSource.getTenantPassword());
            }

            newConnections.add(mtConnection);
        }

        List<DBConnection> existingConnections = listForSource(source.getId());

        List<DBConnection> connectionsToRemove = existingConnections.stream()
                .filter(mtc -> !newConnections.contains(mtc))
                .collect(Collectors.toList());
        dbConnectionRepository.delete(connectionsToRemove);

        newConnections.forEach(connection -> {
            if (existingConnections.contains(connection)) {
                DBConnection existingConnection = existingConnections.get(existingConnections.indexOf(connection));
                connection.setId(existingConnection.getId());
                connection.setActive(existingConnection.isActive()); // If it was de-activated, honor that
            }
        });
        dbConnectionRepository.createOrUpdate(newConnections);
    }

    public DBConnection retrieve(UUID id) {
        return dbConnectionRepository.retrieve(id);
    }

    public void save(List<DBConnection> dbConnections) {
        dbConnectionRepository.createOrUpdate(dbConnections);
        dataSourceService.evict(dbConnections.stream().map(DBConnection::getId).collect(Collectors.toList()));
    }

    public void delete(List<DBConnection> dbConnections) {
        dbConnectionRepository.delete(dbConnections);
        dataSourceService.evict(dbConnections.stream().map(DBConnection::getId).collect(Collectors.toList()));
    }
}
