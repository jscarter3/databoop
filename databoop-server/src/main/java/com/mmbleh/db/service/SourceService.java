package com.mmbleh.db.service;

import com.mmbleh.db.model.Source;
import com.mmbleh.db.repository.SourceRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class SourceService {

    private SourceRepository sourceRepository;

    public SourceService(SourceRepository sourceRepository) {
        this.sourceRepository = sourceRepository;
    }

    public List<Source> list() {
        return sourceRepository.list();
    }

    public Source retrieve(UUID id) {
        return sourceRepository.retrieve(id);
    }

    public void save(Source source) {
        sourceRepository.createOrUpdate(source);
    }

    public void delete(UUID sourceId) {
        sourceRepository.delete(sourceId);
    }

}
