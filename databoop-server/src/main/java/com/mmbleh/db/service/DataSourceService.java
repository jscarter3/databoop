package com.mmbleh.db.service;

import com.mmbleh.db.model.DBConnection;
import com.mmbleh.db.model.Source;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class DataSourceService {

    private Logger logger = LoggerFactory.getLogger(DataSourceService.class);

    private ConcurrentHashMap<DBConnection, HikariDataSource> dsMap = new ConcurrentHashMap<>();

    public HikariDataSource getDataSource(DBConnection dbConnection, Source source) {
        if (dbConnection.getSourceId() == null || dbConnection.getName() == null) {
            // Should only be multi tenant source queries so they are never cached
            return createDatasource(dbConnection, source);
        }
//        else if (!dsMap.containsKey(dbConnection)) {
//            dsMap.put(dbConnection, createDatasource(dbConnection));
//        }
        dsMap.computeIfAbsent(dbConnection, (dbConn) -> createDatasource(dbConn, source));
        return dsMap.get(dbConnection);
    }

    public void evictForSource(UUID sourceId) {
        dsMap.entrySet().stream()
                .filter(entry -> entry.getKey().getSourceId().equals(sourceId))
                .forEach(entry -> {
                    entry.getValue().close();
                    dsMap.remove(entry.getKey());
                });
    }

    public void evict(List<UUID> dbConnections) {
        dsMap.entrySet().stream()
                .filter(entry -> dbConnections.contains(entry.getKey().getId()))
                .forEach(entry -> {
                    entry.getValue().close();
                    dsMap.remove(entry.getKey());
                });
    }

    private HikariDataSource createDatasource(DBConnection dbConnection, Source source) {

        HikariConfig config = new HikariConfig();
        config.setPoolName((source == null ? "" : source.getName()) + "_" + dbConnection.getName());
        config.setJdbcUrl(dbConnection.getDbUrl());
        config.setUsername(dbConnection.getDbUsername());
        config.setPassword(dbConnection.getDbPassword());
        config.setMaximumPoolSize(source == null ? 1 : source.getPoolSize());
        config.setMinimumIdle(0);
        config.setIdleTimeout(5 * 60 * 1000); // 5 minutes
        config.setConnectionTimeout(5000);
        config.setValidationTimeout(1000);
        return new HikariDataSource(config);
    }

    @PreDestroy
    public void cleanup() {
        logger.info("Cleaning up all database connections");
        dsMap.forEach((tenant, dataSource) -> dataSource.close());
    }
}
