package com.mmbleh.db.service;

import com.mmbleh.db.model.DataboopUser;
import com.mmbleh.db.repository.DataboopUserRepository;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class DataboopUserService implements UserDetailsService {

    private DataboopUserRepository userRepository;
    private UserSourceService userSourceService;

    public DataboopUserService(DataboopUserRepository userRepository,
                               UserSourceService userSourceService) {
        this.userRepository = userRepository;
        this.userSourceService = userSourceService;
    }

    @Override
    public DataboopUser loadUserByUsername(String username) throws UsernameNotFoundException {
        DataboopUser user = userRepository.retrieveByUsername(username);
        if (user == null) {
            return null;
        } else if (user.isAdmin()) {
            user.getAuthorities().add(new SimpleGrantedAuthority("ROLE_ADMIN"));
        } else {
            user.getAuthorities().add(new SimpleGrantedAuthority("ROLE_USER"));
            user.setUserSources(userSourceService.listForUser(user.getId()));
        }
        return user;
    }

    public DataboopUser retrieve(UUID id) {
        return userRepository.retrieve(id);
    }

    public List<DataboopUser> list() {
        return userRepository.list();
    }

    public void save(DataboopUser user) {
        userRepository.createOrUpdate(user);
    }

    public void delete(UUID userId) {
        userRepository.delete(userId);
    }
}
