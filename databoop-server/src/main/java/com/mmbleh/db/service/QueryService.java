package com.mmbleh.db.service;

import com.mmbleh.db.model.Query;
import com.mmbleh.db.repository.QueryRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class QueryService {

    private QueryRepository queryRepository;

    public QueryService(QueryRepository queryRepository) {
        this.queryRepository = queryRepository;
    }

    public List<Query> listForSource(UUID sourceId) {
        return queryRepository.retrieveBySourceId(sourceId);
    }

    public Query retrieve(UUID id) {
        return queryRepository.retrieve(id);
    }

    public void save(Query query) {
        queryRepository.createOrUpdate(query);
    }

    public void delete(UUID id) {
        queryRepository.delete(id);
    }

}
