package com.mmbleh.db.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mmbleh.db.DatabaseTypeUtil;
import com.mmbleh.db.exception.DataboopException;
import com.mmbleh.db.model.*;
import com.mmbleh.db.model.queryrunner.QueryRequestData;
import com.mmbleh.db.model.queryrunner.QueryResponseWriterWS;
import com.mmbleh.db.service.DBConnectionService;
import com.mmbleh.db.service.QueryRunner;
import com.mmbleh.db.service.QueryService;
import com.mmbleh.db.service.SourceService;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@Service
public class WSQueryHandler extends TextWebSocketHandler {

    private SourceService sourceService;
    private DBConnectionService dbConnectionService;
    private QueryService queryService;
    private QueryRunner queryRunner;
    private ObjectMapper objectMapper;

    public WSQueryHandler(SourceService sourceService,
                          DBConnectionService dbConnectionService,
                          QueryService queryService,
                          QueryRunner queryRunner,
                          ObjectMapper objectMapper) {
        this.sourceService = sourceService;
        this.dbConnectionService = dbConnectionService;
        this.queryService = queryService;
        this.queryRunner = queryRunner;
        this.objectMapper = objectMapper;
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        super.afterConnectionEstablished(session);
    }

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        DataboopUser user = getUser(session);

        Query query = objectMapper.readValue(message.getPayload(), Query.class);

        // If there is a query id, load the query. If not, make sure there is a source ID to check access
        if (query.getId() != null) {
            query = queryService.retrieve(query.getId());
        } else if (query.getSourceId() == null) {
            throw new DataboopException("Query ID or Source ID is required");
        }

        // Throw exception if user doesn't have access to source
        // If user is a Reporter, they MUST supply a query ID, not run ad-hoc sql
        if (!user.isAdmin()) {
            if (!user.hasAccess(query.getSourceId())) {
                throw new InsufficientAuthenticationException("Insufficient permission");
            } else if (query.getId() == null && Role.REPORTER.equals(user.getRoleForSource(query.getSourceId()).get())) {
                throw new InsufficientAuthenticationException("Query ID required");
            }
        }

        Source source = sourceService.retrieve(query.getSourceId());
        List<DBConnection> connections = dbConnectionService.listForSource(source.getId())
                .stream()
                .filter(DBConnection::isActive)
                .collect(Collectors.toList());
        if (query.getConnectionIds() != null && query.getConnectionIds().size() > 0) {
            List<UUID> connIds = query.getConnectionIds();
            connIds.remove(null);
            if (connIds.size() > 0) {
                connections = connections.stream()
                        .filter(connection -> connIds.contains(connection.getId()))
                        .collect(Collectors.toList());
            }
        }
        List<DBConnection> conns = connections;

        QueryRequestData queryData = new QueryRequestData();
        queryData.setCurrentUser(user.getName());
        queryData.setNumberOfConnections(conns.size());
        queryData.setConcurrentQueries(query.isTestQuery() ? conns.size() : source.getConcurrentQueries());
        queryData.setQuery(query.isTestQuery() ? DatabaseTypeUtil.getValidationQuery(conns.get(0).getDbUrl()) : query.getQuery());

        QueryResponseWriterWS responseWriter = new QueryResponseWriterWS(session);
        queryData.setResponseWriter(responseWriter);

        CompletableFuture.runAsync(() -> queryRunner.query(conns, source, queryData));
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        super.afterConnectionClosed(session, status);
    }

    private DataboopUser getUser(WebSocketSession session) {
        SecurityContext context = (SecurityContext) session.getAttributes().get("SPRING_SECURITY_CONTEXT");
        return (DataboopUser) context.getAuthentication().getPrincipal();
    }
}
