package com.mmbleh.db.controller;

import com.mmbleh.db.model.*;
import com.mmbleh.db.service.QueryAuditService;
import com.mmbleh.db.service.QueryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/query")
public class QueryController {

    private Logger logger = LoggerFactory.getLogger(QueryController.class);

    private QueryService queryService;
    private QueryAuditService queryAuditService;

    public QueryController(QueryService queryService,
                           QueryAuditService queryAuditService) {
        this.queryService = queryService;
        this.queryAuditService = queryAuditService;
    }

    @GetMapping("/{queryId}")
    public Query retrieveQuery(@PathVariable UUID queryId, Authentication authentication) {
        Query query = queryService.retrieve(queryId);

        DataboopUser user = (DataboopUser) authentication.getPrincipal();
        if (!user.isAdmin() && !user.hasAccess(query.getSourceId(), Role.DEVELOPER)) {
            throw new InsufficientAuthenticationException("Insufficient permission");
        }
        return queryService.retrieve(queryId);
    }

    @PostMapping
    @PreAuthorize("hasRole('ADMIN') or principal.hasAccess(#query.sourceId, 'DEVELOPER')")
    public Query saveQuery(@RequestBody Query query) {
        queryService.save(query);
        return query;
    }

    @DeleteMapping("/{queryId}")
    public void deleteQuery(@PathVariable UUID queryId, Authentication authentication) {
        Query query = queryService.retrieve(queryId);

        DataboopUser user = (DataboopUser) authentication.getPrincipal();
        if (!user.isAdmin() && !user.hasAccess(query.getSourceId(), Role.DEVELOPER.name())) {
            throw new InsufficientAuthenticationException("Insufficient permission");
        }
        queryService.delete(queryId);
    }

    @GetMapping(path = "/audit")
    public List<QueryAuditSummary> audit() {
        return queryAuditService.getSummary();
    }

    @GetMapping(path = "/audit/{queryGroupId}")
    public List<QueryAudit> auditDetailsForGroup(@PathVariable UUID queryGroupId) {
        return queryAuditService.getForGroup(queryGroupId);
    }
}
