package com.mmbleh.db.controller;

import com.mmbleh.db.exception.DataboopException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import javax.annotation.PostConstruct;
import java.util.*;

@RestControllerAdvice
public class ErrorHandler {

    private Logger logger = LoggerFactory.getLogger(ErrorHandler.class);
    private Map<Class<?>, ErrorMessage> statusCodes = new HashMap<>();
    private Set<Class<?>> dontLog = new HashSet<>();

    @PostConstruct
    public void init() {
        statusCodes.put(BadCredentialsException.class, new ErrorMessage(HttpStatus.BAD_REQUEST, "Incorrect username or password"));

        dontLog.add(BadCredentialsException.class);
        dontLog.add(DataboopException.class);
    }

    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<Object> handleAccessDeniedException(Exception ex, WebRequest request) {
        if (!dontLog.contains(ex.getClass())) {
            logger.error(ex.getLocalizedMessage(), ex);
        }

        ErrorMessage errorMessage = statusCodes.get(ex.getClass());

        if (errorMessage == null) {
            return new ResponseEntity<>(
                    ex.getLocalizedMessage(),
                    new HttpHeaders(),
                    HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            return new ResponseEntity<>(
                    errorMessage.message,
                    new HttpHeaders(),
                    errorMessage.status);
        }

    }

    private Throwable getRootCause(Throwable throwable) {
        if (throwable.getCause() != null)
            return getRootCause(throwable.getCause());

        return throwable;
    }

    private class ErrorMessage {
        public HttpStatus status;
        public String message;

        public ErrorMessage(HttpStatus status, String message) {
            this.status = status;
            this.message = message;
        }
    }
}
