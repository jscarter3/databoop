package com.mmbleh.db.controller;

import com.mmbleh.db.model.DataboopUser;
import com.mmbleh.db.service.DataboopUserService;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.HashMap;

@RestController
@RequestMapping("/api")
public class AuthController {

    private AuthenticationManager authenticationManager;
    private DataboopUserService userService;
    private PasswordEncoder passwordEncoder;

    public AuthController(AuthenticationManager authenticationManager,
                          DataboopUserService userService,
                          PasswordEncoder passwordEncoder) {
        this.authenticationManager = authenticationManager;
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
    }

    @PostMapping("/login")
    public DataboopUser login(HttpServletRequest request,
                      @RequestBody HashMap<String, String> loginRequest) {

        if (loginRequest.get("username").toLowerCase().equals("admin")) {
            try {
                userService.loadUserByUsername("admin");
            } catch (EmptyResultDataAccessException e) {
                createAdminUser();
            }
        }

        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(loginRequest.get("username").toLowerCase(), loginRequest.get("password"));
        token.setDetails(new WebAuthenticationDetails(request));

        Authentication authentication;
        try {
            authentication = authenticationManager.authenticate(token);
        } catch (InternalAuthenticationServiceException e) {
            throw new BadCredentialsException(e.getLocalizedMessage(), e);
        }
        SecurityContext securityContext = SecurityContextHolder.getContext();
        securityContext.setAuthentication(authentication);

        HttpSession session = request.getSession(true);
        session.setAttribute("SPRING_SECURITY_CONTEXT", securityContext);

        return (DataboopUser) authentication.getPrincipal();
    }

    @PostMapping("/logout")
    public void logout(HttpServletRequest request) {
        SecurityContextHolder.clearContext();
        request.getSession().invalidate();
    }

    private void createAdminUser() {
        DataboopUser user = new DataboopUser();
        user.setUsername("admin");
        user.setName("Admin Account");
//        user.setEmail("admin");
        user.setPassword(passwordEncoder.encode("admin"));
        user.setAdmin(true);
        user.setActive(true);
        user.setCreated(new Date());
        userService.save(user);
    }

}
