package com.mmbleh.db.controller;

import com.mmbleh.db.exception.DataboopException;
import com.mmbleh.db.model.DataboopUser;
import com.mmbleh.db.model.UserSource;
import com.mmbleh.db.service.DataboopUserService;
import com.mmbleh.db.service.UserSourceService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/user")
public class DataboopUserController {

    private DataboopUserService databoopUserService;
    private PasswordEncoder passwordEncoder;
    private UserSourceService userSourceService;

    public DataboopUserController(DataboopUserService databoopUserService,
                                  PasswordEncoder passwordEncoder,
                                  UserSourceService userSourceService) {
        this.databoopUserService = databoopUserService;
        this.passwordEncoder = passwordEncoder;
        this.userSourceService = userSourceService;
    }

    @GetMapping
    @PreAuthorize("hasRole('ADMIN')")
    public List<DataboopUser> list() {
        return databoopUserService.list();
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public DataboopUser retrieve(@PathVariable UUID id) {
        return databoopUserService.retrieve(id);
    }

    @PostMapping
    @PreAuthorize("hasRole('ADMIN')")
    public DataboopUser save(@RequestBody DataboopUser newUser) {
        if (StringUtils.isEmpty(newUser.getUsername())) {
            throw new DataboopException("Username is required");
        }
        DataboopUser user;
        if (newUser.getId() == null) {
            user = newUser;
        } else {
            user = databoopUserService.retrieve(newUser.getId());
            user.setUsername(newUser.getUsername());
            user.setName(newUser.getName());
            user.setEmail(newUser.getEmail());
            user.setAdmin(newUser.isAdmin());
            user.setActive(newUser.isActive());
        }
        if (StringUtils.isNotEmpty(newUser.getPassword())) {
            user.setPassword(passwordEncoder.encode(newUser.getPassword()));
        }
        databoopUserService.save(user);
        return user;
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public void delete(@PathVariable UUID id) {
        databoopUserService.delete(id);
    }

    @GetMapping("/{id}/sources")
    @PreAuthorize("hasRole('ADMIN')")
    public List<UserSource> listSourcesForUser(@PathVariable UUID id) {
        return userSourceService.listForUser(id);
    }

    @PostMapping("/{id}/sources")
    @PreAuthorize("hasRole('ADMIN')")
    public List<UserSource> saveSourcesForUser(@PathVariable UUID id,
                                         @RequestBody List<UserSource> userSources) {
        userSourceService.saveForUser(id, userSources);
        return userSources;
    }
}
