package com.mmbleh.db.controller;

import com.mmbleh.db.exception.DataboopException;
import com.mmbleh.db.model.*;
import com.mmbleh.db.service.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/source")
public class SourceController {

    private SourceService sourceService;
    private UserSourceService userSourceService;
    private DBConnectionService dbConnectionService;
    private QueryService queryService;
    private SourceSchemaService dbMetadataService;

    public SourceController(SourceService sourceService,
                            UserSourceService userSourceService,
                            DBConnectionService dbConnectionService,
                            QueryService queryService,
                            SourceSchemaService dbMetadataService) {
        this.sourceService = sourceService;
        this.userSourceService = userSourceService;
        this.dbConnectionService = dbConnectionService;
        this.queryService = queryService;
        this.dbMetadataService = dbMetadataService;
    }

    @GetMapping
    public List<Source> listSources(Authentication authentication) {
        List<Source> sources = sourceService.list();
        DataboopUser user = (DataboopUser) authentication.getPrincipal();
        if (!user.isAdmin()) {
            List<UUID> userSources = userSourceService.listForUser(user.getId()).stream()
                    .map(UserSource::getSourceId)
                    .collect(Collectors.toList());
            sources = sources.stream()
                    .filter(source -> userSources.contains(source.getId()))
                    .collect(Collectors.toList());
        }
        return sources;
    }

    @GetMapping("/{sourceId}")
    @PreAuthorize("hasRole('ADMIN') or principal.hasAccess(#sourceId, 'REPORTER')")
    public Source retrieveSource(@PathVariable UUID sourceId) {
        return sourceService.retrieve(sourceId);
    }

    @PostMapping
    @PreAuthorize("hasRole('ADMIN')")
    public Source saveSource(@RequestBody Source newSource) {
        if (StringUtils.isEmpty(newSource.getName())) {
            throw new DataboopException("Name is required");
        }
        Source source;
        if (newSource.getId() == null) {
            source = newSource;
        } else {
            source = sourceService.retrieve(newSource.getId());
            source.setName(newSource.getName());
            source.setReadOnly(newSource.isReadOnly());
            source.setPoolSize(newSource.getPoolSize());
            source.setQueryTimeout(newSource.getQueryTimeout());
            source.setMultiTenant(newSource.isMultiTenant());
            source.setConcurrentQueries(newSource.getConcurrentQueries());
            if (newSource.getMultiTenantSource() != null) {
                if (source.getMultiTenantSource() == null) source.setMultiTenantSource(new MultiTenantSource());
                MultiTenantSource mtSource = source.getMultiTenantSource();
                MultiTenantSource newMtSource = newSource.getMultiTenantSource();
                mtSource.setSourceId(source.getId());
                mtSource.setDbUrl(newMtSource.getDbUrl());
                mtSource.setDbUsername(newMtSource.getDbUsername());
                if (StringUtils.isNotEmpty(newMtSource.getDbPassword())) mtSource.setDbPassword(newMtSource.getDbPassword());
                mtSource.setTenantUsername(newMtSource.getTenantUsername());
                if (StringUtils.isNotEmpty(newMtSource.getTenantPassword())) mtSource.setTenantPassword(newMtSource.getTenantPassword());

                mtSource.setQuery(newMtSource.getQuery());
                mtSource.setCredentialSource(newMtSource.getCredentialSource());
            }
        }
        sourceService.save(source);
        return source;
    }

    @DeleteMapping("/{sourceId}")
    @PreAuthorize("hasRole('ADMIN')")
    public void deleteSource(@PathVariable UUID sourceId) {
        sourceService.delete(sourceId);
    }

    @GetMapping("/{sourceId}/users")
    @PreAuthorize("hasRole('ADMIN')")
    public List<UserSource> listUsersForSource(@PathVariable UUID sourceId) {
        return userSourceService.listForSource(sourceId);
    }

    @PostMapping("/{sourceId}/users")
    @PreAuthorize("hasRole('ADMIN')")
    public List<UserSource> saveUsersForSource(@PathVariable UUID sourceId,
                                         @RequestBody List<UserSource> userSources) {
        userSourceService.saveForSource(sourceId, userSources);
        return userSources;
    }

    @GetMapping("/{sourceId}/connection")
    @PreAuthorize("hasRole('ADMIN') or principal.hasAccess(#sourceId, 'DEVELOPER')")
    public List<DBConnection> listConnections(@PathVariable UUID sourceId) {
        return dbConnectionService.listForSource(sourceId);
    }

    @PostMapping("/{sourceId}/connection")
    @PreAuthorize("hasRole('ADMIN')")
    public DBConnection saveConnection(@PathVariable UUID sourceId, @RequestBody DBConnection newConnection) {
        DBConnection dbConnection;

        Source source = sourceService.retrieve(sourceId);
        if (source == null) {
            throw new DataboopException("No source found");
        }

        if (newConnection.getId() == null) {
            dbConnection = newConnection;
            dbConnection.setSourceId(source.getId());
        } else {
            if (source.isMultiTenant()) {
                dbConnection = dbConnectionService.retrieve(newConnection.getId());
            } else {
                dbConnection = dbConnectionService.listForSource(sourceId).get(0);
            }
            dbConnection.setName(newConnection.getName());
            dbConnection.setSourceId(source.getId());
            dbConnection.setActive(newConnection.isActive());
            dbConnection.setDbUrl(newConnection.getDbUrl());
            dbConnection.setDbUsername(newConnection.getDbUsername());

            if (StringUtils.isNotEmpty(newConnection.getDbPassword())) {
                dbConnection.setDbPassword(newConnection.getDbPassword());
            }
        }

        dbConnectionService.save(Collections.singletonList(dbConnection));
        return dbConnection;
    }

    @DeleteMapping("/{sourceId}/connection/{connectionId}")
    @PreAuthorize("hasRole('ADMIN')")
    public void deleteConnection(@PathVariable UUID connectionId) {
        dbConnectionService.delete(Collections.singletonList(dbConnectionService.retrieve(connectionId)));
    }

    @GetMapping("/{sourceId}/connection/schema")
    @PreAuthorize("hasRole('ADMIN') or principal.hasAccess(#sourceId, 'DEVELOPER')")
    public List<DBTable> getSchema(@PathVariable UUID sourceId,
                                   @RequestParam(required = false, defaultValue = "false") boolean force) {
        Source source = sourceService.retrieve(sourceId);
        return dbMetadataService.getSchema(source, force);
    }

    @GetMapping("/{sourceId}/connection/refresh")
    @PreAuthorize("hasRole('ADMIN')")
    public void refreshConnections(@PathVariable UUID sourceId) {

        Source source = sourceService.retrieve(sourceId);

        if (source.isMultiTenant() && source.getMultiTenantSource() != null) {
            dbConnectionService.refreshMultiTenantConnections(source);
        }
    }

    @GetMapping("/{sourceId}/query")
    @PreAuthorize("hasRole('ADMIN') or principal.hasAccess(#sourceId, 'REPORTER')")
    public List<Query> listQueries(@PathVariable UUID sourceId) {
        List<Query> queries = queryService.listForSource(sourceId);
        queries.forEach(query -> query.setQuery(null));
        return queries;
    }

}
