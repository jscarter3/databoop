package com.mmbleh.db.controller;

import com.mmbleh.db.model.DataboopUser;
import com.mmbleh.db.service.DataboopUserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/account")
public class AccountController {

    private DataboopUserService databoopUserService;
    private PasswordEncoder passwordEncoder;

    public AccountController(DataboopUserService databoopUserService,
                             PasswordEncoder passwordEncoder) {
        this.databoopUserService = databoopUserService;
        this.passwordEncoder = passwordEncoder;
    }

    @GetMapping
    @PreAuthorize("hasRole('ADMIN')")
    public DataboopUser retrieve(Authentication authentication) {
        DataboopUser user = (DataboopUser) authentication.getPrincipal();
        return databoopUserService.retrieve(user.getId());
    }

    @PostMapping
    public DataboopUser saveAccount(@RequestBody DataboopUser newUser, Authentication authentication) {

        DataboopUser user = databoopUserService.retrieve(((DataboopUser) authentication.getPrincipal()).getId());
        user.setName(newUser.getName());
        user.setEmail(newUser.getEmail());
        if (StringUtils.isNotEmpty(newUser.getPassword())) {
            user.setPassword(passwordEncoder.encode(newUser.getPassword()));
        }
        databoopUserService.save(user);
        return user;
    }
}
