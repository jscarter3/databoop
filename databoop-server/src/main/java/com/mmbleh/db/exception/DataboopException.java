package com.mmbleh.db.exception;

public class DataboopException extends RuntimeException {

    public DataboopException(String msg) {
        super(msg);
    }

    public DataboopException(Throwable cause) {
        super(cause);
    }

    public DataboopException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
