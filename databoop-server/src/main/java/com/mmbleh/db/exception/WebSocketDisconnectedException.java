package com.mmbleh.db.exception;

public class WebSocketDisconnectedException extends RuntimeException {

    public WebSocketDisconnectedException(String msg) {
        super(msg);
    }

    public WebSocketDisconnectedException(Throwable cause) {
        super(cause);
    }

    public WebSocketDisconnectedException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
