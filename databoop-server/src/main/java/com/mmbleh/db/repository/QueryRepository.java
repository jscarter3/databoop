package com.mmbleh.db.repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mmbleh.db.model.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Repository
public class QueryRepository {

    private NamedParameterJdbcTemplate jdbcTemplate;
    private ObjectMapper objectMapper;
    private QueryMapper queryMapper = new QueryMapper();
    private Logger logger = LoggerFactory.getLogger(SourceSchemaRepository.class);

    public QueryRepository(DataSource dataSource, ObjectMapper objectMapper) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        this.objectMapper = objectMapper;
    }

    public List<Query> retrieveBySourceId(UUID sourceId) {
        String sql = "SELECT * FROM query WHERE source_id = :id";
        return jdbcTemplate.query(sql, Collections.singletonMap("id", sourceId), queryMapper);
    }

    public Query retrieve(UUID id) {
        String sql = "SELECT * FROM query WHERE id=:id";
        return jdbcTemplate.queryForObject(sql, Collections.singletonMap("id", id), queryMapper);
    }

    public void createOrUpdate(Query query) {
        if (query.getId() == null) {
            create(query);
        } else {
            update(query);
        }
    }

    private void create(Query query) {
        String sql = "INSERT INTO query(id, name, source_id, connection_ids, query, modified) " +
                "VALUES (:id,:name,:sourceId,:connectionIds,:query,:modified)";
        query.setId(UUID.randomUUID());
        query.setModified(new Date());

        MapSqlParameterSource parameterSource = new MapSqlParameterSource()
                .addValue("id", query.getId())
                .addValue("name", query.getName())
                .addValue("sourceId", query.getSourceId())
                .addValue("query", query.getQuery())
                .addValue("modified", query.getModified());

        try {
            parameterSource.addValue("connectionIds", objectMapper.writeValueAsString(query.getConnectionIds()));
        } catch (JsonProcessingException e) {
            logger.error("Unable to serialize connection ids", e);
        }
        jdbcTemplate.update(sql, parameterSource);
    }

    private void update(Query query) {
        String sql = "UPDATE query SET name=:name, source_id=:sourceId, connection_ids=:connectionIds, query=:query, " +
                "modified=:modified WHERE id=:id";

        MapSqlParameterSource parameterSource = new MapSqlParameterSource()
                .addValue("id", query.getId())
                .addValue("name", query.getName())
                .addValue("sourceId", query.getSourceId())
                .addValue("query", query.getQuery())
                .addValue("modified", query.getModified());

        try {
            parameterSource.addValue("connectionIds", objectMapper.writeValueAsString(query.getConnectionIds()));
        } catch (JsonProcessingException e) {
            logger.error("Unable to serialize connection ids", e);
        }

        jdbcTemplate.update(sql, parameterSource);
    }

    public void delete(UUID id) {
        String sql = "DELETE FROM query WHERE id=:id";
        jdbcTemplate.update(sql, Collections.singletonMap("id", id));
    }

    private class QueryMapper implements RowMapper<Query> {

        @Override
        public Query mapRow(ResultSet rs, int i) throws SQLException {
            Query query = new Query();

            query.setId(UUID.fromString(rs.getString("id")));
            query.setName(rs.getString("name"));
            query.setSourceId(UUID.fromString(rs.getString("source_id")));

            try {
                query.setConnectionIds(objectMapper.readValue(rs.getString("connection_ids"), new TypeReference<List<UUID>>(){}));
            } catch (IOException e) {
                logger.error(e.getLocalizedMessage(), e);
            }
            query.setQuery(rs.getString("query"));
            query.setModified(new Date(rs.getTimestamp("modified").getTime()));

            return query;
        }
    }
}
