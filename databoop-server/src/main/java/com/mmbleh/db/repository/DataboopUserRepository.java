package com.mmbleh.db.repository;

import com.mmbleh.db.model.DataboopUser;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Repository
public class DataboopUserRepository {

    private NamedParameterJdbcTemplate jdbcTemplate;
    private DataboopUserMapper databoopUserMapper = new DataboopUserMapper();

    public DataboopUserRepository(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public List<DataboopUser> list() {
        String sql = "SELECT * FROM databoop_user";
        return jdbcTemplate.query(sql, databoopUserMapper);
    }

    public DataboopUser retrieve(UUID id) {
        String sql = "SELECT * FROM databoop_user WHERE id=:id";
        return jdbcTemplate.queryForObject(sql, Collections.singletonMap("id", id), databoopUserMapper);
    }

    public DataboopUser retrieveByUsername(String username) {
        String sql = "SELECT * FROM databoop_user WHERE username=:username";
        return jdbcTemplate.queryForObject(sql, Collections.singletonMap("username", username), databoopUserMapper);
    }

    public void createOrUpdate(DataboopUser user) {
        if (user.getId() == null) {
            create(user);
        } else {
            update(user);
        }
    }

    private void create(DataboopUser user) {
        String sql = "INSERT INTO databoop_user(id, username, password, name, email, admin, active, created) " +
                "VALUES (:id,:username,:password,:name,:email,:admin,:active,:created)";
        user.setId(UUID.randomUUID());
        user.setCreated(new Date());
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(user);
        jdbcTemplate.update(sql, parameterSource);
    }

    private void update(DataboopUser user) {
        String sql = "UPDATE databoop_user SET username=:username, password=:password, name=:name, email=:email, " +
                "admin=:admin, active=:active, created=:created WHERE id=:id";
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(user);
        jdbcTemplate.update(sql, parameterSource);
    }

    public void delete(UUID id) {
        String sql = "DELETE FROM databoop_user WHERE id=:id";
        jdbcTemplate.update(sql, Collections.singletonMap("id", id));
    }

    private class DataboopUserMapper implements RowMapper<DataboopUser> {

        @Override
        public DataboopUser mapRow(ResultSet rs, int i) throws SQLException {
            DataboopUser user = new DataboopUser();

            user.setId(UUID.fromString(rs.getString("id")));
            user.setUsername(rs.getString("username"));
            user.setPassword(rs.getString("password"));
            user.setName(rs.getString("name"));
            user.setEmail(rs.getString("email"));
            user.setAdmin(rs.getBoolean("admin"));
            user.setActive(rs.getBoolean("active"));
            user.setCreated(rs.getTimestamp("created"));
            return user;
        }
    }
}
