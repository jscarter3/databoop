package com.mmbleh.db.repository;

import com.mmbleh.db.model.DBConnection;
import com.mmbleh.db.service.CryptoService;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Repository
public class DBConnectionRepository {

    private NamedParameterJdbcTemplate jdbcTemplate;
    private DBConnectionMapper dbConnectionMapper = new DBConnectionMapper();
    private CryptoService cryptoService;

    public DBConnectionRepository(DataSource dataSource,
                                  CryptoService cryptoService) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        this.cryptoService = cryptoService;
    }

    public List<DBConnection> listForSource(UUID sourceId) {
        String sql = "SELECT * FROM db_connection WHERE source_id=:sourceId";
        return jdbcTemplate.query(sql, Collections.singletonMap("sourceId", sourceId), dbConnectionMapper);
    }

    public DBConnection retrieve(UUID id) {
        String sql = "SELECT * FROM db_connection WHERE id=:id";
        return jdbcTemplate.queryForObject(sql, Collections.singletonMap("id", id), dbConnectionMapper);
    }

    public void createOrUpdate(List<DBConnection> dbConnections) {
        List<DBConnection> toCreate = dbConnections.stream().filter(dbConnection -> dbConnection.getId() == null).collect(Collectors.toList());
        List<DBConnection> toUpdate = dbConnections.stream().filter(dbConnection -> dbConnection.getId() != null).collect(Collectors.toList());
        if (toCreate.size() > 0) {
            create(toCreate);
        }
        if (toUpdate.size() > 0) {
            update(toUpdate);
        }
    }

    private void create(List<DBConnection> dbConnections) {
        String sql = "INSERT INTO db_connection(id, source_id, name, db_url, db_username, db_password, active) " +
                "VALUES (:id, :sourceId, :name, :dbUrl, :dbUsername, :dbPassword, :active)";
        dbConnections.forEach(dbConnection -> dbConnection.setId(UUID.randomUUID()));
        SqlParameterSource[] parameterSources = dbConnections.stream()
                .map(dbConnection -> new MapSqlParameterSource()
                        .addValue("id", dbConnection.getId())
                        .addValue("sourceId", dbConnection.getSourceId())
                        .addValue("name", dbConnection.getName())
                        .addValue("dbUrl", dbConnection.getDbUrl())
                        .addValue("dbUsername", dbConnection.getDbUsername())
                        .addValue("dbPassword", cryptoService.encrypt(dbConnection.getDbPassword()))
                        .addValue("active", dbConnection.isActive()))
                .toArray(MapSqlParameterSource[]::new);

        jdbcTemplate.batchUpdate(sql, parameterSources);
    }

    private void update(List<DBConnection> dbConnections) {
        String sql = "UPDATE db_connection SET source_id=:sourceId, name=:name, db_url=:dbUrl, " +
                "db_username=:dbUsername, db_password=:dbPassword, active=:active WHERE id=:id";
        SqlParameterSource[] parameterSources = dbConnections.stream()
                .map(dbConnection -> new MapSqlParameterSource()
                        .addValue("id", dbConnection.getId())
                        .addValue("sourceId", dbConnection.getSourceId())
                        .addValue("name", dbConnection.getName())
                        .addValue("dbUrl", dbConnection.getDbUrl())
                        .addValue("dbUsername", dbConnection.getDbUsername())
                        .addValue("dbPassword", cryptoService.encrypt(dbConnection.getDbPassword()))
                        .addValue("active", dbConnection.isActive()))
                .toArray(MapSqlParameterSource[]::new);

        jdbcTemplate.batchUpdate(sql, parameterSources);
    }

    public void delete(List<DBConnection> dbConnections) {
        String sql = "DELETE FROM db_connection WHERE id=:id";
        SqlParameterSource[] parameterSources = dbConnections.stream()
                .map(conn -> new MapSqlParameterSource("id", conn.getId()))
                .toArray(MapSqlParameterSource[]::new);
        jdbcTemplate.batchUpdate(sql, parameterSources);
    }

    private class DBConnectionMapper implements RowMapper<DBConnection> {

        @Override
        public DBConnection mapRow(ResultSet rs, int i) throws SQLException {
            DBConnection dbConnection = new DBConnection();

            dbConnection.setId(UUID.fromString(rs.getString("id")));
            dbConnection.setSourceId(UUID.fromString(rs.getString("source_id")));
            dbConnection.setName(rs.getString("name"));
            dbConnection.setDbUrl(rs.getString("db_url"));
            dbConnection.setDbUsername(rs.getString("db_username"));
            dbConnection.setDbPassword(cryptoService.decrypt(rs.getString("db_password")));
            dbConnection.setActive(rs.getBoolean("active"));
            return dbConnection;
        }
    }
}
