package com.mmbleh.db.repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mmbleh.db.model.DBTable;
import com.mmbleh.db.model.SourceSchema;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Repository
public class SourceSchemaRepository {

    private NamedParameterJdbcTemplate jdbcTemplate;
    private ObjectMapper objectMapper;
    private SourceSchemaMapper sourceSchemaMapper = new SourceSchemaMapper();
    private Logger logger = LoggerFactory.getLogger(SourceSchemaRepository.class);

    public SourceSchemaRepository(DataSource dataSource, ObjectMapper objectMapper) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        this.objectMapper = objectMapper;
    }

    public SourceSchema retrieve(UUID id) {
        String sql = "SELECT * FROM source_schema WHERE id=:id";
        return jdbcTemplate.queryForObject(sql, Collections.singletonMap("id", id), sourceSchemaMapper);
    }

    public SourceSchema retrieveForSource(UUID sourceId) {
        String sql = "SELECT * FROM source_schema WHERE source_id=:sourceId";
        return jdbcTemplate.queryForObject(sql, Collections.singletonMap("sourceId", sourceId), sourceSchemaMapper);
    }

    public void createOrUpdate(SourceSchema schema) {
        if (schema.getId() == null) {
            create(schema);
        } else {
            update(schema);
        }
    }

    private void create(SourceSchema schema) {
        String sql = "INSERT INTO source_schema(id, source_id, schema, last_updated) VALUES (:id,:sourceId,:schema,:lastUpdated)";
        schema.setId(UUID.randomUUID());
        SqlParameterSource parameterSource = null;
        try {
            parameterSource = new MapSqlParameterSource()
                    .addValue("id", schema.getId())
                    .addValue("sourceId", schema.getSourceId())
                    .addValue("schema", objectMapper.writeValueAsString(schema.getSchema()))
                    .addValue("lastUpdated", schema.getLastUpdated());
        } catch (JsonProcessingException e) {
            logger.error("failed to serialize schema", e);
        }
        jdbcTemplate.update(sql, parameterSource);
    }

    private void update(SourceSchema schema) {
        String sql = "UPDATE source_schema SET source_id=:sourceId, schema=:schema, last_updated=:lastUpdated WHERE id=:id";
        SqlParameterSource parameterSource = null;
        try {
            parameterSource = new MapSqlParameterSource()
                    .addValue("id", schema.getId())
                    .addValue("sourceId", schema.getSourceId())
                    .addValue("schema", objectMapper.writeValueAsString(schema.getSchema()))
                    .addValue("lastUpdated", schema.getLastUpdated());
        } catch (JsonProcessingException e) {
            logger.error("failed to serialize schema", e);
        }
        jdbcTemplate.update(sql, parameterSource);
    }

    public void delete(UUID id) {
        String sql = "DELETE FROM source_schema WHERE id=:id";
        jdbcTemplate.update(sql, Collections.singletonMap("id", id));
    }

    private class SourceSchemaMapper implements RowMapper<SourceSchema> {

        @Override
        public SourceSchema mapRow(ResultSet rs, int i) throws SQLException {
            SourceSchema schema = new SourceSchema();

            schema.setId(UUID.fromString(rs.getString("id")));
            schema.setSourceId(UUID.fromString(rs.getString("source_id")));
            try {
                schema.setSchema(objectMapper.readValue(rs.getString("schema"), new TypeReference<List<DBTable>>(){}));
            } catch (IOException e) {
                logger.error(e.getLocalizedMessage(), e);
            }
            schema.setLastUpdated(rs.getTimestamp("last_updated"));

            return schema;
        }
    }

}
