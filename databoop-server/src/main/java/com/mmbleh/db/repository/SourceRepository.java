package com.mmbleh.db.repository;

import com.mmbleh.db.model.MultiTenantSource;
import com.mmbleh.db.model.Source;
import com.mmbleh.db.service.CryptoService;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

@Repository
public class SourceRepository {

    private Logger logger = LoggerFactory.getLogger(SourceRepository.class);
    private NamedParameterJdbcTemplate jdbcTemplate;
    private SourceMapper sourceMapper = new SourceMapper();
    private MultiTenantSourceMapper multiTenantSourceMapper = new MultiTenantSourceMapper();
    private CryptoService cryptoService;

    public SourceRepository(DataSource dataSource,
                            CryptoService cryptoService) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        this.cryptoService = cryptoService;
    }

    public List<Source> list() {
        String sql = "SELECT * FROM source";
        return jdbcTemplate.query(sql, sourceMapper);
    }

    public Source retrieve(UUID id) {
        SqlParameterSource parameterSource = new MapSqlParameterSource("id", id);
        Source source = jdbcTemplate.queryForObject("SELECT * FROM source WHERE id=:id", parameterSource, sourceMapper);
        if (source != null && source.isMultiTenant()) {
            try {
                MultiTenantSource multiTenantSource = jdbcTemplate.queryForObject("SELECT * FROM multi_tenant_source WHERE source_id=:id", parameterSource, multiTenantSourceMapper);
                source.setMultiTenantSource(multiTenantSource);
            } catch (EmptyResultDataAccessException e) {
                // no multi tenant source... don't try to load it.
            }
        }
        return source;
    }

    public void createOrUpdate(Source source) {
        if (source.getId() == null) {
            create(source);
        } else {
            update(source);
        }
        MultiTenantSource mtSource = source.getMultiTenantSource();
        if (mtSource != null) {
            mtSource.setSourceId(source.getId());
            String sql;
            if (mtSource.getId() == null) {
                sql = "INSERT INTO multi_tenant_source(id, source_id, db_url, db_username, db_password, credential_source, query, tenant_username, tenant_password) " +
                        "VALUES (:id,:sourceId,:dbUrl,:dbUsername,:dbPassword,:credentialSource,:query,:tenantUsername,:tenantPassword)";
                mtSource.setId(UUID.randomUUID());
            } else {
                sql = "UPDATE multi_tenant_source SET source_id=:sourceId, db_url=:dbUrl, db_username=:dbUsername, " +
                        "db_password=:dbPassword, credential_source=:credentialSource, query=:query, " +
                        "tenant_username=:tenantUsername, tenant_password=:tenantPassword WHERE id=:id";
            }

            SqlParameterSource parameterSource = new MapSqlParameterSource()
                    .addValue("id", mtSource.getId())
                    .addValue("sourceId", mtSource.getSourceId())
                    .addValue("dbUrl", mtSource.getDbUrl())
                    .addValue("dbUsername", mtSource.getDbUsername())
                    .addValue("dbPassword", cryptoService.encrypt(mtSource.getDbPassword()))
                    .addValue("credentialSource", mtSource.getCredentialSource().name())
                    .addValue("query", mtSource.getQuery())
                    .addValue("tenantUsername", mtSource.getTenantUsername())
                    .addValue("tenantPassword", cryptoService.encrypt(mtSource.getTenantPassword()));
            jdbcTemplate.update(sql, parameterSource);
        }
    }

    private void create(Source source) {
        String sql = "INSERT INTO source(id, name, read_only, pool_size, query_timeout, multi_tenant, concurrent_queries) " +
                "VALUES (:id,:name,:readOnly,:poolSize,:queryTimeout,:multiTenant,:concurrentQueries)";
        source.setId(UUID.randomUUID());
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(source);
        jdbcTemplate.update(sql, parameterSource);
    }

    private void update(Source source) {
        String sql = "UPDATE source SET name=:name, read_only=:readOnly, pool_size=:poolSize, query_timeout=:queryTimeout, " +
                "multi_tenant=:multiTenant, concurrent_queries=:concurrentQueries WHERE id=:id";
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(source);
        jdbcTemplate.update(sql, parameterSource);
    }

    public void delete(UUID id) {

        SqlParameterSource parameterSource = new MapSqlParameterSource("id", id);

        jdbcTemplate.update("DELETE FROM multi_tenant_source WHERE source_id=:id", parameterSource);
        jdbcTemplate.update("DELETE FROM source WHERE id=:id", parameterSource);
    }

    private class SourceMapper implements RowMapper<Source> {

        @Override
        public Source mapRow(ResultSet rs, int i) throws SQLException {
            Source source = new Source();

            source.setId(UUID.fromString(rs.getString("id")));
            source.setName(rs.getString("name"));
            source.setReadOnly(rs.getBoolean("read_only"));
            source.setPoolSize(rs.getInt("pool_size"));
            source.setQueryTimeout(rs.getInt("query_timeout"));
            source.setMultiTenant(rs.getBoolean("multi_tenant"));
            source.setConcurrentQueries(rs.getInt("concurrent_queries"));
            return source;
        }
    }

    private class MultiTenantSourceMapper implements RowMapper<MultiTenantSource> {

        @Override
        public MultiTenantSource mapRow(ResultSet rs, int i) throws SQLException {
            MultiTenantSource multiTenantSource = new MultiTenantSource();

            multiTenantSource.setId(UUID.fromString(rs.getString("id")));
            multiTenantSource.setDbUrl(rs.getString("db_url"));
            multiTenantSource.setDbUsername(rs.getString("db_username"));
            multiTenantSource.setDbPassword(cryptoService.decrypt(rs.getString("db_password")));
            String credSource = rs.getString("credential_source");
            if (StringUtils.isNotEmpty(credSource)) {
                multiTenantSource.setCredentialSource(MultiTenantSource.CredentialSource.valueOf(credSource));
            }
            try {
                multiTenantSource.setQuery(IOUtils.toString(rs.getCharacterStream("query")));
            } catch (IOException e) {
                logger.error(e.getLocalizedMessage(), e);
                multiTenantSource.setQuery("");
            }
            multiTenantSource.setTenantUsername(rs.getString("tenant_username"));
            multiTenantSource.setTenantPassword(cryptoService.decrypt(rs.getString("tenant_password")));
            return multiTenantSource;
        }
    }
}
