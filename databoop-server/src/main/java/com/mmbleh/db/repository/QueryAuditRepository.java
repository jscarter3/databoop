package com.mmbleh.db.repository;

import com.mmbleh.db.model.QueryAudit;
import com.mmbleh.db.model.QueryAuditSummary;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Repository
public class QueryAuditRepository {

    private NamedParameterJdbcTemplate jdbcTemplate;
    private QueryAuditMapper queryAuditMapper = new QueryAuditMapper();

    public QueryAuditRepository(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public List<QueryAudit> listForGroup(UUID queryGroup) {
        String sql = "SELECT * FROM query_audit WHERE query_group=:queryGroup";
        return jdbcTemplate.query(sql, Collections.singletonMap("queryGroup", queryGroup), queryAuditMapper);
    }

    public List<QueryAuditSummary> getSummary() {

        String sql = "SELECT query_group, source_name, query, count(query_group) as count, username, max(created) as created " +
                "FROM query_audit GROUP BY query_group, source_name, query, username ORDER BY created DESC LIMIT 500";

        return jdbcTemplate.query(sql, (rs, rowNum) -> {
            QueryAuditSummary summary = new QueryAuditSummary();
            summary.setQueryGroup(UUID.fromString(rs.getString("query_group")));
            summary.setSourceName(rs.getString("source_name"));
            summary.setQuery(rs.getString("query"));
            summary.setCount(rs.getLong("count"));
            summary.setUsername(rs.getString("username"));
            summary.setCreated(rs.getTimestamp("created"));
            return summary;
        });
    }

    public void save(QueryAudit queryAudit) {
        String sql = "INSERT INTO query_audit(id, query_group, query, query_duration, source_name, db_name, db_url, username, created) " +
                "VALUES (:id,:queryGroup,:query,:queryDuration,:sourceName,:dbName,:dbUrl,:username,:created)";
        if (queryAudit.getId() == null) {
            queryAudit.setId(UUID.randomUUID());
        }
        jdbcTemplate.update(sql, new BeanPropertySqlParameterSource(queryAudit));
    }

    private class QueryAuditMapper implements RowMapper<QueryAudit> {

        @Override
        public QueryAudit mapRow(ResultSet rs, int i) throws SQLException {
            QueryAudit audit = new QueryAudit();

            audit.setId(UUID.fromString(rs.getString("id")));
            audit.setQueryGroup(UUID.fromString(rs.getString("query_group")));
            audit.setQuery(rs.getString("query"));
            audit.setQueryDuration(rs.getLong("query_duration"));
            audit.setSourceName(rs.getString("source_name"));
            audit.setDbName(rs.getString("db_name"));
            audit.setDbUrl(rs.getString("db_url"));
            audit.setUsername(rs.getString("username"));
            audit.setCreated(rs.getTimestamp("created"));
            return audit;
        }
    }
}