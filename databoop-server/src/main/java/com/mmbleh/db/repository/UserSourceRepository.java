package com.mmbleh.db.repository;

import com.mmbleh.db.model.Role;
import com.mmbleh.db.model.UserSource;
import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Repository
public class UserSourceRepository {

    private NamedParameterJdbcTemplate jdbcTemplate;
    private UserSourceMapper userSourceMapper = new UserSourceMapper();

    public UserSourceRepository(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public List<UserSource> listForUser(UUID userId) {
        String sql = "SELECT * FROM user_source WHERE user_id=:userId";
        return jdbcTemplate.query(sql, Collections.singletonMap("userId", userId), userSourceMapper);
    }

    public List<UserSource> listForSource(UUID sourceId) {
        String sql = "SELECT * FROM user_source WHERE source_id=:sourceId";
        return jdbcTemplate.query(sql, Collections.singletonMap("sourceId", sourceId), userSourceMapper);
    }

    public void createOrUpdate(List<UserSource> userSources) {
        List<UserSource> toCreate = userSources.stream().filter(userSource -> userSource.getId() == null).collect(Collectors.toList());
        List<UserSource> toUpdate = userSources.stream().filter(userSource -> userSource.getId() != null).collect(Collectors.toList());
        if (toCreate.size() > 0) {
            create(toCreate);
        }
        if (toUpdate.size() > 0) {
            update(toUpdate);
        }
    }

    private void create(List<UserSource> userSources) {
        String sql = "INSERT INTO user_source(id, user_id, source_id, role) VALUES (:id, :userId, :sourceId, :role)";
        userSources.forEach(userSource -> userSource.setId(UUID.randomUUID()));
        SqlParameterSource[] parameterSources = userSources.stream()
                .map(userSource -> new MapSqlParameterSource()
                        .addValue("id", userSource.getId())
                        .addValue("userId", userSource.getUserId())
                        .addValue("sourceId", userSource.getSourceId())
                        .addValue("role", userSource.getRole().name()))
                .toArray(MapSqlParameterSource[]::new);
        jdbcTemplate.batchUpdate(sql, parameterSources);
    }

    private void update(List<UserSource> userSources) {
        String sql = "UPDATE user_source SET user_id=:userId, source_id=:sourceId, role=:role WHERE id=:id";
        SqlParameterSource[] parameterSources = userSources.stream()
                .map(userSource -> new MapSqlParameterSource()
                        .addValue("id", userSource.getId())
                        .addValue("userId", userSource.getUserId())
                        .addValue("sourceId", userSource.getSourceId())
                        .addValue("role", userSource.getRole().name()))
                .toArray(MapSqlParameterSource[]::new);
        jdbcTemplate.batchUpdate(sql, parameterSources);
    }

    public void delete(List<UserSource> userSources) {
        String sql = "DELETE FROM user_source WHERE id=:id";
        SqlParameterSource[] parameterSources = userSources.stream()
                .map(source -> new MapSqlParameterSource("id", source.getId()))
                .toArray(MapSqlParameterSource[]::new);
        jdbcTemplate.batchUpdate(sql, parameterSources);
    }

    private class UserSourceMapper implements RowMapper<UserSource> {

        @Override
        public UserSource mapRow(ResultSet rs, int i) throws SQLException {
            UserSource userSource = new UserSource();

            userSource.setId(UUID.fromString(rs.getString("id")));
            userSource.setUserId(UUID.fromString(rs.getString("user_id")));
            userSource.setSourceId(UUID.fromString(rs.getString("source_id")));
            String strRole = rs.getString("role");
            if (StringUtils.isNotEmpty(strRole)) {
                try {
                    userSource.setRole(Role.valueOf(strRole));
                } catch (IllegalArgumentException e) {
                    // leave role null
                }
            }
            return userSource;
        }
    }
}
