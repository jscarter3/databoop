CREATE TABLE spring_session (
  primary_id            CHAR(36) PRIMARY KEY,
  session_id            CHAR(36),
  creation_time         BIGINT NOT NULL,
  last_access_time      BIGINT NOT NULL,
  max_inactive_interval INT    NOT NULL,
	expiry_time           BIGINT NOT NULL,
  principal_name        VARCHAR(100)
);
CREATE INDEX spring_session_ix1
  ON spring_session (last_access_time);

CREATE TABLE spring_session_attributes (
  session_primary_id      CHAR(36) REFERENCES spring_session (primary_id) ON DELETE CASCADE,
  attribute_name  VARCHAR(200),
  attribute_bytes BYTEA,
  CONSTRAINT spring_session_attributes_pk PRIMARY KEY (session_primary_id, attribute_name)
);
CREATE INDEX spring_session_attributes_ix1
  ON spring_session_attributes (session_primary_id);

CREATE TABLE source (
  id                 UUID PRIMARY KEY,
  name               VARCHAR(255),
  read_only          BOOLEAN,
  pool_size          INTEGER,
  query_timeout      INTEGER,
  multi_tenant       BOOLEAN,
  concurrent_queries INTEGER
);

CREATE TABLE multi_tenant_source (
  id                UUID PRIMARY KEY,
  source_id         UUID REFERENCES source ON DELETE CASCADE,
  db_url            VARCHAR(255),
  db_username       VARCHAR(255),
  db_password       VARCHAR(255),
  credential_source VARCHAR(16),
  query             TEXT,
  tenant_username   VARCHAR(255),
  tenant_password   VARCHAR(255)
);

CREATE TABLE db_connection (
  id          UUID PRIMARY KEY,
  source_id   UUID REFERENCES source ON DELETE CASCADE,
  name        VARCHAR(255),
  db_url      VARCHAR(255),
  db_username VARCHAR(255),
  db_password VARCHAR(255),
  active      BOOLEAN DEFAULT TRUE
);

CREATE TABLE source_schema (
  id           UUID PRIMARY KEY,
  source_id    UUID REFERENCES source ON DELETE CASCADE,
  schema       TEXT,
  last_updated TIMESTAMP DEFAULT NOW()
);

CREATE TABLE databoop_user (
  id       UUID PRIMARY KEY,
  username VARCHAR(32),
  password VARCHAR(255),
  name     VARCHAR(255),
  email    VARCHAR(255),
  admin    BOOLEAN   DEFAULT FALSE,
  active   BOOLEAN   DEFAULT TRUE,
  created  TIMESTAMP DEFAULT NOW()
);

CREATE TABLE user_source (
  id        UUID PRIMARY KEY,
  user_id   UUID REFERENCES databoop_user ON DELETE CASCADE,
  source_id UUID REFERENCES source ON DELETE CASCADE,
  role      VARCHAR(32)
);

CREATE TABLE query (
  id             UUID PRIMARY KEY,
  name           VARCHAR(64),
  source_id      UUID REFERENCES source,
  connection_ids VARCHAR(50000),
  query          VARCHAR(10000),
  modified       TIMESTAMP DEFAULT NOW()
);

CREATE TABLE query_audit (
  id             UUID PRIMARY KEY,
  query_group    UUID,
  query          VARCHAR(10000),
  query_duration BIGINT,
  source_name    VARCHAR(255),
  db_name        VARCHAR(255),
  db_url         VARCHAR(255),
  username       VARCHAR(32),
  created        TIMESTAMP DEFAULT NOW()
)