import m from "mithril";
import stream from "mithril/stream";
import Login from "./pages_auth/login";
import Sources from "./pages_main/sources";
import SourceDetail from "./pages_main/sourceDetail";
import MyAccount from "./pages_main/myAccount";
import About from "./pages_main/about";
import Playground from "./pages_boop/playground";
import Queries from "./pages_boop/queries";
import QueryDetail from "./pages_boop/queryDetail";
import Usage from "./pages_admin/usage";
import Users from "./pages_admin/users";
import UserDetail from "./pages_admin/userDetail";
import Actuator from "./pages_admin/actuator";
import tellie from "./lib/tellie";

m.unwrapProp = function (object, field) {
    let unwrapped = stream(object()[field]);
    unwrapped.map(v => {
        let data = object();
        data[field] = v;
        object(data);
    });
    return unwrapped;
};

window.addEventListener("resize", (e) => tellie.pub("window:resize", e));

m.route.prefix("#");
m.route(document.body, "/", {
    "/"                     : Sources,
    "/login"                : Login,
    "/sources"              : Sources,
    "/source/:id"           : SourceDetail,
    "/myaccount"            : MyAccount,
    "/about"                : About,
    "/boop/:sourceId/playground"            : Playground,
    "/boop/:sourceId/playground/:queryId"   : Playground,
    "/boop/:sourceId/queries"               : Queries,
    "/boop/:sourceId/query/:queryId"        : QueryDetail,
    "/admin"                : Usage,
    "/admin/usage"          : Usage,
    "/admin/users"          : Users,
    "/admin/user/:id"       : UserDetail,
    "/admin/actuator"       : Actuator,
});