import m from "mithril";
import stream from "mithril/stream";

let JSONViewer = {
    oninit: (vnode) => {
        vnode.state.collapsed = stream(vnode.attrs.collapsed || false);
    },
    view: (vnode) => {
        let collapsed = vnode.state.collapsed,
            data = vnode.attrs.data;
        if (!data) {
            return "";
        }
        switch (R.type(data)) {
            case "Object":
                return [
                    "{ ",
                    collapsed() ? "" : toggle(collapsed),
                    collapsed() ? "..." : R.keys(data).sort().map(d => m("div.json-nest", [
                        d + ": ",
                        m(JSONViewer, {data: data[d]}),
                    ])),
                    "} ",
                    collapsed() ? toggle(collapsed) : "",
                ];
            case "Array":
                return [
                    "[ ",
                    collapsed() ? "" : toggle(collapsed),
                    collapsed() ? "..." : data.map(d => m("div.json-nest", m(JSONViewer, {data: d}))),
                    "] ",
                    collapsed() ? toggle(collapsed) : "",
                ];
            default:
                return JSON.stringify(data);
        }
    },
};

function toggle(collapsed) {
    return m("span.json-toggle", {
        onclick: () => collapsed(!collapsed()),
    }, collapsed() ? "[+]" : "[-]");
}

export default JSONViewer;