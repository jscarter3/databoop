import m from "mithril";
import tellie from "../lib/tellie";
import {Util} from "./mdl";

export default {
    oncreate: (vnode) => {
        let vs = vnode.state;

        vs.el = vnode.dom;
        Util.upgradeElement(vnode);

        tellie.sub("snackbar", (data) => {
            if (vs.el) {
                vs.el.MaterialSnackbar.showSnackbar(data);
            }
        });
    },
    onremove: tellie.resetc(/snackbar.*/),
    view: () => m("div.mdl-snackbar.mdl-js-snackbar", [
        m("div.mdl-snackbar__text"),
        m("button.mdl-snackbar__action", {type: "button"}),
    ]),
};