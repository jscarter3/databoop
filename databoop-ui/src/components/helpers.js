import m from "mithril";

export let Row = {
    oninit: (vnode) => {
        let vs = vnode.state,
            va = vnode.attrs;
        vs.classes = ["row"];
        if (va.right) vs.classes.push("right-align");
    },
    view: (vnode) => m("div", {
        class: vnode.state.classes.join(" "),
    }, vnode.children),
};