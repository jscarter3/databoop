import m from "mithril";
import tellie from "../lib/tellie";

export default {
    oninit: (vnode) => {
        let vs = vnode.state;
        vs.isLoading = false;
        vs.loadingMessage = undefined;

        tellie.sub("loading:on", (message) => {
            vs.loadingMessage = message;
            vs.isLoading = true;
        });
        tellie.sub("loading:off", () => {
            vs.isLoading = false;
            vs.loadingMessage = undefined;
            m.redraw();
        });
    },
    onremove: tellie.resetc(/loading.*/),
    view: (vnode) => {
        let vs = vnode.state;
        return !vs.isLoading ? "" : m("div.loading-bg", [
            m("div.loading-whirlpool-container", [
                m("div.loading-whirlpool"),
            ]),
            m("div.loading-message", vs.loadingMessage),
        ]);
    },
};