import m from "mithril";
import {Util} from "./util";

export let List = {
    oninit: (vnode) => {
        let vs = vnode.state,
            attrBuilder = Util.attributeBuilder(vnode);

        attrBuilder.addClass("mdl-list");
        vs.attrs = attrBuilder.build();
    },
    // oncreate: Util.upgradeElement,
    view: (vnode) => m("ul", vnode.state.attrs, vnode.children),
};

export let ListItem = {
    oninit: (vnode) => {
        let vs = vnode.state,
            va = vnode.attrs,
            attrBuilder = Util.attributeBuilder(vnode);

        attrBuilder.addClass("mdl-list__item");

        if (va.twoLine) attrBuilder.addClass("mdl-list__item-two-line");
        if (va.threeLine) attrBuilder.addClass("mdl-list__item-three-line");

        vs.attrs = attrBuilder.build();
    },
    // oncreate: Util.upgradeElement,
    view: (vnode) => m("li", vnode.state.attrs, vnode.children),
};

export let ListItemPrimaryContent = {
    oninit: (vnode) => {
        let vs = vnode.state,
            attrBuilder = Util.attributeBuilder(vnode);

        attrBuilder.addClass("mdl-list__item-primary-content");

        vs.attrs = attrBuilder.build();
    },
    view: (vnode) => m("span", vnode.state.attrs, vnode.children),
};

export let ListItemSecondaryContent = {
    oninit: (vnode) => {
        let vs = vnode.state,
            attrBuilder = Util.attributeBuilder(vnode);

        attrBuilder.addClass("mdl-list__item-secondary-content");

        vs.attrs = attrBuilder.build();
    },
    view: (vnode) => m("span", vnode.state.attrs, vnode.children),
};

export let ListItemIcon = {
    oninit: (vnode) => {
        let vs = vnode.state,
            attrBuilder = Util.attributeBuilder(vnode);

        attrBuilder.addClass("material-icons", "mdl-list__item-icon");

        vs.attrs = attrBuilder.build();
    },
    view: (vnode) => m("i", vnode.state.attrs, vnode.children),
};

export let ListItemAvatar = {
    oninit: (vnode) => {
        let vs = vnode.state,
            attrBuilder = Util.attributeBuilder(vnode);

        attrBuilder.addClass("material-icons", "mdl-list__item-avatar");

        vs.attrs = attrBuilder.build();
    },
    view: (vnode) => m("i", vnode.state.attrs, vnode.children),
};

export let ListItemSecondaryAction = {
    oninit: (vnode) => {
        let vs = vnode.state,
            attrBuilder = Util.attributeBuilder(vnode);

        attrBuilder.addClass("mdl-list__item-secondary-action");

        vs.attrs = attrBuilder.build();
    },
    view: (vnode) => m("span", vnode.state.attrs, vnode.children),
};

export let ListItemSecondaryInfo = {
    oninit: (vnode) => {
        let vs = vnode.state,
            attrBuilder = Util.attributeBuilder(vnode);

        attrBuilder.addClass("mdl-list__item-secondary-info");

        vs.attrs = attrBuilder.build();
    },
    view: (vnode) => m("span", vnode.state.attrs, vnode.children),
};

export let ListItemTextBody = {
    oninit: (vnode) => {
        let vs = vnode.state,
            attrBuilder = Util.attributeBuilder(vnode);

        attrBuilder.addClass("mdl-list__item-text-body");

        vs.attrs = attrBuilder.build();
    },
    view: (vnode) => m("span", vnode.state.attrs, vnode.children),
};