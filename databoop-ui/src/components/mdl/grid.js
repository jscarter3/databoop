import m from "mithril";
import {Util} from "./util";

export let Grid = {
    oninit: (vnode) => {
        let va = vnode.attrs,
            attrBuilder = Util.attributeBuilder(vnode);

        attrBuilder.addClass("mdl-grid");
        if (va.noSpacing) attrBuilder.addClass("mdl-grid--no-spacing");

        vnode.state.attrs = attrBuilder.build();
    },
    view: (vnode) => m("div", vnode.state.attrs, vnode.children),
};

export let Cell = {
    oninit: (vnode) => {
        let va = vnode.attrs,
            attrBuilder = Util.attributeBuilder(vnode);

        attrBuilder.addClass("mdl-cell");
        if (va.col) attrBuilder.addClass(`mdl-cell--${va.col}-col`);
        if (va.colDesktop) attrBuilder.addClass(`mdl-cell--${va.colDesktop}-col-desktop`);
        if (va.colTablet) attrBuilder.addClass(`mdl-cell--${va.colTablet}-col-tablet`);
        if (va.colPhone) attrBuilder.addClass(`mdl-cell--${va.colPhone}-col-phone`);
        if (va.offset) attrBuilder.addClass(`mdl-cell--${va.offset}-offset`);
        if (va.offsetDesktop) attrBuilder.addClass(`mdl-cell--${va.offsetDesktop}-offset-desktop`);
        if (va.offsetTablet) attrBuilder.addClass(`mdl-cell--${va.offsetTablet}-offset-tablet`);
        if (va.offsetPhone) attrBuilder.addClass(`mdl-cell--${va.offsetPhone}-offset-phone`);
        if (va.order) attrBuilder.addClass(`mdl-cell--order-${va.offset}`);
        if (va.orderDesktop) attrBuilder.addClass(`mdl-cell--order-${va.orderDesktop}-desktop`);
        if (va.orderTablet) attrBuilder.addClass(`mdl-cell--order-${va.orderTablet}-tablet`);
        if (va.orderPhone) attrBuilder.addClass(`mdl-cell--order-${va.orderPhone}-phone`);
        if (va.hideDesktop) attrBuilder.addClass("mdl-cell--hide-desktop");
        if (va.hideTablet) attrBuilder.addClass("mdl-cell--hide-tablet");
        if (va.hidePhone) attrBuilder.addClass("mdl-cell--hide-phone");
        if (va.stretch) attrBuilder.addClass("mdl-cell--stretch");
        if (va.top) attrBuilder.addClass("mdl-cell--top");
        if (va.middle) attrBuilder.addClass("mdl-cell--middle");
        if (va.bottom) attrBuilder.addClass("mdl-cell--bottom");

        vnode.state.attrs = attrBuilder.build();
    },
    view: (vnode) => m("div", vnode.state.attrs, vnode.children),
};