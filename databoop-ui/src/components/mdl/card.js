import m from "mithril";
import {Util} from "./util";

export let Card = {
    oninit: (vnode) => {
        let vs = vnode.state,
            va = vnode.attrs,
            attrBuilder = Util.attributeBuilder(vnode);

        attrBuilder.addClass("mdl-card");

        if (va.shadow) {
            let value = Math.round(va.shadow),
                allowedValues = [2, 3, 4, 6, 8, 16];

            if (!allowedValues.includes(value)) {
                value = 2;
                console.warn(`Shadow value of ${value} is not allowed. Value must be one of: ${allowedValues.join(", ")}`);
            }
            attrBuilder.addClass(`mdl-shadow--${va.shadow}dp`);
        }
        vs.attrs = attrBuilder.build();
    },
    view: (vnode) => m("div", vnode.state.attrs, vnode.children),
};

export let CardTitle = {
    oninit: (vnode) => {
        let vs = vnode.state,
            va = vnode.attrs,
            attrBuilder = Util.attributeBuilder(vnode);

        attrBuilder.addClass("mdl-card__title");
        if (va.border) attrBuilder.addClass("mdl-card--border");

        vs.attrs = attrBuilder.build();
    },
    view: (vnode) => m("div", vnode.state.attrs, vnode.children),
};

export let CardTitleText = {
    oninit: (vnode) => {
        let vs = vnode.state,
            va = vnode.attrs,
            allowedValues = [1, 2, 3, 4, 5, 6];

        va.h = va.h || 1;
        if (!allowedValues.includes(va.h)) {
            va.h = 1;
            console.warn(`h value of ${va.h} is not allowed. Value must be one of: ${allowedValues.join(", ")}`);
        }
        vs.tag = `h${va.h}`;

    },
    view: (vnode) => {
        return m(`${vnode.state.tag}.mdl-card__title-text`, vnode.children);
    },
};

// export let CardSubtitleText = {
//     view: (vnode) => {
//         return m("div.mdl-card__subtitle-text", vnode.children);
//     },
// };

export let CardMedia = {
    oninit: (vnode) => {
        let vs = vnode.state,
            va = vnode.attrs,
            attrBuilder = Util.attributeBuilder(vnode);

        attrBuilder.addClass("mdl-card__media");
        if (va.border) attrBuilder.addClass("mdl-card--border");

        vs.attrs = attrBuilder.build();
    },
    view: (vnode) => m("div", vnode.state.attrs, vnode.children),
};

export let CardSupportingText = {
    oninit: (vnode) => {
        let vs = vnode.state,
            va = vnode.attrs,
            attrBuilder = Util.attributeBuilder(vnode);

        attrBuilder.addClass("mdl-card__supporting-text");
        if (va.border) attrBuilder.addClass("mdl-card--border");

        vs.attrs = attrBuilder.build();
    },
    view: (vnode) => m("div", vnode.state.attrs, vnode.children),
};

export let CardActions = {
    oninit: (vnode) => {
        let vs = vnode.state,
            va = vnode.attrs,
            attrBuilder = Util.attributeBuilder(vnode);

        attrBuilder.addClass("mdl-card__actions");
        if (va.border) attrBuilder.addClass("mdl-card--border");

        vs.attrs = attrBuilder.build();
    },
    view: (vnode) => m("div", vnode.state.attrs, vnode.children),
};