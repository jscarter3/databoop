import m from "mithril";
import {Util} from "./util";

export let Spinner = {
    oninit: (vnode) => {
        let attrBuilder = Util.attributeBuilder(vnode);

        attrBuilder.addClass("mdl-spinner", "mdl-js-spinner", "is-active");

        if (vnode.attrs.single) attrBuilder.addClass("mdl-spinner--single-color");

        vnode.state.attrs = attrBuilder.build();
    },
    oncreate: Util.upgradeElement,
    view: (vnode) => m("div", vnode.state.attrs),
};