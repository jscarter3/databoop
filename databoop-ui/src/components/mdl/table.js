import m from "mithril";
import {Util} from "./util";

export let Table = {
    oninit: (vnode) => {
        let va = vnode.attrs,
            attrBuilder = Util.attributeBuilder(vnode);

        attrBuilder.addClass("mdl-data-table", "mdl-js-data-table");
        if (va.selectable) attrBuilder.addClass("mdl-data-table--selectable");
        if (va.shadow) attrBuilder.addClass(`mdl-shadow--${va.shadow}dp`);
        if (va.compact) attrBuilder.addClass("table-compact");

        vnode.state.attrs = attrBuilder.build();
    },
    oncreate: Util.upgradeElement,
    view: (vnode) => m("table", vnode.state.attrs, vnode.children),
};

export let TableHeader = {
    oncreate: Util.upgradeElement,
    oninit: (vnode) => {
        let va = vnode.attrs,
            attrBuilder = Util.attributeBuilder(vnode);

        if (va.ascending) attrBuilder.addClass("mdl-data-table__header--sorted-ascending");
        if (va.descending) attrBuilder.addClass("mdl-data-table__header--sorted-descending");
        if (va.nonNumeric) attrBuilder.addClass("mdl-data-table__cell--non-numeric");

        vnode.state.attrs = attrBuilder.build();
    },
    onbeforeupdate: (vnode) => {
        let va = vnode.attrs,
            attrBuilder = Util.attributeBuilder(vnode);

        if (va.ascending) attrBuilder.addClass("mdl-data-table__header--sorted-ascending");
        if (va.descending) attrBuilder.addClass("mdl-data-table__header--sorted-descending");
        if (va.nonNumeric) attrBuilder.addClass("mdl-data-table__cell--non-numeric");

        vnode.state.attrs = attrBuilder.build();
    },
    view: (vnode) => m("th", vnode.state.attrs, vnode.children),
};

export let TableCell = {
    oninit: (vnode) => {
        let va = vnode.attrs,
            attrBuilder = Util.attributeBuilder(vnode);

        if (va.nonNumeric) attrBuilder.addClass("mdl-data-table__cell--non-numeric");

        vnode.state.attrs = attrBuilder.build();
    },
    oncreate: Util.upgradeElement,
    view: (vnode) => m("td", vnode.state.attrs, vnode.children),
};