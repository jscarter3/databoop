export let Util = {
    upgradeElement: (vnode) => componentHandler.upgradeElement(vnode.dom),
    attributeBuilder: (vnode) => {
        let va = vnode.attrs,
            classes = va.classes || [],
            attrs = {};

        // copy properties
        let propsToCopy = ["id", "for", "style", "disabled"];
        for(let prop in va) {
            if (va.hasOwnProperty(prop) && (prop.startsWith("on") || propsToCopy.includes(prop))) {
                attrs[prop] = va[prop];
            }
        }

        return {
            addClass: function() {
                classes.push(...arguments);
            },
            addAttribute: function(name, value) {
                attrs[name] = value;
            },
            removeAttribute: function(name) {
                delete attrs[name];
            },
            build: () => {
                attrs.class = classNames(classes);
                return attrs;
            },
        };
    },
};

/*!
 Copyright (c) 2016 Jed Watson.
 Licensed under the MIT License (MIT), see
 http://jedwatson.github.io/classnames
 */

function classNames () {
    let classes = [];

    for (let i = 0; i < arguments.length; i++) {
        let arg = arguments[i];
        if (!arg) continue;

        let argType = typeof arg;

        if (argType === "string" || argType === "number") {
            classes.push(arg);
        } else if (Array.isArray(arg)) {
            classes.push(classNames.apply(null, arg));
        } else if (argType === "object") {
            for (let key in arg) {
                if (arg.hasOwnProperty(key)) {
                    if ((typeof arg[key] === "function" && arg[key]()) || arg[key]) {
                        classes.push(key);
                    }
                }
            }
        }
    }

    return classes.join(" ");
}