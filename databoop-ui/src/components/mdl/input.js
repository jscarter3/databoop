import m from "mithril";
import {Util} from "./util";

export let Input = {
    oninit: (vnode) => {
        let va = vnode.attrs,
            attrBuilder = Util.attributeBuilder(vnode);

        attrBuilder.addClass("mdl-textfield", "mdl-js-textfield");

        if (va.floating) attrBuilder.addClass("mdl-textfield--floating-label");

        vnode.state.attrs = attrBuilder.build();
    },
    oncreate: Util.upgradeElement,
    view: (vnode) => {
        let va = vnode.attrs,
            valIsProp = va.value instanceof Function;
        return m("div", vnode.state.attrs, [
            m("input.mdl-textfield__input", {
                id: va.id,
                type: va.type || "text",
                oninput: valIsProp ? m.withAttr("value", va.value) : null,
                placeholder: va.placeholder,
                pattern: va.pattern,
                value: valIsProp ? va.value() : va.value,
                disabled: va.disabled,
                readonly: va.readonly,
            }),
            m("label.mdl-textfield__label", {"for": va.id}, va.label),
            va.pattern ? m("span.mdl-textfield__error", va.error) : "",
        ]);
    },
};

export let TextArea = {
    oninit: (vnode) => {
        let va = vnode.attrs,
            vs = vnode.state,
            attrBuilder = Util.attributeBuilder(vnode);

        attrBuilder.addClass("mdl-textfield", "mdl-js-textfield");

        if (va.floating) attrBuilder.addClass("mdl-textfield--floating-label");

        vs.attrs = attrBuilder.build();
    },
    oncreate: Util.upgradeElement,
    view: (vnode) => {
        let va = vnode.attrs,
            valIsProp = va.value instanceof Function;
        return m("div", vnode.state.attrs, [
            m("textarea.mdl-textfield__input", {
                id: va.id,
                rows: va.rows,
                oninput: (e) => {
                    if (!valIsProp) return;
                    va.value(e.currentTarget.value);
                    if (va.selected) {
                        va.selected("");
                    }
                },
                onselect: (e) => {
                    if (!valIsProp) return;
                    let ct = e.currentTarget,
                        start = ct.selectionStart,
                        end = ct.selectionEnd;
                    if (va.selected) {
                        va.selected(va.value().substring(start, end));
                    }
                },
                placeholder: va.placeholder,
                value: valIsProp ? va.value() : va.value,
                disabled: va.disabled,
                readonly: va.readonly,
            }),
            m("label.mdl-textfield__label", {"for": va.id}, va.label),
        ]);
    },
};