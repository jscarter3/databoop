import m from "mithril";

export let Icon = {
    oninit: () => {},
    //oncreate: (vnode) => componentHandler.upgradeElement(vnode.dom),
    view: (vnode) => m("i.material-icons", {class: vnode.attrs.class, style: vnode.attrs.style}, vnode.children),
};