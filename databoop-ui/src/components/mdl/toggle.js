import m from "mithril";
import {Util} from "./util";
import classnames from "classnames";

export let Checkbox = {
    oninit: (vnode) => {
        let va = vnode.attrs;
        // attrBuilder = Util.attributeBuilder(vnode);

        let classes = va.classes || [];
        classes.push("mdl-checkbox", "mdl-js-checkbox");

        if (vnode.attrs.ripple) classes.push("mdl-js-ripple-effect");
        // if (va.id) attrBuilder.addAttribute("for", va.id);

        // attrBuilder.addClass({"is-checked": va.value});
        // vnode.state.attrBuilder = attrBuilder;
        // vnode.state.attrs = attrBuilder.build();
        vnode.state.classes = classes;
    },
    oncreate: Util.upgradeElement,
    view: (vnode) => {
        let va = vnode.attrs,
            vs = vnode.state;
        return m("label", {
            "for": va.id,
            class: classnames(vs.classes),
        }, [
            m("input.mdl-checkbox__input", {
                id: va.id,
                type: "checkbox",
                onchange: m.withAttr("checked", va.value),
                checked: va.value(),
                disabled: va.disabled,
            }),
            va.label ? m("span.mdl-checkbox__label", va.label) : "",
        ]);
    },
};

export let Switch = {
    oninit: (vnode) => {
        let va = vnode.attrs,
            attrBuilder = Util.attributeBuilder(vnode);

        attrBuilder.addClass("mdl-switch", "mdl-js-switch");

        if (vnode.attrs.ripple) attrBuilder.addClass("mdl-js-ripple-effect");
        if (va.id) attrBuilder.addAttribute("for", va.id);

        vnode.state.attrs = attrBuilder.build();
    },
    oncreate: Util.upgradeElement,
    view: (vnode) => {
        let va = vnode.attrs;
        return m("label", vnode.state.attrs, [
            m("input.mdl-switch__input", {
                id: va.id,
                type: "checkbox",
                onchange: m.withAttr("checked", va.value),
                checked: va.value(),
            }),
            va.label ? m("span.mdl-switch__label", va.label) : "",
        ]);
    },
};

export let RadioButton = {
    oninit: (vnode) => {
        let va = vnode.attrs,
            attrBuilder = Util.attributeBuilder(vnode);

        attrBuilder.addClass("mdl-radio", "mdl-js-radio");

        if (va.ripple) attrBuilder.addClass("mdl-js-ripple-effect");
        if (va.id) attrBuilder.addAttribute("for", va.id);

        vnode.state.attrs = attrBuilder.build();
        vnode.state.attrs.id = undefined;
    },
    oncreate: Util.upgradeElement,
    view: (vnode) => {
        let va = vnode.attrs;
        return m("label", vnode.state.attrs, [
            m("input.mdl-radio__button", {
                id: va.id,
                type: "radio",
                name: va.name,
                value: va.value,
                onchange: e => e.currentTarget.checked ? va.prop(va.value) : null,
                checked: va.prop() === va.value,
            }),
            va.label ? m("span.mdl-radio__label", va.label) : "",
        ]);
    },
};