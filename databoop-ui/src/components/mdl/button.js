import m from "mithril";
import {Util} from "./util";

export let Button = {
    oninit: (vnode) => {
        let vs = vnode.state,
            va = vnode.attrs,
            attrBuilder = Util.attributeBuilder(vnode);

        attrBuilder.addClass("mdl-button", "mdl-js-button");
        if (va.ripple) attrBuilder.addClass("mdl-js-ripple-effect");
        if (va.raised) attrBuilder.addClass("mdl-button--raised");
        if (va.fab) attrBuilder.addClass("mdl-button--fab");
        if (va.miniFab) attrBuilder.addClass("mdl-button--mini-fab");
        if (va.colored) attrBuilder.addClass("mdl-button--colored");
        if (va.primary) attrBuilder.addClass("mdl-button--primary");
        if (va.accent) attrBuilder.addClass("mdl-button--accent");
        if (va.icon) attrBuilder.addClass("mdl-button--icon");
        vs.attrs = attrBuilder.build();
    },
    oncreate: Util.upgradeElement,
    view: (vnode) => m("button", vnode.state.attrs, vnode.children),
};