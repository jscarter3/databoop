import m from "mithril";
import {Util} from "./util";

export let Menu = {
    oninit: (vnode) => {
        let vs = vnode.state,
            va = vnode.attrs,
            attrBuilder = Util.attributeBuilder(vnode);

        attrBuilder.addClass("mdl-menu", "mdl-js-menu");

        if (va.ripple) attrBuilder.addClass("mdl-js-ripple-effect");
        if (va.topLeft) attrBuilder.addClass("mdl-menu--top-left");
        if (va.topRight) attrBuilder.addClass("mdl-menu--top-right");
        if (va.bottomRight) attrBuilder.addClass("mdl-menu--bottom-right");

        vs.attrs = attrBuilder.build();
    },
    oncreate: Util.upgradeElement,
    view: (vnode) => m("ul", vnode.state.attrs, vnode.children),
};

export let MenuItem = {
    oninit: (vnode) => {
        let vs = vnode.state,
            va = vnode.attrs,
            attrBuilder = Util.attributeBuilder(vnode);

        attrBuilder.addClass("mdl-menu__item");

        if (va.divider) attrBuilder.addClass("mdl-menu__item--full-bleed-divider");

        vs.attrs = attrBuilder.build();
    },
    view: (vnode) => m("li", vnode.state.attrs, vnode.children),
};