import m from "mithril";

export let Select = {
    oninit: () => {
    },
    view: ({attrs}) => m("div.mdl-select", [
        m("label", {"for": attrs.id}, attrs.label),
        m("select", {
            id: attrs.id,
            onchange: m.withAttr("value", attrs.value),
            value: attrs.value(),
        }, attrs.options.map(opt => [
            m("option", {
                value: opt.value || opt,
            }, opt.name || opt),
        ])),
    ]),
};