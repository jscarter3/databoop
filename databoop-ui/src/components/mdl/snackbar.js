import m from "mithril";
import {Util} from "./util";

export let Snackbar = {
    oncreate: (vnode) => {
        let vs = vnode.state;
        Util.upgradeElement(vnode);
        vs.el = vnode.dom;
        vnode.attrs.data.map(data => {
            if (vs.el) {
                if (!vs.el.MaterialSnackbar) {
                    console.log("undefined :(");
                    return;
                }
                vs.el.MaterialSnackbar.showSnackbar(data);
            }
        });
    },
    view: () => m("div.mdl-snackbar.mdl-js-snackbar", [
        m("div.mdl-snackbar__text"),
        m("button.mdl-snackbar__action", {type: "button"}),
    ]),
};