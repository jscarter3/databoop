import m from "mithril";
import {Util} from "./util";

export let Dialog = {
    oninit: (vnode) => {
        let attrBuilder = Util.attributeBuilder(vnode);

        vnode.state.attrs = attrBuilder.build();
    },
    oncreate: (vnode) => {
        let vs = vnode.state;
        vs.el = vnode.dom;
        vnode.attrs.open.map(v => {
            if (!vs.el) return;
            else if (v && !vs.el.open) {
                vs.el.showModal();
            } else if (!v && vs.el.open) {
                vs.el.close();
            }
        });
    },
    onremove: (vnode) => {
        vnode.state.el = undefined;
    },
    view: (vnode) => m("dialog.mdl-dialog", vnode.state.attrs, vnode.children),
};

export let DialogTitle = {
    oninit: (vnode) => {
        let attrBuilder = Util.attributeBuilder(vnode);

        vnode.state.attrs = attrBuilder.build();
    },
    view: (vnode) => m("div.mdl-dialog__title", vnode.state.attrs, vnode.children),
};

export let DialogContent = {
    oninit: (vnode) => {
        let attrBuilder = Util.attributeBuilder(vnode);

        vnode.state.attrs = attrBuilder.build();
    },
    view: (vnode) => m("div.mdl-dialog__content", vnode.state.attrs, vnode.children),
};

export let DialogActions = {
    oninit: (vnode) => {
        let attrBuilder = Util.attributeBuilder(vnode);

        attrBuilder.addClass("mdl-dialog__actions");
        if (vnode.attrs.fullWidth) attrBuilder.addClass("mdl-dialog__actions--full-width");

        vnode.state.attrs = attrBuilder.build();
    },
    view: (vnode) => m("div", vnode.state.attrs, vnode.children),
};