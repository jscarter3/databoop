import m from "mithril";
import stream from "mithril/stream";
import tellie from "../lib/tellie";
import {
    Button,
    Dialog,
    DialogTitle,
    DialogContent,
    DialogActions,
} from "../components/mdl";

export default {
    oninit: (vnode) => {
        let vs = vnode.state;
        vs.open = stream(false);
        vs.body = undefined;

        function close() {
            vs.open(false);
            vs.body = undefined;
        }

        tellie.sub("dialog:open", (content) => {
            vs.body = content;
            vs.open(true);
        });
        tellie.sub("dialog:alert", (content) => {
            if (content instanceof Error && content.message) content = content.message;
            vs.body = [
                m(DialogContent, {
                    style: {
                        maxHeight: "400px",
                        overflowY: "auto",
                    },
                },content),
                m(DialogActions, [
                    m(Button, {onclick: close}, "Okay"),
                ]),
            ];
            vs.open(true);
        });
        tellie.sub("dialog:confirm", (message, callback) => {
            vs.body = [
                m(DialogTitle, "Confirm"),
                m(DialogContent, [
                    m("p", message),
                ]),
                m(DialogActions, [
                    m(Button, {
                        onclick: () => {
                            callback();
                            close();
                        },
                    }, "Yes"),
                    m(Button, {onclick: close}, "No"),
                ]),
            ];
            vs.open(true);
        });
        tellie.sub("dialog:close", close);
    },
    onremove: tellie.resetc(/dialog.*/),
    view: ({state}) => !state.body ? "" : m(Dialog, {open: state.open}, state.body),
};