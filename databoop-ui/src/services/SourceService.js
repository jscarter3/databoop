// import m from "mithril";
import http from "./http";

let url = "/api/source",
    insensitiveSort = (items) => items.sort((a, b) => a.name.toLowerCase().localeCompare(b.name.toLowerCase()));

export default {
    listSources: () => http.send("GET", url)
        .then(http.json)
        .then(insensitiveSort),
    retrieveSource: id => http.send("GET", `${url}/${id}`)
        .then(http.json),
    saveSource: source => http.send("POST", url, {data: source})
        .then(http.json),
    deleteSource: id => http.send("DELETE", `${url}/${id}`),
    retrieveUsersForSource: id => http.send("GET", `${url}/${id}/users`)
        .then(http.json),
    saveUsersForSource: (id, userSources) => http.send("POST", `${url}/${id}/users`, {data: userSources})
        .then(http.json),

    listConnections: (sourceId) => http.send("GET", `${url}/${sourceId}/connection`)
        .then(http.json)
        .then(insensitiveSort),
    getSchema: (sourceId, force) => http.send("GET", `${url}/${sourceId}/connection/schema`, force ? {data: {force}} : {})
        .then(http.json),
    saveConnection: (sourceId, connection) => http.send("POST", `${url}/${sourceId}/connection`, {data: connection})
        .then(http.json),
    deleteConnection: (sourceId, connId) => http.send("DELETE", `${url}/${sourceId}/connection/${connId}`),
    refreshMTConnections: (sourceId) => http.send("GET", `${url}/${sourceId}/connection/refresh`),

    listQueries: (sourceId) => http.send("GET", `${url}/${sourceId}/query`)
        .then(http.json)
        .then(insensitiveSort),
};