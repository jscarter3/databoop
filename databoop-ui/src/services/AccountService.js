import http from "./http";

let url = "/api/account";

export default {
    retrieveAccount: () => http.send("GET", url)
        .then(http.json),
    saveAccount: data => http.send("POST", url, {data}),
};