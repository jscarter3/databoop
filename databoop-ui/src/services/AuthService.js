// import m from "mithril";
import http from "./http";
import Store from "../services/Store";

let url = "/api";

export default {
    getUser: () => Store.get("auth"),
    isAdmin: () => Store.get("auth").admin,
    login: (req) => {
        return http.send("POST", `${url}/login`, {data: req})
            .then(http.json)
            .then(user => Store.set("auth", user));
    },
    logout: () => http.send("POST", `${url}/logout`),
};