/*global R, WebSocket, location*/
import http from "./http";

let url = "/api/query";

export default {
    retrieveQuery: id => http.send("GET", `${url}/${id}`)
        .then(http.json),
    saveQuery: data => http.send("POST", url, {data})
        .then(http.json),
    deleteQuery: id => http.send("DELETE", `${url}/${id}`),
    listAudit: () => http.send("GET", `${url}/audit`)
        .then(http.json),
    listAuditDetails: (groupId) => http.send("GET", `${url}/audit/${groupId}`)
        .then(http.json)
        .then(details => details.sort((a, b) => a.dbName.toLowerCase().localeCompare(b.dbName.toLowerCase())))
    ,
    runQuery: (query, callbacks) => {
        let ws = new WebSocket(`ws${location.origin.indexOf("https://") > -1 ? "s" : ""}://${location.host}/ws/execute`),
            newData = {},
            errors = {},
            header,
            isInconsistentData = false,
            callback = Object.assign({
                progress: R.identity,
                success: R.identity,
                status: R.identity,
                error: R.identity,
            }, callbacks);

        ws.onmessage = (data) => {
            let d = JSON.parse(data.data);
            switch (d.type) {
                case "status": {
                    d = d.data;
                    callback.status(d);
                    if (d.done === d.total) {
                        ws.close();
                        if (isInconsistentData) {
                            callback.success(processData(newData));
                        } else {
                            let data = {
                                header,
                                data: [],
                            };
                            R.forEachObjIndexed((value) => {
                                data.data.push(...value.data);
                            }, newData);
                            callback.success(data);
                        }
                        if (!R.isEmpty(errors)) {
                            callback.error(errors);
                        }
                    }
                    break;
                }
                case "data": {
                    d = d.data;
                    callback.progress(d);
                    let name = d.dbConnection.name;
                    if (d.header) {
                        if (!header) {
                            header = d.header;
                        } else if (!R.equals(header, d.header)) {
                            isInconsistentData = true;
                        }

                        if (!newData[name]) {
                            newData[name] = {
                                header: d.header,
                                data: [],
                            };
                        }
                    }
                    if (d.data) {
                        newData[name].data.push(...d.data);
                    }
                    if (d.error) {
                        errors[name] = d.error;
                    }
                    break;
                }
            }
        };
        ws.onopen = () => ws.send(JSON.stringify(query));
        return {
            close() {
                if (ws) {
                    ws.close();
                    ws = undefined;
                }
            },
        };
    },
};

function processData(newData) {
    let header = [],
        data = [];
    R.forEachObjIndexed((value) => {
        header = R.union(header, value.header);
    }, newData);

    R.forEachObjIndexed((value) => {
        let h = value.header,
            headerMapping = R.map(R.indexOf(R.__, h), header),
            newRow;

        value.data.forEach((row) => {
            let rearranger = R.ifElse(R.equals(-1), R.always("--DNE--"), R.nth(R.__, row));
            newRow = R.map(rearranger, headerMapping);
            data.push(newRow);
        });

    }, newData);

    return {header, data};
}