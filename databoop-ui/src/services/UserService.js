import http from "./http";

let url = "/api/user",
    insensitiveSort = (users) => users.sort((a, b) => a.username.toLowerCase().localeCompare(b.username.toLowerCase()));

export default {
    listUsers: () => http.send("GET", url)
        .then(http.json)
        .then(insensitiveSort),
    retrieveUser: id => http.send("GET", `${url}/${id}`)
        .then(http.json),
    saveUser: (user) => http.send("POST", url, {data: user})
        .then(http.json),
    deleteUser: id => http.send("DELETE", `${url}/${id}`),
    retrieveSourcesForUser: id => http.send("GET", `${url}/${id}/sources`)
        .then(http.json),
    saveSourcesForUser: (id, userSources) => http.send("POST", `${url}/${id}/sources`, {data: userSources})
        .then(http.json),
};