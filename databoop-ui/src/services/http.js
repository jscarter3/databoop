import m from "mithril";
//
// function extract(xhr, opts) {
//     return {
//         status: xhr.status,
//         body: xhr.responseText,
//     };
// }

export default {
    send: (method, url, opts) => {
        opts = opts || {};
        opts.method = method;
        opts.url = url;
        opts.extract = res => res;
        return new Promise((resolve, reject) => {
            return m.request(opts)
                .then(res => resolve(res))
                .catch(res => reject(res));
        });
        // return m.request(opts);
    },
    json: res => JSON.parse(res.responseText),
    defaultCatch: err => {
        if (err.status === 403) {
            m.route.set("/login");
        }
        // } else {
        //
        //     return Promise.reject(err);
        //     // return Promise.reject(err);
        //     // throw err;
        // }
        throw err;
    },

};