let npStore = {}, // not persisted
    store = {};

store.get = (key) => {
    if (localStorage.getItem(key)) return JSON.parse(localStorage.getItem(key));
    else if (npStore[key]) return npStore[key];
    else return undefined;
};

store.set = (key, data, persist) => {
    if (persist === false) {
        npStore[key] = data;
    } else {
        localStorage.setItem(key, JSON.stringify(data));
    }
};

store.clear = (key) => {
    if (key) {
        localStorage.removeItem(key);
        delete npStore[key];
    }
};

export default store;