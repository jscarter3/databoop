import m from "mithril";
import stream from "mithril/stream";
import Template from "./template";
import {
    Button,
    Card,
    CardTitle,
    CardTitleText,
    CardSupportingText,
    Checkbox,
    Table,
    TableHeader,
    TableCell,
    Input,
    Icon,
    Spinner,
    DialogTitle,
    DialogContent,
    DialogActions,
} from "../components/mdl";
import {Row} from "../components/helpers";
import SourceService from "../services/SourceService";
import tellie from "../lib/tellie";
import http from "../services/http";
import Auth from "../services/AuthService";

export default {
    oninit: (vnode) => {
        let vs = vnode.state;
        vs.sources = stream();
        SourceService.listSources()
            .then(vs.sources)
            .catch(http.defaultCatch)
            .then(m.redraw);

        vs.newSource = stream({});

        vs.addSource = () => SourceService.saveSource(vs.newSource())
            .then(source => {
                tellie.pub("dialog:close");
                return source;
            })
            .then(source => m.route.set(`/source/${source.id}`))
            .catch(http.defaultCatch)
            .then(m.redraw);
    },
    view: (vnode) => {
        let vs = vnode.state;
        return m(Template, [
            m("div.container", [
                m(Card, {shadow: 8, classes: ["width-100"]}, [
                    m(CardTitle, [
                        m(CardTitleText, {h: 5}, "Sources"),
                    ]),
                    m(CardSupportingText, [
                        !vs.sources() ? m(Spinner) : [
                            !Auth.isAdmin() ? "" : m(Row, {right: 1}, [
                                m(Button, {
                                    icon: 1,
                                    onclick: () => {
                                        vs.newSource({});
                                        tellie.pub("dialog:open", [
                                        // DialogService.open([
                                            m(DialogTitle, "Add Source"),
                                            m(DialogContent, {
                                                onkeyup: (e) => {
                                                    if (e.keyCode === 13) {
                                                        vs.addSource();
                                                    }
                                                },
                                            }, [
                                                m(Input, {
                                                    id: "name",
                                                    label: "Name",
                                                    floating: 1,
                                                    value: m.unwrapProp(vs.newSource, "name"),
                                                }),
                                                m(Checkbox, {
                                                    label: "Multi-Tenant",
                                                    ripple: 1,
                                                    value: m.unwrapProp(vs.newSource, "multiTenant"),
                                                }),
                                            ]),
                                            m(DialogActions, [
                                                m(Button, {
                                                    onclick: vs.addSource,
                                                }, "Create"),
                                                m(Button, {onclick: tellie.pubc("dialog:close")}, "Cancel"),//DialogService.close}, "Cancel"),
                                            ]),
                                        ]);
                                    },
                                }, m(Icon, "add")),
                            ]),
                            m(Table, {classes: ["width-100"]}, [
                                m("thead", m("tr", [
                                    !Auth.isAdmin() ? "" : m(TableHeader, {nonNumeric: 1}, "Edit"),
                                    m(TableHeader, {nonNumeric: 1}, "Name"),
                                    m(TableHeader, {nonNumeric: 1}, "Boop"),
                                ])),
                                m("tbody", [
                                    vs.sources().map(source => m("tr", [
                                        !Auth.isAdmin() ? "" : m(TableCell, {nonNumeric: 1}, m("a", {
                                            href: `/source/${source.id}`,
                                            oncreate: m.route.link,
                                        }, m(Icon, "edit"))),
                                        m(TableCell, {nonNumeric: 1}, source.name),
                                        m(TableCell, {nonNumeric: 1}, m("a", {
                                            href: `/boop/${source.id}/queries`,
                                            oncreate: m.route.link,
                                        }, m(Icon, "launch"))),
                                        // m(TableCell, {nonNumeric: 1}, m(Button, {
                                        //     icon: 1,
                                        //     onclick: () => m.route.set(`/boop/${source.id}/`),
                                        // }, m(Icon, "launch"))),
                                    ])),
                                ]),
                            ]),
                        ],
                    ]),
                ]),
            ]),
        ]);
    },
};