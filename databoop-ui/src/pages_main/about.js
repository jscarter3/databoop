import m from "mithril";
import Template from "./template";
import {
    Card,
    CardTitle,
    CardTitleText,
    CardSupportingText,
} from "../components/mdl";

export default {
    oninit: () => {},
    view: () => {
        return m(Template, m("div.container", [
            m(Card, {shadow: 8, classes: ["width-100"]}, [
                m(CardTitle, [
                    m(CardTitleText, {h: 5}, "About"),
                ]),
                m(CardSupportingText, [
                    m("p", "Here lies info about databoop."),
                ]),
            ]),
        ]));
    },
};