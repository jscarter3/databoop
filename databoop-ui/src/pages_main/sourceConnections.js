import m from "mithril";
import stream from "mithril/stream";
import {
    Button,
    Card,
    CardTitle,
    CardTitleText,
    CardSupportingText,
    Table,
    TableHeader,
    TableCell,
    Input,
    TextArea,
    Checkbox,
    Icon,
    Spinner,
    DialogTitle,
    DialogContent,
    DialogActions,
} from "../components/mdl";
import {Row} from "../components/helpers";
import tellie from "../lib/tellie";
import http from "../services/http";
import SourceService from "../services/SourceService";
import QueryService from "../services/QueryService";

export default {
    oninit: (vnode) => {
        let va = vnode.attrs,
            vs = vnode.state;

        vs.sourceId = va.source().id;
        vs.connections = stream();
        vs.statusData = stream({});
        vs.connectionDetail = stream({});

        function loadConnections() {
            // vs.connections([]);
            vs.statusData({});
            SourceService.listConnections(vs.sourceId)
                .then(vs.connections)
                .then(m.redraw)
                .catch(http.defaultCatch);
        }
        loadConnections();

        vs.saveConnection = () => SourceService.saveConnection(vs.sourceId, vs.connectionDetail)
            .then(loadConnections)
            .catch(http.defaultCatch)
            .then(tellie.pubc("dialog:close"))
            .then(m.redraw);

        tellie.sub("connections:load", loadConnections);

        vs.canAddConnection = false;
        vs.connections
            .map(connections => {
                vs.canAddConnection = va.source().multiTenant || connections.length < 1;
            });

        vs.testConnections = () => {
            tellie.pub("loading:on", [
                m("div", "Testing connections..."),
            ]);
            vs.statusData({});
            QueryService.runQuery({
                sourceId: vs.sourceId,
                testQuery: true,
            }, {
                progress: (data) => {
                    let statusData = vs.statusData();
                    statusData[data.dbConnection.id] = data.error ? data.error : "OK";
                    vs.statusData(statusData);
                    m.redraw();
                },
                status: (status) => {
                    tellie.pub("loading:on", [
                        m("div", "Testing connections..."),
                        m("div", `${status.done} of ${status.total} completed`),
                    ]);
                    if (status.done === status.total) {
                        tellie.pub("loading:off");
                    }
                    m.redraw();
                },
            });
        };

    },
    onremove: tellie.resetc("connections:load"),
    view: (vnode) => {
        let vs = vnode.state;
        return m(Card, {shadow: 6, classes: ["width-100", "margin-10"]}, [
            m(CardTitle, [
                m(CardTitleText, {h: 5}, "DB Connections"),
            ]),
            m(CardSupportingText, {}, !vs.connections() ? m(Spinner) : [
                m(Row, {right: 1}, [
                    m(Button, {
                        ripple: 1,
                        onclick: vs.testConnections,
                    }, "Test Connections"),
                    !vs.canAddConnection ? "" : m(Button, {
                        icon: 1,
                        onclick: () => {
                            vs.connectionDetail({});
                            tellie.pub("dialog:open", getConnectionDialog(vnode, false));
                        },
                    }, m(Icon, "add")),
                ]),
                m("div", m(Table, {classes: ["width-100"], style: {tableLayout: "fixed"}}, [
                    m("thead", [
                        m("tr", [
                            m(TableHeader, {nonNumeric: 1}, "Edit"),
                            m(TableHeader, {nonNumeric: 1}, "Name"),
                            m(TableHeader, {nonNumeric: 1}, "Active"),
                            m(TableHeader, {nonNumeric: 1}, "Status"),
                            m(TableHeader, {nonNumeric: 1}, "Delete"),
                        ]),
                    ]),
                    m("tbody", [
                        vs.connections().map(connection => {
                            let status = vs.statusData()[connection.id],
                                color = "#ddd";
                            if (status && status === "OK") color = "#00bb00";
                            else if (status) color = "#bb0000";

                            return m("tr", [
                                m(TableCell, {nonNumeric: 1}, m(Button, {
                                    icon: 1, accent: 1, onclick: () => {
                                        vs.connectionDetail(connection);
                                        tellie.pub("dialog:open", getConnectionDialog(vnode, true));
                                    },
                                }, m(Icon, "edit"))),
                                m(TableCell, {
                                    nonNumeric: 1,
                                    style: {overflow: "hidden", textOverflow: "ellipsis"},
                                }, connection.name),
                                m(TableCell, {nonNumeric: 1}, m(Button, {
                                    icon: 1,
                                    accent: 1,
                                    onclick: () => {
                                        connection.active = !connection.active;
                                        vs.connectionDetail(connection);
                                        vs.saveConnection();
                                    },
                                }, m(Icon, connection.active ? "check_box" : "check_box_outline_blank"))),
                                m(TableCell, {nonNumeric: 1}, m(Icon, {style: {color}}, "lens")),
                                m(TableCell, {nonNumeric: 1}, m(Button, {
                                    icon: 1,
                                    onclick: () => {
                                        tellie.pub("dialog:confirm", "Are you sure you want to delete this connection?", () => SourceService.deleteConnection(vs.sourceId, connection.id)
                                            .catch(http.defaultCatch)
                                            .then(tellie.pubc("connections:load"))
                                            .then(m.redraw));
                                    },
                                }, m(Icon, "clear"))),
                            ]);
                        }),
                    ]),
                ])),
            ]),
        ]);
    },
};

function getConnectionDialog(vnode, existing) {
    let vs = vnode.state;
    return [
        m(DialogTitle, existing ? "Edit" : "New" + " Connection"),
        m(DialogContent, {
            onkeyup: (e) => {
                if (e.keyCode === 13) {
                    vs.saveConnection();
                }
            },
        }, [
            m(Input, {
                id: "name",
                label: "Name",
                floating: 1,
                value: m.unwrapProp(vs.connectionDetail, "name"),
            }),
            m(TextArea, {
                id: "dbUrl",
                label: "DB Url",
                floating: 1,
                rows: 5,
                value: m.unwrapProp(vs.connectionDetail, "dbUrl"),
            }),
            m(Input, {
                id: "dbUsername",
                label: "DB Username",
                floating: 1,
                value: m.unwrapProp(vs.connectionDetail, "dbUsername"),
            }),
            m(Input, {
                id: "dbPassword",
                type: "password",
                label: "DB Password",
                floating: 1,
                value: m.unwrapProp(vs.connectionDetail, "dbPassword"),
            }),
            m(Checkbox, {
                label: "Active",
                value: m.unwrapProp(vs.connectionDetail, "active"),
            }),
        ]),
        m(DialogActions, [
            m(Button, {
                onclick: vs.saveConnection,
            }, existing ? "Save" : "Create"),
            m(Button, {onclick: tellie.pubc("dialog:close")}, "Cancel"),
        ]),
    ];
}