import m from "mithril";
import stream from "mithril/stream";
import Template from "./template";
import {
    Button,
    Card,
    CardTitle,
    CardTitleText,
    CardSupportingText,
    Input,
    Spinner,
    Grid,
    Cell,
} from "../components/mdl";
import {Row} from "../components/helpers";
import http from "../services/http";
import tellie from "../lib/tellie";
import AccountService from "../services/AccountService";

export default {
    oninit: ({state}) => {
        state.account = stream();
        AccountService.retrieveAccount()
            .then(state.account)
            .catch(http.defaultCatch);

        state.pwd1 = stream("");
        state.pwd2 = stream("");

        state.save = () => {
            let account = state.account(),
                send = true;
            if (state.pwd1()) {
                if (state.pwd1() === state.pwd2()) {
                    account.password = state.pwd1();
                } else {
                    send = false;
                    tellie.pub("dialog:alert", "Passwords do not match");
                }
            }
            if (send) {
                AccountService.saveAccount(account)
                    .catch(http.defaultCatch);
            }
        };
    },
    view: ({state}) => m(Template, m("div.container", [
        m(Card, {shadow: 8, classes: ["width-100"]}, [
            m(CardTitle, [
                m(CardTitleText, {h: 5}, "My Account"),
            ]),
            m(CardSupportingText, !state.account() ? m(Spinner) : [
                m(Row, {right: 1}, [
                    m(Button, {
                        ripple: 1,
                        onclick: () => window.history.go(-1),
                    }, "Back"),
                    m(Button, {
                        ripple: 1, onclick: state.save,
                    }, "Save"),
                ]),
                m(Grid, [
                    m(Cell, {col: 6}, [
                        m(Input, {
                            classes: ["block"],
                            id: "name",
                            label: "Name",
                            floating: 1,
                            value: m.unwrapProp(state.account, "name"),
                        }),
                        m(Input, {
                            classes: ["block"],
                            id: "email",
                            label: "Email",
                            floating: 1,
                            value: m.unwrapProp(state.account, "email"),
                        }),
                    ]),
                    m(Cell, {col: 6}, [
                        m(Input, {
                            classes: ["block"],
                            id: "new_password1",
                            type: "password",
                            label: "New Password",
                            floating: 1,
                            placeholder: "****",
                            value: state.pwd1,
                        }),
                        m(Input, {
                            classes: ["block"],
                            id: "new_password2",
                            type: "password",
                            label: "Repeat New Password",
                            floating: 1,
                            placeholder: "****",
                            value: state.pwd2,
                        }),
                    ]),
                ]),
            ]),
        ]),
    ])),
};