import m from "mithril";
import stream from "mithril/stream";
import Template from "./template";
import {
    Button,
    Card,
    CardTitle,
    CardTitleText,
    CardSupportingText,
    DialogTitle,
    DialogContent,
    DialogActions,
    Grid,
    Cell,
    Input,
    TextArea,
    Checkbox,
    RadioButton,
    Spinner,
    Select,
} from "../components/mdl";
import {Row} from "../components/helpers";
import tellie from "../lib/tellie";
import SourceConnections from "./sourceConnections";
import http from "../services/http";
import SourceService from "../services/SourceService";
import UserService from "../services/UserService";

export default {
    oninit: (vnode) => {
        let va = vnode.attrs,
            vs = vnode.state;

        vs.source = stream();
        vs.statusData = stream({});

        SourceService.retrieveSource(vnode.attrs.id)
            .then(vs.source)
            .then(() => {
                if (vs.source().multiTenant) {
                    vs.multiTenantSource = m.unwrapProp(vs.source, "multiTenantSource");
                    if (!vs.multiTenantSource()) {
                        vs.multiTenantSource({
                            credentialSource: "STATIC",
                        });
                    }
                }
            })
            .catch(http.defaultCatch)
            .then(m.redraw);

        vs.saveSource = () => SourceService.saveSource(vs.source())
            .catch(http.defaultCatch)
            .then(m.redraw);

        vs.users = stream();
        vs.userSources = stream();

        UserService.listUsers()
            .then(users => users.filter(user => !user.admin))
            .then(vs.users)
            .catch(http.defaultCatch)
            .then(m.redraw);

        SourceService.retrieveUsersForSource(va.id)
            .then(vs.userSources)
            .catch(http.defaultCatch)
            .then(m.redraw);

        vs.saveUserSources = () => {
            SourceService.saveUsersForSource(va.id, vs.userSources())
                .then(vs.userSources)
                .catch(http.defaultCatch)
                .then(tellie.pubc("dialog:close"));
        };
    },
    view: (vnode) => {
        let vs = vnode.state;
        return m(Template, [
            m("div.container", [
                // Detail section
                m(Card, {shadow: 6, classes: ["width-100", "margin-10"]}, [
                    m(CardTitle, [
                        m(CardTitleText, {h: 5}, "Source"),
                    ]),
                    m(CardSupportingText, {}, !vs.source() ? m(Spinner) : [
                        m(Row, {right: 1}, [
                            m(Button, {
                                ripple: 1,
                                onclick: () => window.history.go(-1),
                            }, "Back"),
                            m(Button, {
                                ripple: 1, onclick: () => {
                                    tellie.pub("dialog:confirm", "Are you sure you want to delete this source?", () => SourceService.deleteSource(vnode.attrs.id)
                                        .then(() => m.route.set("/"))
                                        .catch(http.defaultCatch));
                                },
                            }, "Delete"),
                            m(Button, {
                                ripple: 1, onclick: vs.saveSource,
                            }, "Save"),
                        ]),
                        m(Grid, [
                            m(Cell, {col: 6}, [
                                m(Input, {
                                    id: "name",
                                    label: "Name",
                                    floating: 1,
                                    value: m.unwrapProp(vs.source, "name"),
                                }),
                                m(Checkbox, {
                                    label: "Read Only",
                                    ripple: 1,
                                    value: m.unwrapProp(vs.source, "readOnly"),
                                }),
                                m(Checkbox, {
                                    label: "Multi-Tenant",
                                    ripple: 1,
                                    value: m.unwrapProp(vs.source, "multiTenant"),
                                    disabled: 1,
                                }),
                                m(Button, {
                                    onclick: () => {
                                        tellie.pub("dialog:open", sourceUserDialog(vnode));
                                    },
                                }, "Configure Users"),
                            ]),
                            m(Cell, {col: 6}, [
                                m(Input, {
                                    id: "pool-size",
                                    label: "Connection Pool Size",
                                    pattern: "^[1-9][0-9]*$",
                                    error: "Number must be greater than 0",
                                    floating: 1,
                                    value: m.unwrapProp(vs.source, "poolSize"),
                                }),
                                m(Input, {
                                    id: "query-timeout",
                                    label: "Query Timeout",
                                    pattern: "-?[0-9]*(\.[0-9]+)?", // eslint-disable-line
                                    error: "Please enter a valid number",
                                    floating: 1,
                                    value: m.unwrapProp(vs.source, "queryTimeout"),
                                }),
                                vs.source().multiTenant ? m(Input, {
                                    id: "concurrent-queries",
                                    label: "Concurrent Queries",
                                    pattern: "-?[0-9]*(\.[0-9]+)?", // eslint-disable-line
                                    error: "Please enter a valid number",
                                    floating: 1,
                                    value: m.unwrapProp(vs.source, "concurrentQueries"),
                                }) : "",
                            ]),
                        ]),
                    ]),
                ]),
                // Multi tenant source section
                !vs.multiTenantSource || !vs.multiTenantSource() ? "" : m(Card, {
                    shadow: 6,
                    classes: ["width-100", "margin-10"],
                }, [
                    m(CardTitle, [
                        m(CardTitleText, {h: 5}, "Multi Tenant Source Stuff"),
                    ]),
                    m(CardSupportingText, {}, [
                        m(Row, {right: 1}, [
                            m(Button, {
                                ripple: 1, onclick: () => {
                                    tellie.pub("dialog:confirm", "Are you sure you want to clear the multi tenant stuff?", () => {
                                        vs.multiTenantSource({credentialSource: "STATIC"});
                                    });
                                },
                            }, "Clear"),
                            m(Button, {
                                ripple: 1, onclick: () => {
                                    tellie.pub("loading:on", "Refreshing connections...");
                                    vs.saveSource()
                                        .then(() => SourceService.refreshMTConnections(vnode.attrs.id))
                                        .then(tellie.pubc("connections:load"))
                                        .catch(http.defaultCatch)
                                        .catch(tellie.pubc("dialog:alert"))
                                        .then(tellie.pubc("loading:off"));
                                },
                            }, "Refresh Connections"),
                        ]),
                        m(Grid, [
                            m(Cell, {col: 6}, [
                                m(TextArea, {
                                    classes: ["block"],
                                    id: "dbUrl",
                                    label: "DB Url",
                                    floating: 1,
                                    rows: 5,
                                    value: m.unwrapProp(vs.multiTenantSource, "dbUrl"),
                                }),
                                m(Input, {
                                    classes: ["block"],
                                    id: "dbUsername",
                                    label: "DB Username",
                                    floating: 1,
                                    value: m.unwrapProp(vs.multiTenantSource, "dbUsername"),
                                }),
                                m(Input, {
                                    classes: ["block"],
                                    type: "password",
                                    id: "dbPassword",
                                    label: "DB Password",
                                    floating: 1,
                                    value: m.unwrapProp(vs.multiTenantSource, "dbPassword"),
                                }),
                            ]),
                            m(Cell, {col: 6}, [
                                m(Row, [
                                    m(RadioButton, {
                                        id: "boop1",
                                        style: {marginRight: "10px"},
                                        prop: m.unwrapProp(vs.multiTenantSource, "credentialSource"),
                                        name: "credentialSource",
                                        value: "STATIC",
                                        label: "Static",
                                    }),
                                    m(RadioButton, {
                                        id: "boop2",
                                        prop: m.unwrapProp(vs.multiTenantSource, "credentialSource"),
                                        name: "credentialSource",
                                        value: "QUERY",
                                        label: "Query",
                                    }),
                                ]),
                                vs.multiTenantSource().credentialSource !== "STATIC" ? "" : [
                                    m(Input, {
                                        classes: ["block"],
                                        id: "tenantUsername",
                                        label: "Tenant Username",
                                        floating: 1,
                                        value: m.unwrapProp(vs.multiTenantSource, "tenantUsername"),
                                    }),
                                    m(Input, {
                                        classes: ["block"],
                                        type: "password",
                                        id: "tenantPassword",
                                        label: "Tenant Password",
                                        floating: 1,
                                        value: m.unwrapProp(vs.multiTenantSource, "tenantPassword"),
                                    }),
                                ],
                            ]),
                            m(Cell, {col: 12}, [
                                m(TextArea, {
                                    style: {width: "100%"},
                                    rows: 3,
                                    id: "query",
                                    label: "Query",
                                    floating: 1,
                                    value: m.unwrapProp(vs.multiTenantSource, "query"),
                                }),
                            ]),
                        ]),
                    ]),
                ]),
                !vs.source() ? "" : m(SourceConnections, {source: vs.source}),
            ]),
        ]);
    },
};

function sourceUserDialog({state}) {
    return [
        m(DialogTitle, "Configure Access"),
        m(DialogContent, [
            state.users().length > 0 ? state.users().map(user => m(SourceUserItem, {
                user,
                userSources: state.userSources,
            })) : "No non-admin users configured",
        ]),
        m(DialogActions, [
            m(Button, {
                onclick: state.saveUserSources,
            }, "Save"),
            m(Button, {onclick: tellie.pubc("dialog:close")}, "Cancel"),
        ]),
    ];
}

let SourceUserItem = {
    oninit: ({state, attrs}) => {
        state.options = ["NONE", "DEVELOPER", "REPORTER"];
        state.value = stream();
        let us = attrs.userSources().find(userSource => userSource.userId === attrs.user.id);
        if (us) {
            state.value(us.role);
        } else {
            state.value("NONE");
        }
        state.value
            .map(value => {
                let userSources = attrs.userSources(),
                    index = userSources.findIndex(userSource => userSource.userId === attrs.user.id);
                if (index > -1 && value === "NONE") {
                    userSources.splice(index, 1);
                } else if (index < 0 && value !== "NONE") {
                    userSources.push({userId: attrs.user.id, role: value});
                } else if (index > -1) {
                    userSources[index].role = value;
                }
                attrs.userSources(userSources);
            });
    },
    view: ({state, attrs}) => m(Select, {
        id: attrs.user.id,
        label: attrs.user.name,
        value: state.value,
        options: state.options,
    }),
};