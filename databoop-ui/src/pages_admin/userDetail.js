import m from "mithril";
import stream from "mithril/stream";
import Template from "./template";
import {
    Button,
    Card,
    CardTitle,
    CardTitleText,
    CardSupportingText,
    DialogTitle,
    DialogContent,
    DialogActions,
    Input,
    Spinner,
    Checkbox,
    Grid,
    Cell,
    Select,
} from "../components/mdl";
import {Row} from "../components/helpers";
import tellie from "../lib/tellie";
import http from "../services/http";
import UserService from "../services/UserService";
import SourceServce from "../services/SourceService";

export default {
    oninit: ({state, attrs}) => {
        state.user = stream();
        state.userSources = stream();
        state.sources = stream();

        UserService.retrieveUser(attrs.id)
            .then(state.user)
            .then(m.redraw)
            .catch(http.defaultCatch);
        UserService.retrieveSourcesForUser(attrs.id)
            .then(state.userSources)
            .then(m.redraw)
            .catch(http.defaultCatch);
        SourceServce.listSources()
            .then(state.sources)
            .then(m.redraw)
            .catch(http.defaultCatch);

        state.saveUserSources = () => {
            UserService.saveSourcesForUser(attrs.id, state.userSources())
                .then(state.userSources)
                .catch(http.defaultCatch)
                .then(tellie.pubc("dialog:close"))
                .then(m.redraw);
        };

    },
    view: (vnode) => {
        let vs = vnode.state;
        return m(Template, m("div.container", [
            m(Card, {shadow: 8, classes: ["width-100"]}, [
                m(CardTitle, [
                    m(CardTitleText, {h: 5}, "User"),
                ]),
                m(CardSupportingText, [
                    !vs.user() ? m(Spinner) : [
                        m(Row, {right: 1}, [
                            m(Button, {
                                ripple: 1,
                                onclick: () => window.history.go(-1),
                            }, "Back"),
                            m(Button, {
                                ripple: 1, onclick: () => {
                                    tellie.pub("dialog:confirm", "Are you sure you want to delete this user?", () => UserService.deleteUser(vnode.attrs.id)
                                        .then(() => m.route.set("/admin/users")));
                                },
                            }, "Delete"),
                            m(Button, {
                                ripple: 1, onclick: () => {
                                    UserService.saveUser(vs.user())
                                        .then(m.redraw)
                                        .catch(http.defaultCatch);
                                },
                            }, "Save"),
                        ]),
                        m(Grid, [
                            m(Cell, {col: 6}, [
                                m(Input, {
                                    classes: ["block"],
                                    id: "username",
                                    label: "Username",
                                    floating: 1,
                                    value: m.unwrapProp(vs.user, "username"),
                                }),
                                m(Input, {
                                    classes: ["block"],
                                    id: "password",
                                    type: "password",
                                    label: "Password",
                                    floating: 1,
                                    placeholder: "****",
                                    value: m.unwrapProp(vs.user, "password"),
                                }),
                                m(Input, {
                                    classes: ["block"],
                                    id: "name",
                                    label: "Name",
                                    floating: 1,
                                    value: m.unwrapProp(vs.user, "name"),
                                }),
                                m(Input, {
                                    classes: ["block"],
                                    id: "email",
                                    label: "Email",
                                    floating: 1,
                                    value: m.unwrapProp(vs.user, "email"),
                                }),
                            ]),
                            m(Cell, {col: 6}, [
                                m(Checkbox, {
                                    id: "admin",
                                    label: "Admin",
                                    value: m.unwrapProp(vs.user, "admin"),
                                }),
                                m(Checkbox, {
                                    id: "active",
                                    label: "Active",
                                    value: m.unwrapProp(vs.user, "active"),
                                }),
                                vs.user().admin ? "" : m(Button, {
                                    onclick: () => {
                                        tellie.pub("dialog:open", userSourceDialog(vnode));
                                    },
                                }, "Configure Sources"),
                            ]),
                        ]),
                    ],
                ]),
            ]),
        ]));
    },
};

function userSourceDialog({state}) {
    return [
        m(DialogTitle, "Configure Access"),
        m(DialogContent, [
            state.sources().map(source => m(UserSourceItem, {
                source,
                userSources: state.userSources,
            })),
        ]),
        m(DialogActions, [
            m(Button, {
                onclick: state.saveUserSources,
            }, "Save"),
            m(Button, {onclick: tellie.pubc("dialog:close")}, "Cancel"),
        ]),
    ];
}

let UserSourceItem = {
    oninit: ({state, attrs}) => {
        state.options = ["NONE", "DEVELOPER", "REPORTER"];
        state.value = stream();
        let us = attrs.userSources().find(userSource => userSource.sourceId === attrs.source.id);
        if (us) {
            state.value(us.role);
        } else {
            state.value("NONE");
        }
        state.value
            .map(value => {
                let userSources = attrs.userSources(),
                    index = userSources.findIndex(userSource => userSource.sourceId === attrs.source.id);
                if (index > -1 && value === "NONE") {
                    userSources.splice(index, 1);
                } else if (index < 0 && value !== "NONE") {
                    userSources.push({sourceId: attrs.source.id, role: value});
                } else if (index > -1) {
                    userSources[index].role = value;
                }
                attrs.userSources(userSources);
            });
    },
    view: ({state, attrs}) => m(Select, {
        id: attrs.source.id,
        label: attrs.source.name,
        value: state.value,
        options: state.options,
    }),
};