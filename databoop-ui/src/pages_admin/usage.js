import m from "mithril";
import stream from "mithril/stream";
import Template from "./template";
import {
    Button,
    Card,
    CardTitle,
    CardTitleText,
    CardSupportingText,
    Table,
    TableHeader,
    TableCell,
    Spinner,
    Input,
    TextArea,
    DialogTitle,
    DialogContent,
    DialogActions,
} from "../components/mdl";
import tellie from "../lib/tellie";
import http from "../services/http";
import QueryService from "../services/QueryService";

export default {
    oninit: ({state}) => {
        state.auditSummaries = stream();
        QueryService.listAudit()
            .then(state.auditSummaries)
            .then(m.redraw)
            .catch(http.defaultCatch);
        // state.auditSummary = stream({});
        // state.auditDetails = stream();
        // state.auditSummary
        //     .map(audit => {
        //         if (!audit.queryGroup) return;
        //         QueryService.listAuditDetails(audit.queryGroup)
        //             .then(data => {
        //                 state.auditDetails(data);
        //             })
        //             .then(m.redraw)
        //             .catch(http.defaultCatch);
        //     });
    },
    view: ({state}) => {
        return m(Template, m("div.width-100", [
            m(Card, {shadow: 6, classes: ["width-100", "margin-10"]}, [
                m(CardTitle, [
                    m(CardTitleText, {h: 5}, "Usage?"),
                ]),
                m(CardSupportingText, {}, !state.auditSummaries() ? m(Spinner) : [
                    m(Table, {compact: 1, classes: ["width-100"], style: {tableLayout: "fixed"}}, [
                        m("thead", [
                            m("tr", [
                                m(TableHeader, {nonNumeric: 1}, "Source"),
                                m(TableHeader, {nonNumeric: 1}, "Query"),
                                m(TableHeader, {nonNumeric: 0}, "Databases"),
                                m(TableHeader, {nonNumeric: 1}, "User"),
                                m(TableHeader, {nonNumeric: 1}, "Time stamp"),
                            ]),
                        ]),
                        m("tbody", [
                            state.auditSummaries().map(row => m("tr", {
                                onclick: () => {
                                    tellie.pub("loading:on");
                                    QueryService.listAuditDetails(row.queryGroup)
                                        .then(details => {
                                            tellie.pub("loading:off");
                                            tellie.pub("dialog:open", getUsageDetail(row, details));
                                        })
                                        // .then(m.redraw)
                                        .catch(http.defaultCatch);
                                },
                            }, [
                                m(TableCell, {nonNumeric: 1}, row.sourceName),
                                m(TableCell, {
                                    nonNumeric: 1,
                                    style: {overflow: "hidden", textOverflow: "ellipsis"},
                                }, row.query),
                                m(TableCell, {nonNumeric: 0}, row.count),
                                m(TableCell, {nonNumeric: 1}, row.username),
                                m(TableCell, {nonNumeric: 1}, new Date(row.created).toISOString()),
                            ])),
                        ]),
                    ]),
                ]),
            ]),
        ]));
    },
};

function getUsageDetail(summary, details) {
    return [
        m(DialogTitle, "Usage Detail"),
        m(DialogContent, [

            m(Input, {
                id: "sourceName",
                label: "Source Name",
                classes: ["width-100"],
                floating: 1,
                readonly: 1,
                value: summary.sourceName,
            }),
            m(TextArea, {
                id: "query",
                label: "Query",
                classes: ["width-100"],
                floating: 1,
                rows: 4,
                reaodnly: 1,
                value: summary.query,
            }),
            m("div", {
                style: {
                    maxHeight: "300px",
                    overflowY: "auto",
                },
            }, [
                m(Table, {compact: 1, classes: ["width-100"], style: {tableLayout: "fixed"}}, [
                    m("thead", [
                        m("tr", [
                            m(TableHeader, {nonNumeric: 1}, "DB Name"),
                            m(TableHeader, {nonNumeric: 0}, "Query Duration"),
                        ]),
                    ]),
                    m("tbody", [
                        details.map(row => m("tr", [
                            m(TableCell, {nonNumeric: 1}, row.dbName),
                            m(TableCell, {nonNumeric: 0}, row.queryDuration),
                        ])),
                    ]),
                ]),
            ]),
        ]),
        m(DialogActions, [
            m(Button, {onclick: tellie.pubc("dialog:close")}, "Close"),
        ]),
    ];
}