import m from "mithril";
import stream from "mithril/stream";
import Template from "./template";
import {
    Button,
    Card,
    CardTitle,
    CardTitleText,
    CardSupportingText,
    Table,
    TableHeader,
    TableCell,
    Input,
    Icon,
    Spinner,
    DialogTitle,
    DialogContent,
    DialogActions,
    Checkbox,
} from "../components/mdl";
import {Row} from "../components/helpers";
import tellie from "../lib/tellie";
import http from "../services/http";
import UserService from "../services/UserService";

export default {
    oninit: (vnode) => {
        let vs = vnode.state;
        vs.users = stream();

        UserService.listUsers()
            .then(vs.users)
            .then(m.redraw)
            .catch(http.defaultCatch);

        vs.userDetail = stream({});

        vs.addUser = () => UserService.saveUser(vs.userDetail)
            .then(() => UserService.listUsers())
            .then(vs.users)
            .catch(http.defaultCatch)
            .then(tellie.pubc("dialog:close"))
            .then(m.redraw);
    },
    view: (vnode) => {
        let vs = vnode.state;
        return m(Template, m("div.container", [
            m(Card, {shadow: 8, classes: ["width-100"]}, [
                m(CardTitle, [
                    m(CardTitleText, {h: 5}, "Users"),
                ]),
                m(CardSupportingText, [
                    !vs.users() ? m(Spinner) : [
                        m(Row, {right: 1}, [
                            m(Button, {
                                icon: 1,
                                onclick: () => {
                                    vs.userDetail({});
                                    tellie.pub("dialog:open", getUserDialog(vnode, false));
                                },
                            }, m(Icon, "add")),
                        ]),
                        m(Table, {style: {width: "100%"}}, [
                            m("thead", m("tr", [
                                m(TableHeader, {nonNumeric: 1}, "Edit"),
                                m(TableHeader, {nonNumeric: 1}, "Name"),
                                m(TableHeader, {nonNumeric: 1}, "Admin"),
                                m(TableHeader, {nonNumeric: 1}, "Created"),
                            ])),
                            m("tbody", [
                                vs.users().map(user => m("tr", [
                                    m(TableCell, {nonNumeric: 1}, m("a", {
                                        href: `/admin/user/${user.id}`,
                                        oncreate: m.route.link,
                                    }, m(Icon, "edit"))),
                                    m(TableCell, {nonNumeric: 1}, user.name),
                                    m(TableCell, {nonNumeric: 1}, user.admin ? m(Icon, "check") : ""),
                                    m(TableCell, {nonNumeric: 1}, new Date(user.created).toISOString()),
                                ])),
                            ]),
                        ]),
                    ],
                ]),
            ]),
        ]));
    },
};

function getUserDialog(vnode, existing) {
    let vs = vnode.state;
    return [
        m(DialogTitle, existing ? "Edit" : "New" + " User"),
        m(DialogContent, {
            onkeyup: (e) => {
                if (e.keyCode === 13) {
                    vs.addUser();
                }
            },
        }, [
            m(Input, {
                id: "username",
                label: "Username",
                floating: 1,
                value: m.unwrapProp(vs.userDetail, "username"),
            }),
            m(Input, {
                id: "password",
                type: "password",
                label: "Password",
                floating: 1,
                value: m.unwrapProp(vs.userDetail, "password"),
            }),
            m(Input, {
                id: "name",
                label: "Name",
                floating: 1,
                value: m.unwrapProp(vs.userDetail, "name"),
            }),
            m(Input, {
                id: "email",
                label: "Email",
                floating: 1,
                value: m.unwrapProp(vs.userDetail, "email"),
            }),
            m(Checkbox, {
                id: "admin",
                label: "Admin",
                value: m.unwrapProp(vs.userDetail, "admin"),
            }),
        ]),
        m(DialogActions, [
            m(Button, {
                onclick: vs.addUser,
            }, existing ? "Save" : "Create"),
            m(Button, {onclick: tellie.pubc("dialog:close")}, "Cancel"),
        ]),
    ];
}