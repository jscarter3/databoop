import m from "mithril";
import {Button, Icon, Menu, MenuItem} from "../components/mdl";
import DialogComponent from "../components/DialogComponent";
import LoadingComponent from "../components/LoadingComponent";
import SnackbarComponent from "../components/SnackbarComponent";
import Auth from "../services/AuthService";

export default {
    oninit: (vnode) => {
        vnode.state.tabs = [
            {
                path: "#/admin/usage",
                name: "Usage",
            },{
                path: "#/admin/users",
                name: "Users",
            },{
                path: "#/admin/actuator",
                name: "Actuator",
            },
        ];
    },
    view: (vnode) => m("div.mdl-layout.mdl-layout--fixed-header"/*.mdl-js-layout*/, {
        // oncreate: (vnode) => componentHandler.upgradeElement(vnode.dom),
    }, [
        m("header.mdl-layout__header.dark-bg", [
            m("div.mdl-layout__header-row", [
                m("span.mdl-layout-title", m("a.no-decoration", {href: "#/"}, "DataBoop")),
                m("nav.mdl-navigation", [
                    vnode.state.tabs.map(makeTab),
                ]),
                m("div.mdl-layout-spacer"),
                m(Button, {"id": "nav-menu", ripple: 1, style: {color: "white"}}, [Auth.getUser().name, m(Icon, "expand_more")]),
                m(Menu, {"for": "nav-menu", ripple: 1, bottomRight: 1}, [
                    m(MenuItem, {divider: 1}, m("a.no-decoration", {href: "/", oncreate: m.route.link}, "Home")),
                    m(MenuItem, m("a.no-decoration", {href: "/myaccount", oncreate: m.route.link}, "My Account")),
                    m(MenuItem, {divider: 1}, m("a.no-decoration", {href: "/about", oncreate: m.route.link}, "About")),
                    m(MenuItem, {onclick: () => Auth.logout().then(() => m.route.set("/login"))}, "Log out"),
                ]),
            ]),
        ]),
        m("main.mdl-layout__content", vnode.children),
        m(DialogComponent),
        m(LoadingComponent),
        m(SnackbarComponent),
    ]),
};

function makeTab(tab) {
    let classes = ["mdl-navigation__link"];
    if (m.route.get() === tab.path) classes.push("is-active");

    return m("a", {
        class: classes.join(" "),
        href: tab.path,
        oncreate: () => m.route.link,
    }, tab.name);
}