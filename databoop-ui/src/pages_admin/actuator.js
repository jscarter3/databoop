import m from "mithril";
import stream from "mithril/stream";
import Template from "./template";
import {
    Button,
    Card,
    CardTitle,
    CardTitleText,
    CardSupportingText,
    Grid,
    Cell,
} from "../components/mdl";
import http from "../services/http";

export default {
    oninit: (vnode) => {
        let vs = vnode.state;
        vs.data = stream();

        vs.loadJson = (endpoint) => http.send("GET", `/actuator/${endpoint}`)
            .then(http.json)
            .then(data => JSON.stringify(data, undefined, 4))
            .then(vs.data)
            .then(m.redraw)
            .catch(http.defaultCatch);

        vs.loadJson("health");

        vs.loadText = (endpoint) => http.send("GET", `/actuator/${endpoint}`, {
            headers: {
                "Range": "bytes=-100000",
            },
        })
            .then(data => data.responseText)
            .then(vs.data)
            .then(m.redraw)
            .catch(http.defaultCatch);

    },
    view: (vnode) => {
        let vs = vnode.state;
        return m(Template, m(Grid, [
            m(Cell, {col: 8, offset: 2}, [
                m(Card, {shadow: 8, classes: ["width-100"]}, [
                    m(CardTitle, [
                        m(CardTitleText, {h: 5}, "Actuator"),
                    ]),
                    m(CardSupportingText, [
                        m(Button, {onclick: () => vs.loadJson("health")}, "Health"),
                        m(Button, {onclick: () => vs.loadJson("info")}, "Info"),
                        m(Button, {onclick: () => vs.loadJson("env")}, "Env"),
                        m(Button, {onclick: () => vs.loadJson("metrics")}, "Metrics"),
                        // m(Button, {onclick: () => vs.loadJson("beans")}, "Beans"),
                        m(Button, {onclick: () => vs.loadJson("flyway")}, "Flyway"),
                        // m(Button, {onclick: () => vs.loadText("logfile")}, "Log File"),
                        // m(Button, {onclick: () => vs.loadJson("auditevents")}, "Audit Events"),
                        // m(Button, {onclick: () => vs.loadJson("mappings")}, "Mappings"),
                        // m(Button, {onclick: () => vs.loadJson("autoconfig")}, "Autoconfig"),
                        // m(Button, {onclick: () => vs.loadJson("configprops")}, "Config Props"),
                        // m(Button, {onclick: () => vs.loadJson("trace")}, "Trace"),
                        // m(Button, {onclick: () => vs.loadJson("dump")}, "Thread Dump"),
                    ]),
                ]),
                m(Card, {shadow: 8, classes: ["width-100"], style: {marginTop: "15px"}}, [
                    m(CardSupportingText, [
                        m("pre", {
                            style: {
                                overflowX: "auto",
                            },
                        }, [
                            vs.data() ? vs.data() : "",
                        ]),
                    ]),
                ]),
            ]),
        ]));
    },
};