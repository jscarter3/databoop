import m from "mithril";
import stream from "mithril/stream";
import {
    Button,
    Checkbox,
} from "../components/mdl";
import {Row} from "../components/helpers";

export default {
    oninit: () => {},
    oncreate: (vnode) => {
        vnode.state.el = vnode.dom;
    },
    view: (vnode) => {
        let va = vnode.attrs,
            vs = vnode.state;

        return m("div", [
            m(Row, [
                m(Button, {
                    onclick: () => vs.el.querySelectorAll(".tenant:not(.is-checked) input").forEach(input => input.click()),
                }, "All"),
                m(Button, {
                    onclick: () => vs.el.querySelectorAll(".tenant.is-checked input").forEach(input => input.click()),
                }, "Clear"),
            ]),
            va.tenants().map(tenant => {
                return m(Tenant, {tenant, selectedTenants: va.selectedTenants});
            }),
        ]);
    },
};

let Tenant = {
    oninit: (vnode) => {
        let va = vnode.attrs,
            vs = vnode.state;

        vs.selected = stream(va.selectedTenants().includes(va.tenant.id));
        vs.selected
            .map(selected => {
                let selectedTenants = va.selectedTenants(),
                    index = selectedTenants.indexOf(va.tenant.id);
                if (index > -1 && !selected) {
                    selectedTenants.splice(index, 1);
                } else if (index < 0 && selected) {
                    selectedTenants.push(va.tenant.id);
                }
                va.selectedTenants(selectedTenants);
            });
    },
    view: (vnode) => {
        let va = vnode.attrs;
        return m(Checkbox, {
            id: va.tenant.id,
            classes: ["tenant"],
            label: va.tenant.name,
            value: vnode.state.selected,
        });
    },
};