import m from "mithril";
import stream from "mithril/stream";
import Template from "./template";
import {
    Card,
    CardTitle,
    CardTitleText,
    CardSupportingText,
    Icon,
    Spinner,
    Table,
    TableHeader,
    TableCell,
} from "../components/mdl";
import {Row} from "../components/helpers";
import http from "../services/http";
import SourceService from "../services/SourceService";

export default {
    oninit: ({state, attrs}) => {

        state.queries = stream();
        SourceService.listQueries(attrs.sourceId)
            .then(state.queries)
            .then(m.redraw)
            .catch(http.defaultCatch);
    },
    view: ({state, attrs}) => m(Template, {sourceId: attrs.sourceId}, [
        m("div.container", [
            m(Card, {shadow: 8, classes: ["width-100"]}, [
                m(CardTitle, [
                    m(CardTitleText, {h: 5}, "Queries"),
                ]),
                m(CardSupportingText, [
                    !state.queries() ? m(Spinner) : [
                        m(Row, {right: 1}, [
                            m("a.no-decoration", {
                                href: `/boop/${attrs.sourceId}/playground`,
                                oncreate: m.route.link,
                            }, m(Icon, "add")),
                        ]),
                        m(Table, {classes: ["width-100"]}, [
                            m("thead", m("tr", [
                                m(TableHeader, {nonNumeric: 1}, "Edit"),
                                m(TableHeader, {nonNumeric: 1}, "Name"),
                            ])),
                            m("tbody", [
                                state.queries().map(query => m("tr", [
                                    m(TableCell, {nonNumeric: 1}, m("a", {
                                        href: `/boop/${attrs.sourceId}/playground/${query.id}`,
                                        oncreate: m.route.link,
                                    }, m(Icon, "edit"))),
                                    m(TableCell, {nonNumeric: 1}, m("a", {
                                        href: `/boop/${attrs.sourceId}/query/${query.id}`,
                                        oncreate: m.route.link,
                                    }, query.name)),
                                ])),
                            ]),
                        ]),
                    ],
                ]),
            ]),
        ]),
    ]),
};