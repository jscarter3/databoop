import m from "mithril";
import stream from "mithril/stream";
import {
    Spinner,
    Button,
    Input,
    Icon,
} from "../components/mdl";
import {Row} from "../components/helpers";
import http from "../services/http";
import SourceService from "../services/SourceService";
import Store from "../services/Store";
import tellie from "../lib/tellie";

export default {
    oninit: (vnode) => {
        let vs = vnode.state;

        vs.searchText = stream("");
        vs.schema = stream();
        vs.filteredSchema = stream();

        vs.refreshSchema = function(force) {
            let key = `schema_${vnode.attrs.sourceId}`,
                schema = force ? null : Store.get(key);
            if (schema) {
                vs.schema(schema.sort((a, b) => a.name.toLowerCase().localeCompare(b.name.toLowerCase())));
            } else {
                SourceService.getSchema(vnode.attrs.sourceId, force)
                    .then(tables => {
                        Store.set(key, tables, false);
                        return tables;
                    })
                    .then(tables => tables.sort((a, b) => a.name.toLowerCase().localeCompare(b.name.toLowerCase())))
                    .then(vs.schema)
                    .then(m.redraw)
                    .catch(http.defaultCatch);
            }
        };
        vs.refreshSchema();

        let tableFilter = (table) => table.name.toLowerCase().indexOf(vs.searchText().toLowerCase()) >= 0;

        function filterTables() {
            if (vs.schema()) {
                vs.filteredSchema(vs.schema().filter(tableFilter));
            }
        }
        vs.searchText.map(filterTables);
        vs.schema.map(filterTables);
        vs.style = {
            overflowY: "auto",
        };
    },
    onremove: ({state}) => tellie.unsub("window:resize", state.resizeSubId),
    oncreate: ({state}) => {
        state.resizeSubId = tellie.sub("window:resize", updateHeight);
        function updateHeight() {
            state.style.maxHeight = `${window.innerHeight - 120}px`;
            m.redraw();
        }
        updateHeight();
    },
    view: (vnode) => {
        let vs = vnode.state;
        return m("div.mdl-shadow--6dp.padding-10.schema-view", {
            style: vs.style,
        }, [
            m(Input, {
                floating: 1,
                value: vs.searchText,
                label: "Search Tables",
            }),
            !vs.filteredSchema() ? m(Spinner) : vs.filteredSchema().map(table => m(SchemaTable, {table: table})),
            m(Row, [
                m(Button, {
                    onclick: () => {
                        vs.filteredSchema(undefined);
                        vs.refreshSchema(true);
                    },
                }, "Refresh"),
            ]),
        ]);
    },
};

let SchemaTable = {
    oninit: (vnode) => {
        vnode.state.open = stream(false);
    },
    view: (vnode) => {
        let vs = vnode.state,
            va = vnode.attrs;

        return m("div.mdl-shadow--2dp", {
            onclick: () => vs.open(!vs.open()),
            style: {margin: "10px 0", padding: "5px"},
        }, [
            m("div", {
            }, [
                m("span", va.table.name),
                // m(Icon, {class: ["pull-right"]}, `arrow_drop_${vs.open() ? "up" : "down"}`),
                m(Icon, {class: ["pull-right"]}, `expand_${vs.open() ? "less" : "more"}`),
            ]),
            !vs.open() ? "" : [
                m("div", {style: {height: "5px"}}),
                va.table.columns.map(col => m("div", {
                    style: {paddingLeft: "20px", fontSize: "13px"},
                }, [
                    m("span", col.name),
                    m("span.pull-right", col.type), // + ((col.type === "VARCHAR") ? `(${col.size})` : "")),
                ])),
            ],
        ]);
    },
};