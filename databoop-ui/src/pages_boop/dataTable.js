import m from "mithril";
import stream from "mithril/stream";
import {
    Button,
    Icon,
    Table,
    TableHeader,
    TableCell,
} from "../components/mdl";
import tellie from "../lib/tellie";
// import JSONViewer from "./JSONViewer";

export default {
    oninit: ({state, attrs}) => {
        let vs = state,
            query = attrs.query,
            data = attrs.data;

        vs.header = stream();
        vs.rows = stream();

        vs.sortAsc = null;
        vs.sortColumn = null;

        vs.page = 1;
        // vs.maxPages;
        vs.pageSize = 500;

        data.map(d => {
            vs.sortAsc = null;
            vs.sortColumn = null;
            vs.header(d.header);
            vs.rows(d.data);
            vs.page = 1;
            if (d.data) vs.maxPages = Math.ceil(d.data.length/vs.pageSize);
        });


        vs.export = () => {
            let file = vs.header().map(h => escape(h.name)).join(",") + "\n",
                name = query() && query().name ? query().name : "databoop_export";
            vs.rows().forEach(row => {
                file = file + row.map(escape).join(",") + "\n";
            });

            download(`${name}.csv`, file);
        };

        function escape(val) {
            if (R.is(String, val) && val.length > 0) {
                val = val.replace(/"/g, "\\\"");
                if (val.charAt(0) !== "\"") val = "\"" + val;
                if (val.charAt(val.length - 1) !== "\"") val = val + "\"";
            }
            return val;
        }

        vs.changeSortDirection = () => {
            vs.sortAsc = !vs.sortAsc;
            vs.rows(R.reverse(vs.rows()));
        };

        vs.changeSortColumn = (col) => {
            vs.sortAsc = true;
            vs.sortColumn = col;
            let func = getSortFunction(vs.header(), col);
            vs.rows(func(vs.rows()));
        };

        vs.updateHeight = () => {
            if (vs.dom && vs.dom.getBoundingClientRect) {
                vs.tableHeight = `${window.innerHeight - vs.dom.getBoundingClientRect().top - 72}px`;
                vs.dom.getElementsByClassName("data-table-body")[0].style.maxHeight = vs.tableHeight;
            }
        };

    },
    onupdate: ({state, dom}) => {
        if (state.dom !== dom) {
            state.dom = dom;
            state.updateHeight();
        }
    },
    onremove: ({state}) => tellie.unsub("window:resize", state.resizeSubId),
    oncreate: ({state}) => {
        state.resizeSubId = tellie.sub("window:resize", state.updateHeight);
    },
    view: (vnode) => {
        let vs = vnode.state,
            idxOffset = (vs.page - 1) * vs.pageSize;

        if (!vs.header() || !vs.rows()) {
            return "";
        }
        return [
            m("div.data-table", {style: {paddingTop: "15px"}}, [
                m("div.mdl-shadow--6dp", [
                    m("div.data-table-header", [
                        m("span", "Page:"),
                        m(Button, {
                            icon: 1,
                            onclick: () => vs.page > 1 ? vs.page-- : "",
                        }, m(Icon, "keyboard_arrow_left")),
                        m("input", {
                            value: vs.page,
                            onchange: m.withAttr("value", (val) => {
                                if (!isNaN(val)) {
                                    val = parseInt(val);
                                    vs.page = R.clamp(1, vs.maxPages, val);
                                }
                            }),
                            style: {
                                width: "20px",
                                textAlign: "right",
                            },
                        }),
                        m("span", `/${vs.maxPages}`),
                        m(Button, {
                            icon: 1,
                            onclick: () => vs.page < vs.maxPages ? vs.page++ : "",
                        }, m(Icon, "keyboard_arrow_right")),
                        m("span", "Rows: "),
                        m("input", {
                            value: vs.pageSize,
                            onchange: m.withAttr("value", (val) => {
                                if (!isNaN(val)) {
                                    val = parseInt(val);
                                    vs.pageSize = R.clamp(1, 10000, val);
                                    vs.maxPages = Math.ceil(vs.rows().length/vs.pageSize);
                                    vs.page = R.clamp(1, vs.maxPages, vs.page);
                                }
                            }),
                            style: {
                                width: "40px",
                                textAlign: "right",
                            },
                        }),
                        m(Button, {onclick: vs.export}, "Export"),
                        m(Button, {
                            icon: 1,
                            style: {float: "right"},
                            onclick: () => console.log("expand!"),
                        }, m(Icon, "fullscreen")),
                    ]),
                    m("div.data-table-body", {
                        style: {overflow: "auto"},
                    }, [
                        m(Table, {compact: 1}, [
                            m("thead", [
                                m(TableHeader, "#"),
                                vs.header().map((h, i) => m(TableHeader, {
                                    ascending: i === vs.sortColumn && vs.sortAsc,
                                    descending: i === vs.sortColumn && !vs.sortAsc,
                                    nonNumeric: isNonNumeric(h.dataType),
                                    onclick: () => {
                                        if (i === vs.sortColumn) {
                                            vs.changeSortDirection();
                                        } else {
                                            vs.changeSortColumn(i);
                                        }
                                    },
                                }, h.name)),
                            ]),
                            m("tbody", [
                                vs.rows().slice(idxOffset, vs.page * vs.pageSize).map((row, i) => m("tr", [
                                    m("td", idxOffset + i + 1),
                                    row.map((cell, j) => m(TableCell, {
                                        nonNumeric: isNonNumeric(vs.header()[j].dataType),
                                    }, formatData(cell, vs.header()[j].dataType))),
                                ])),
                            ]),
                        ]),
                    ]),
                ]),
            ]),
        ];
    },
};

function formatData(data, dataType) {
    switch (dataType) {
        case "DATE":
        case "TIME":
        case "TIMESTAMP":
            try {
                return new Date(data).toISOString();
            } catch (e) {
                return data;
            }
        case "BOOLEAN":
        case "LONG":
        case "DOUBLE":
        case "BIGDECIMAL":
            return data;
        case "CLOB":
        case "STRING":
            return data;
            // try {
            //     let d = JSON.parse(data);
            //     if (R.contains(R.type(d), ["Object","Array"])) {
            //         return m(JSONViewer, {collapsed: true, data: d});
            //     } else {
            //         return data;
            //     }
            // } catch (err) {
            //     return data;
            // }
        default:
            console.log(`Unmapped type: ${dataType}`);
            return data;
    }
}

function getSortFunction(header, index) {
    if (!header) {
        return R.identity; // return input
    }
    switch (header[index].dataType) {
        case "DATE":
        case "TIME":
        case "TIMESTAMP":
        case "BOOLEAN":
        case "LONG":
        case "DOUBLE":
        case "BIGDECIMAL":
            return R.sortBy(R.prop(index));
        case "CLOB":
        case "STRING":
            return R.sortBy(row => {
                let d = row[index];
                return d ? d.toLowerCase() : d;
            });
        default:
            return R.identity; // return input
    }
}

function isNonNumeric(dataType) {
    switch (dataType) {
        case "LONG":
        case "DOUBLE":
            return false;
        case "BIGDECIMAL": // Returned as string
        case "DATE":
        case "TIME":
        case "TIMESTAMP":
        case "BOOLEAN":
        case "CLOB":
        case "STRING":
            return true;
        default:
            console.log(`Unmapped type: ${dataType}`);
            return false;
    }
}

function download(filename, text) {
    let element = document.createElement("a");
    element.setAttribute("href", "data:text/plain;charset=utf-8," + encodeURIComponent(text));
    element.setAttribute("download", filename);

    element.style.display = "none";
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
}