import m from "mithril";
import stream from "mithril/stream";
import Template from "./template";
import {
    Button,
    Card,
    CardTitle,
    CardTitleText,
    CardSupportingText,
} from "../components/mdl";
import DataTable from "./dataTable";
import http from "../services/http";
import tellie from "../lib/tellie";
import QueryService from "../services/QueryService";

export default {
    oninit: ({state, attrs}) => {
        state.query = stream();
        state.data = stream({});

        QueryService.retrieveQuery(attrs.queryId)
            .then(state.query)
            .then(m.redraw)
            .catch(http.defaultCatch);

        let q;

        function cancelQ() {
            if (q) {
                q.close();
                tellie.pub("loading:off");
                m.redraw();
            }
        }
        tellie.pub("loading:on", [
            m("div", "Loading data..."),
            m(Button, {
                raised: 1,
                primary: 1,
                colored: 1,
                onclick: cancelQ,
            }, "Cancel"),
        ]);
        q = QueryService.runQuery({
            id: attrs.queryId,
        }, {
            success: (data) => {
                state.data(data);
                m.redraw();
            },
            error: (err) => {
                let msg = "";
                if (R.is(Object, err)) {
                    R.forEachObjIndexed((value, key) => {
                        msg += `${key}: ${value}\n`;
                    }, err);
                } else {
                    msg = err;
                }
                tellie.pub("dialog:alert", msg);
            },
            status: (status) => {
                tellie.pub("loading:on", [
                    m("div", "Loading data..."),
                    m("div", `${status.done} of ${status.total} completed`),
                    m(Button, {
                        raised: 1,
                        primary: 1,
                        colored: 1,
                        onclick: cancelQ,
                    }, "Cancel"),
                ]);
                if (status.done === status.total) {
                    tellie.pub("loading:off");
                }
                m.redraw();
            },
        });
    },
    view: ({attrs, state}) => {
        return m(Template, {sourceId: attrs.sourceId}, m("div.container", [
            m(Card, {shadow: 8, classes: ["width-100"]}, [
                m(CardTitle, [
                    m(CardTitleText, {h: 5}, "Query Detail"),
                ]),
                m(CardSupportingText, [
                    m(DataTable, {
                        query: state.query,
                        data: state.data,
                    }),
                ]),
            ]),
        ]));
    },
};
//
// let Plot = {
//     oncreate: ({dom}) => {
//         let data = [
//             {
//                 x: ['giraffes', 'orangutans', 'monkeys'],
//                 y: [20, 14, 23],
//                 type: 'bar'
//             }
//         ];
//         Plotly.newPlot(dom, data);
//     },
//     view: () => m("div"),
// };

/*

 TESTER = document.getElementById('tester');
 Plotly.plot( TESTER, [{
 x: [1, 2, 3, 4, 5],
 y: [1, 2, 4, 8, 16] }], {
 margin: { t: 0 } } );

 */