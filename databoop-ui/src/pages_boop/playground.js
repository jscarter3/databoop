/*global R*/
import m from "mithril";
import stream from "mithril/stream";
import Template from "./template";
import {
    Button,
    Card,
    CardSupportingText,
    CardTitle,
    CardTitleText,
    CardActions,
    Grid,
    Cell,
    Input,
    TextArea,
    DialogTitle,
    DialogContent,
    DialogActions,
    Spinner,
} from "../components/mdl";
import http from "../services/http";
import SourceService from "../services/SourceService";
import QueryService from "../services/QueryService";
import tellie from "../lib/tellie";
import SchemaView from "./schemaView";
import TenantSelection from "./tenantSelection";
import DataTable from "./dataTable";
import Store from "../services/Store";

export default {
    oninit: (vnode) => {
        let va = vnode.attrs,
            vs = vnode.state;

        vs.source = stream();
        vs.tenants = stream();
        SourceService.retrieveSource(va.sourceId)
            .then(vs.source)
            .then(() => SourceService.listConnections(va.sourceId))
            .then(connections => connections.filter(conn => conn.active))
            .then(vs.tenants)
            .catch(http.defaultCatch)
            .then(m.redraw);

        let queryKey = `playground_${va.sourceId}`;

        vs.query = stream();
        vs.query
            .map(query => !query.id ? Store.set(queryKey, query) : undefined);

        if (va.queryId) {
            QueryService.retrieveQuery(va.queryId)
                .then(vs.query)
                .catch(http.defaultCatch)
                .then(m.redraw);
        } else {
            vs.query(Store.get(`${queryKey}`) || {
                sourceId: va.sourceId,
                connectionIds: [],
                query: "",
            });
        }
        vs.selectedQuery = stream("");

        vs.data = stream({});

        vs.saveQuery = () => QueryService.saveQuery(vs.query)
            .then(vs.query)
            .then(tellie.pubc("dialog:close"))
            .then(() => {
                if (!va.queryId) {
                    Store.clear(queryKey);
                    m.route.set(`/boop/${va.sourceId}/playground/${vs.query().id}`);
                }
            })
            .catch(http.defaultCatch)
            .then(m.redraw);

        vs.runQuery = () => {
            let q;

            function cancelQ() {
                if (q) {
                    q.close();
                    tellie.pub("loading:off");
                    m.redraw();
                }
            }
            tellie.pub("loading:on", [
                m("div", "Running query..."),
                m(Button, {
                    raised: 1,
                    primary: 1,
                    colored: 1,
                    onclick: cancelQ,
                }, "Cancel"),
            ]);
            q = QueryService.runQuery({
                sourceId: va.sourceId,
                connectionIds: vs.query().connectionIds,
                query: vs.selectedQuery() ? vs.selectedQuery() : vs.query().query,
            }, {
                success: (data) => {
                    vs.data(data);
                    m.redraw();
                },
                error: (err) => {
                    let msg = "";
                    if (R.is(Object, err)) {
                        R.forEachObjIndexed((value, key) => {
                            msg += `${key}: ${value}\n`;
                        }, err);
                    } else {
                        msg = err;
                    }
                    tellie.pub("dialog:alert", msg);
                },
                status: (status) => {
                    tellie.pub("loading:on", [
                        m("div", "Running query..."),
                        m("div", `${status.done} of ${status.total} completed`),
                        m(Button, {
                            raised: 1,
                            primary: 1,
                            colored: 1,
                            onclick: cancelQ,
                        }, "Cancel"),
                    ]);
                    if (status.done === status.total) {
                        tellie.pub("loading:off");
                    }
                    m.redraw();
                },
            });
        };
    },
    view: (vnode) => {
        let va = vnode.attrs,
            vs = vnode.state;
        return !vs.source() ? "" : m(Template, {sourceId: va.sourceId}, m(Grid, [
            m(Cell, {col: 2}, m(SchemaView, {sourceId: va.sourceId})),
            m(Cell, {col: 10}, !vs.query() ? m(Spinner) : [
                m(Card, {
                    shadow: 6,
                    classes: ["width-100"],
                    style: {
                        minHeight: "inherit",
                    },
                }, [
                    vs.query().name ? m(CardTitle, {
                        style: {
                            padding: "5px 16px",
                        },
                    }, [
                        m(CardTitleText, {h: 5}, vs.query().name),
                    ]) : "",
                    m(CardSupportingText, {
                        style: {
                            padding: "5px 16px",
                        },
                    }, [
                        m(TextArea, {
                            label: "Query",
                            floating: 1,
                            rows: 4,
                            classes: ["width-100"],
                            value: m.unwrapProp(vs.query, "query"),
                            selected: vs.selectedQuery,
                            onkeydown: (e) => {
                                if (e.ctrlKey && e.keyCode === 13) {
                                    vs.runQuery();
                                }
                            }}),
                    ]),
                    m(CardActions, [
                        m(Button, {
                            onclick: vs.runQuery,
                        }, "Run"),
                        vs.source() && vs.source().multiTenant && vs.tenants() ? [
                            m(Button, {
                                onclick: () => {
                                    tellie.pub("dialog:open", [
                                        m(DialogTitle, "Select Tenants"),
                                        m(DialogContent, {
                                            style: {
                                                maxHeight: "400px",
                                                overflowY: "auto",
                                            },
                                        }, m(TenantSelection, {
                                            tenants: vs.tenants,
                                            selectedTenants: m.unwrapProp(vs.query, "connectionIds"),
                                        })),
                                        m(DialogActions, [
                                            m(Button, {onclick: tellie.pubc("dialog:close")}, "Done"),
                                        ]),
                                    ]);
                                },
                            }, `Select Tenants (${[0, vs.tenants().length].includes(vs.query().connectionIds.length) ? "All" : vs.query().connectionIds.length})`),
                        ] : "",
                        m(Button, {
                            onclick: () => {
                                tellie.pub("dialog:open", [
                                    m(DialogTitle, "Save Query"),
                                    m(DialogContent, m(Input, {
                                        id: "query_name",
                                        label: "Name",
                                        floating: 1,
                                        value: m.unwrapProp(vs.query, "name"),
                                    })),
                                    m(DialogActions, [
                                        m(Button, {
                                            onclick: vs.saveQuery,
                                        }, "Save"),
                                        m(Button, {onclick: tellie.pubc("dialog:close")}, "Cancel"),
                                    ]),
                                ]);
                            },
                        }, "Save Query"),
                    ]),
                ]),
                m(DataTable, {
                    query: vs.query,
                    data: vs.data,
                }),
            ]),
        ]));
    },
};