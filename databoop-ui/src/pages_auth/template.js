import m from "mithril";
import DialogComponent from "../components/DialogComponent";
import LoadingComponent from "../components/LoadingComponent";
import SnackbarComponent from "../components/SnackbarComponent";

export default {
    oninit: () => {},
    view: (vnode) => m("div.mdl-layout.mdl-layout--fixed-header"/*.mdl-js-layout*/, {
    }, [
        m("header.mdl-layout__header", [
            m("div.mdl-layout__header-row", [
                m("span.mdl-layout-title", m("a.no-decoration", {href: "#/"}, "DataBoop")),
            ]),
        ]),
        m("main.mdl-layout__content", vnode.children),
        m(DialogComponent),
        m(LoadingComponent),
        m(SnackbarComponent),
    ]),
};