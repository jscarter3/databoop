import m from "mithril";
import stream from "mithril/stream";
import Template from "./template";
import {Button, Card, CardTitle, CardTitleText, CardSupportingText, CardActions, Input} from "../components/mdl";
import Auth from "../services/AuthService";
import http from "../services/http";
import tellie from "../lib/tellie";

export default {
    oninit: (vnode) => {
        let vs = vnode.state;
        vs.loginRequest = stream({});
        vs.error = stream("");
        vs.login = () => {
            tellie.pub("loading:on", "Logging in...");
            Auth.login(vs.loginRequest())
                .then(() => m.route.set("/"))
                .catch(http.defaultCatch)
                .catch((err) => {vs.error(err.message);})
                .then(tellie.pubc("loading:off"));
        };
    },
    view: (vnode) => {
        let vs = vnode.state;
        return m(Template, [
            m("div.container", [
                m(Card, {
                    onkeyup: (e) => {
                        if (e.keyCode === 13) {
                            vs.login();
                        }
                    },
                    shadow: 8,
                    style: {margin: "auto"},
                }, [
                    m(CardTitle, [
                        m(CardTitleText, {h: 3}, "Please log in"),
                    ]),
                    m(CardSupportingText, [
                        m(Input, {
                            id: "username",
                            label: "Username",
                            floating: 1,
                            value: m.unwrapProp(vs.loginRequest, "username"),
                        }),
                        m(Input, {
                            id: "password",
                            label: "Password",
                            floating: 1,
                            type: "password",
                            value: m.unwrapProp(vs.loginRequest, "password"),
                        }),
                        m("p", {style: {color: "red"}}, vs.error()),
                    ]),
                    m(CardActions, [
                        m(Button, {onclick: vs.login}, "Log In"),
                    ]),
                ]),
            ]),
        ]);
    },
};