"use strict";
let tellie = {},
    channels = {},
    uuid = 0;

function isObject(object) {
    return {}.toString.call(object) === "[object Object]";
}

function channelHop(channel) {
    return {}.hasOwnProperty.call(channels, channel);
}

tellie.sub = function(channel, callback) {
    if (!channelHop(channel) || !isObject(channels[channel])) channels[channel] = {};
    let id = "" + uuid++;
    channels[channel][id] = callback;
    return id;
};

tellie.unsub = function(channel, id) {
    if (!channelHop(channel)) return;
    if (isObject(channels[channel])) delete channels[channel][id];
};

tellie.pub = function() {
    let args = Array.prototype.slice.call(arguments);
    if (args.length > 0) {
        let channel = args[0];
        if (!channelHop(channel) || !isObject(channels[channel])) return;
        args.splice(0, 1);
        Object.keys(channels[channel]).forEach(function(key) {
            channels[channel][key].apply(null, args);
        });
    }
};

tellie.pubc = function(channel) {
    return function() {
        let args = Array.prototype.slice.call(arguments);
        args.unshift(channel);
        tellie.pub(...args);
    };
};

tellie.reply = function(channel, callback) {
    channels[channel] = callback;
};

tellie.request = function() {
    let args = Array.prototype.slice.call(arguments);
    if (args.length > 0) {
        let channel = args[0];
        if (!channelHop(channel) || isObject(channels[channel])) return;
        args.splice(0, 1);
        return channels[channel].call(null, args);
    }
};

tellie.reset = function(channel) {
    if (channel) {
        let toRemove = [];
        if (channel instanceof RegExp) {
            Object.keys(channels).forEach(function (key) {
                if (key.match(channel)) toRemove.push(key);
            });
        } else toRemove.push(channel);
        toRemove.forEach(function(key) {
            delete channels[key];
        });
    } else channels = {};
};

tellie.resetc = function(channel) {
    return function() {
        tellie.reset(channel);
    };
};

export default tellie;