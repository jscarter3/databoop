import m from "./mithril";

m.unwrapProp = function (object, field) {
    let unwrapped = m.prop(object()[field]);
    unwrapped.run(v => {
        let data = object();
        data[field] = v;
        object(data);
    });
    return unwrapped;
};

export default m;