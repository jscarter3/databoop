let R = require("ramda");

let data = [
    "a",
    "b",
    "c"
];

let mapping = [
    2,
    1,
    0,
    -1,
];

// let f = R.ifElse(R.equals(true), (v) => console.log("boop", v), (v) => console.log("beep", v));
// f(true);
// f(false);
// let f = R.ifElse(R.equals(-1), (v) => "-DNE-" , (v) => data[v]);
let f = R.ifElse(R.equals(-1), R.always("-DNE-") , R.nth(R.__, data));
// console.log(f(-1));
// console.log(f(0));
console.log(R.map(f, mapping));

console.log(R.map(R.ifElse(R.equals(-1), "boop", R.nth(R.__, data), R.__), mapping));

// console.log(R.map(R.nth(R.__, data), mapping));