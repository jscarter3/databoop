// let template = {
//     view: (vnode) => m("div.mdl-layout.mdl-js-layout.mdl-layout--fixed-header", {
//         oncreate: (vnode) => componentHandler.upgradeElement(vnode.dom),
//     }, [
//         m("header.mdl-layout__header", [
//             m("div.mdl-layout__header-row", [
//                 m("span.mdl-layout--title", "DataBoop"),
//                 m("nav.mdl-navigation", [
//                     m("a.mdl-navigation__link", {href: "/1", oncreate: m.route.link}, "page 1"),
//                     m("a.mdl-navigation__link", {href: "/2", oncreate: m.route.link}, "page 2"),
//                 ]),
//             ]),
//         ]),
//         m("main.mdl-layout__content", vnode.children),
//     ])
// }

let template = {
    view: (vnode) => m("div.mdl-layout.mdl-layout--fixed-header.mdl-js-layout"/**/, {
        oncreate: (vnode) => componentHandler.upgradeElement(vnode.dom),
        onbeforeremove: (vnode) => componentHandler.downgradeElements(vnode.dom),
    }, [
        m("header.mdl-layout__header", [
            m("a", {href: "/1", oncreate: m.route.link}, "page 1"),
            m("a", {href: "/2", oncreate: m.route.link}, "page 2"),
        ]),
        m("main.mdl-layout__content", vnode.children),
    ])
}

let page1 = {
    view: () => m(template, m('div', 'page1'))
}
let page2 = {
    view: () => m(template, m('div', 'page2'))
}


m.route.prefix("#");
m.route(document.body, "/1", {
    "/1": page1,
    "/2": page2,
});