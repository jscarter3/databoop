/*global require, setTimeout, clearTimeout, process*/
let chokidar = require("chokidar"),
    rollup = require("rollup"),
    resolve = require("rollup-plugin-node-resolve"),
    eslint = require("rollup-plugin-eslint").eslint,
    commonjs = require("rollup-plugin-commonjs"),
    cache;

let args = process.argv.slice(2),
    watch = args.includes("-w"),
    prod = args.includes("-prod");

// watcher.on("all", (event, path) => {
//     console.log(event, path);
// });

let build = debounce(function() {
    console.log("Bundling...");
    let start = Date.now();
    rollup.rollup({
        // The bundle's starting point. This file will be
        // included, along with the minimum necessary code
        // from its dependencies
        input: "src/index.js",
        // If you have a bundle you want to re-use (e.g., when using a watcher to rebuild as files change),
        // you can tell rollup use a previous bundle as its starting point.
        // This is entirely optional!
        cache: cache,
        plugins: [
            resolve({
                "jsnext": true,
                "main": true,
                "browser": true,
            }),
            commonjs(),
            eslint({
                "exclude": [
                    "node_modules/**",
                    "src/lib/**",
                ],
            }),
        ],
    }).then((bundle) => {
        // Cache our bundle for later use (optional)
        cache = bundle;

        // Alternatively, let Rollup do it for you
        // (this returns a promise). This is much
        // easier if you're generating a sourcemap
        let bundleProps = {
            format: "cjs",
            file: prod ? "build/ui/temp.js" : "dist/bundle.js",
        };
        if (!prod) bundleProps.sourcemap = "inline";

        return bundle.write(bundleProps);
    }).then(() => {
        console.log(`Bundled in ${Date.now() - start}ms.`);
    }).catch(console.log);
}, 250);

function debounce(func, wait, immediate) {
    let timeout;
    return function() {
        let context = this, args = arguments;
        let later = function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        let callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
}

if (watch) {
    let watcher = chokidar.watch("./src/", {ignored: /[\/\\]\./});
    console.log("Watching...");
    watcher.on("all", build);
} else {
    build();
}