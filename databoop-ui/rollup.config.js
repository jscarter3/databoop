import { eslint } from "rollup-plugin-eslint";
import resolve from "rollup-plugin-node-resolve";
import commonjs from "rollup-plugin-commonjs";
// import replace from 'rollup-plugin-replace';

export default {
    input: "src/index.js",
    output: {
        file: process.env.BUILD === 'prod' ? "build/ui/temp.js" : "dist/bundle.js", // equivalent to --output
        format: "cjs",
        sourceMap: "inline"
    },
    plugins: [
        resolve({
            "jsnext": true,
            "main": true,
            "browser": true,
        }),
        commonjs(),
        eslint({
            "exclude": [
                "node_modules/**",
                "src/lib/**",
            ],
        }),
        // replace({
        //     ENV: JSON.stringify(process.env.NODE_ENV || 'development'),
        // }),
    ],
};