module.exports = {
    "env": {
        "es6": true
    },
    "parserOptions": {
        "sourceType": "module"
    },
    "extends": "eslint:recommended",
    "globals": {
        "document": true,
        "window": true,
        "localStorage": true,
        "alert": true,
        "prompt": true,
        "console": true,
        "componentHandler": true,
        "R": true,
    },
    "rules": {
        // enable additional rules
        "indent": ["error", 4, {"SwitchCase": 1}],
        "linebreak-style": ["error", "unix"],
        "quotes": ["error", "double"],
        "semi": ["error", "always"],
        "curly": ["error", "multi-line"],
        "brace-style": ["error", "1tbs", {"allowSingleLine": true}],
        "eqeqeq": ["error", "smart"],

        // override default options for rules from base configurations
        "comma-dangle": ["error", "always-multiline"],

        "no-cond-assign": ["error", "always"],

        // disable rules from base configurations
        "no-console": "off",
    }
};