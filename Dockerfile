FROM openjdk:8u171-jre-alpine

ADD build/databoop.tar.gz /opt/databoop/
COPY boop.jks /opt/databoop/boop.jks
RUN keytool -noprompt -genkey -alias ledger -keysize 2048 -keyalg RSA -keystore /opt/databoop/databoop.jks -storepass changeit -keypass changeit -dname "CN=databoop.com, L=databoop, S=com, C=EN" -validity 3650

EXPOSE 8443
ENTRYPOINT exec java -Djavax.net.ssl.keyStore=/opt/databoop/boop.jks -Djavax.net.ssl.keyStorePassword=changeit -Djavax.net.ssl.trustStore=/opt/databoop/boop.jks -Djavax.net.ssl.trustStorePassword=changeit -Djava.security.egd=file:/dev/./urandom -Dtomcat.util.http.parser.HttpParser.requestTargetAllow="|{}" -Xmx1g -jar /opt/databoop/databoop-server.jar
